/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * SECTION:sl-person
 * @short_description: a person
 * @stability: Unstable
 * @include: sl-person.h
 *
 * #SlPerson represents a real-world person. A person has attributes
 * (#SlAttribute) that can be added, removed and modified.
 */

/**
 * SECTION:well-known-attributes
 * @short_description: descriptions of all well-known attributes
 * @stability: Unstable
 * @include: sl-person.h
 *
 * A well-known attribute is an attribute that's name is known to the system
 * (e.g. nick, email). Every attribute has a description and a runtime-type.
 */

/**
 * SECTION:sl-attribute-types
 * @short_description: runtime-types of attribute-values
 * @stability: Unstable
 * @include: sl-person.h
 *
 * Some structs to make your life with attributes easier.
 */

#include "sl-mutual-inclusion.h"

#include <libempathy/empathy-utils.h>

/* private structs and fields */

struct _SlPersonPriv
{
  gpointer cb_user_data;
  GCallback cb;
  GMainLoop *sync_loop;
  GError *sync_error;
};

static GObjectClass *parent_class = NULL;

static void sl_person_class_init (gpointer g_class, gpointer class_data);
static void sl_person_init (GTypeInstance *instance, gpointer g_class);
static void sl_person_dispose (GObject *object);
static void sl_person_get_property (GObject *object, guint property_id,
  GValue *value, GParamSpec *pspec);
static void sl_person_set_property (GObject *object, guint property_id,
  const GValue *value, GParamSpec *pspec);

static void mc_chat_ready (MissionControl *mc, GError *error, SlPerson *self);

GType
sl_person_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info =
    {
      sizeof (SlPersonClass),
      NULL,
      NULL,
      sl_person_class_init,
      NULL,
      NULL,
      sizeof (SlPerson),
      0,
      sl_person_init,
      NULL
    };
  
  if (type == 0)
    {
      type = g_type_register_static (SL_ENTITY_TYPE, "SlPerson", &info, 0);
    }
  return type;
}

static void
sl_person_class_init (gpointer g_class, gpointer class_data)
{
  parent_class = g_type_class_peek (g_type_parent (SL_PERSON_TYPE));
  g_assert (parent_class != NULL);
  
  GObjectClass *obj_class = G_OBJECT_CLASS (g_class);
  obj_class->dispose = sl_person_dispose;
  obj_class->get_property = sl_person_get_property;
  obj_class->set_property = sl_person_set_property;
}

static void
sl_person_init (GTypeInstance *instance, gpointer g_class)
{
  SlPerson *self = SL_PERSON (instance);
  SlPersonPriv *priv = g_new (SlPersonPriv, 1);
  self->priv = priv;
  SL_ENTITY (self)->disposed = FALSE;
}

static void
sl_person_dispose (GObject *object)
{
  SlPerson *self = SL_PERSON (object);
  g_return_if_fail (!SL_ENTITY (self)->disposed);
  
  g_free (self->priv);
  
  parent_class->dispose (object);
}

static void
sl_person_get_property (GObject *object, guint property_id, GValue *value,
  GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

static void
sl_person_set_property (GObject *object, guint property_id, const GValue *value,
  GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

SlName *sl_name_new (void)
{
  SlName *name = g_new (SlName, 1);
  name->names = NULL;
  name->family_name = "";
  name->honoric_prefixes = NULL;
  name->honoric_suffixes = NULL;
  return name;
}

SlAddress *sl_address_new (void)
{
  SlAddress *address = g_new (SlAddress, 1);
  address->country = "";
  address->region = "";
  address->city = "";
  address->street = "";
  address->postal_code = "";
  address->addition = "";
  address->po_box = "";
  return address;
}

SlPerson *
sl_person_new (const gchar *name, const gchar *family_name)
{
  SlPerson *self = g_object_new (SL_PERSON_TYPE, NULL);
  sl_person_constr (self, name, family_name);
  return self;
}

SlPerson *
sl_person_new_with_econtact (EContact *econtact)
{
  SlPerson *self = g_object_new (SL_PERSON_TYPE, NULL);
  sl_person_constr_with_econtact (self, econtact);
  return self;
}

void
sl_person_constr (SlPerson *self, const gchar *name, const gchar *family_name)
{
  sl_entity_constr (SL_ENTITY (self));
  self->priv->sync_loop = NULL;
  self->priv->sync_error = NULL;
  SlName *n = sl_name_new ();
  n->names = g_list_append (NULL, (gchar *) name);
  n->family_name = (gchar *) family_name;
  sl_person_set (self, SL_ATTR_NAME, n);
}

void
sl_person_constr_with_econtact (SlPerson *self, EContact *econtact)
{
  sl_entity_constr_with_econtact (SL_ENTITY (self), econtact);
  self->priv->sync_loop = NULL;
  self->priv->sync_error = NULL;
}

gchar *
sl_person_get_name (SlPerson *self)
{
  SlName *name = sl_person_get (self, SL_ATTR_NAME);
  return g_list_nth_data (name->names, 0);
}

void
sl_person_set_name (SlPerson *self, gchar *name)
{
  SlAttribute *attr_name = sl_person_get_attribute (self, SL_ATTR_NAME);
  SlName *n = sl_attribute_get (attr_name);
  if (n->names == NULL)
    {
      n->names = g_list_append (n->names, name);
    }
  else
    {
      /* TODO: does this leak? */
      n->names->data = name;
    }
  sl_attribute_modified (attr_name);
}

gchar *
sl_person_get_family_name (SlPerson *self)
{
  SlName *name = sl_person_get (self, SL_ATTR_NAME);
  return name->family_name;
}

void
sl_person_set_family_name (SlPerson *self, gchar *family_name)
{
  SlAttribute *attr_name = sl_person_get_attribute (self, SL_ATTR_NAME);
  SlName *n = sl_attribute_get (attr_name);
  n->family_name = family_name;
  sl_attribute_modified (attr_name);
}

gchar *
sl_person_get_full_name (SlPerson *self)
{
  return sl_person_get (self, SL_ATTR_FULL_NAME);
}

void
sl_person_set_full_name (SlPerson *self, gchar *fullname)
{
  sl_person_set (self, SL_ATTR_FULL_NAME, fullname);
}

gchar *
sl_person_get_nick (SlPerson *self)
{
  return sl_person_get (self, SL_ATTR_NICK);
}

void
sl_person_set_nick (SlPerson *self, gchar *nick)
{
  sl_person_set (self, SL_ATTR_NICK, nick);
}

gchar *
sl_person_get_string (SlPerson *self)
{
  gchar *nick = sl_person_get_nick (self);
  if (nick == NULL || g_str_equal (nick, ""))
    {
      return "?";
    }
  return nick;
}

static void
sl_async_init (GCallback *cbp, gpointer *cb_user_datap, GCallback cb, gpointer cb_user_data)
{
  g_return_if_fail (cbp != NULL);
  g_return_if_fail (cb_user_datap != NULL);
  
  *cbp = cb;
  *cb_user_datap = cb_user_data;
}

static void
sl_sync_ready (GMainLoop **loopp, GError **errorp, GError *error)
{
  g_return_if_fail (loopp != NULL);
  g_return_if_fail (errorp != NULL);
  
  *errorp = error;
  g_main_loop_quit (*loopp);
}

static gboolean
sl_synchronize (GMainLoop **loopp, GError **errorp, GError **error)
{
  g_return_val_if_fail (loopp != NULL, FALSE);
  g_return_val_if_fail (errorp != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  
  *loopp = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (*loopp);
  *loopp = NULL;
  if (*errorp == NULL)
    {
      return TRUE;
    }
  else
    {
      g_propagate_error (error, *errorp);
      *errorp = NULL;
      return FALSE;
    }
}

static void
communicate_chat_sync_cb (SlPerson *person, GError *error, gpointer user_data)
{
  g_return_if_fail (person != NULL && SL_IS_PERSON (person));
  
  sl_sync_ready (&person->priv->sync_loop, &person->priv->sync_error, error);
}

gboolean
sl_person_communicate_chat (SlPerson *self, GError **error)
{
  g_return_val_if_fail (self != NULL && SL_IS_PERSON (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  
  sl_person_communicate_chat_async (self, communicate_chat_sync_cb, NULL);
  return sl_synchronize (&self->priv->sync_loop, &self->priv->sync_error, error);
}

void
sl_person_communicate_chat_async (SlPerson *self, SlPersonCommunicateChatCb callback, gpointer user_data)
{
  g_return_if_fail (self != NULL && SL_IS_PERSON (self));
  
  /* TODO: better algorithm for finding "default" account to use for chat */
  sl_async_init (&self->priv->cb, &self->priv->cb_user_data, G_CALLBACK (callback), user_data);
  EmpathyContact *imcontact = sl_entity_get_imcontacts (SL_ENTITY (self))->data;
  sl_person_launch_chat (self, imcontact);
}

gboolean
sl_person_communicate_chat_to (SlPerson *self, const gchar *to_id, GError **error)
{
  g_return_val_if_fail (self != NULL && SL_IS_PERSON (self), FALSE);
  g_return_val_if_fail (to_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  
  sl_person_communicate_chat_to_async (self, to_id, communicate_chat_sync_cb, NULL);
  return sl_synchronize (&self->priv->sync_loop, &self->priv->sync_error, error);
}

void
sl_person_communicate_chat_to_async (SlPerson *self, const gchar *to_id, SlPersonCommunicateChatCb callback, gpointer user_data)
{
  g_return_if_fail (self != NULL && SL_IS_PERSON (self));
  g_return_if_fail (to_id != NULL);
  
  sl_async_init (&self->priv->cb, &self->priv->cb_user_data, G_CALLBACK (callback), user_data);
  EmpathyContact *imcontact = sl_entity_get_imcontact_from_id (SL_ENTITY (self), to_id);
  sl_person_launch_chat (self, imcontact);
}

gboolean
sl_person_communicate_chat_from (SlPerson *self, const gchar *from_id, GError **error)
{
  g_return_val_if_fail (self != NULL && SL_IS_PERSON (self), FALSE);
  g_return_val_if_fail (from_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  
  sl_person_communicate_chat_from_async (self, from_id, communicate_chat_sync_cb, NULL);
  return sl_synchronize (&self->priv->sync_loop, &self->priv->sync_error, error);
}

void
sl_person_communicate_chat_from_async (SlPerson *self, const gchar *from_id, SlPersonCommunicateChatCb callback, gpointer user_data)
{
  g_return_if_fail (self != NULL && SL_IS_PERSON (self));
  g_return_if_fail (from_id != NULL);
  
  sl_async_init (&self->priv->cb, &self->priv->cb_user_data, G_CALLBACK (callback), user_data);
  EmpathyContact *imcontact = sl_entity_get_imcontact_from_account_id (SL_ENTITY (self), from_id);
  sl_person_launch_chat (self, imcontact);
}

void
sl_person_launch_chat (SlPerson *self, EmpathyContact *imcontact)
{
  g_return_if_fail (self != NULL && SL_IS_PERSON (self));
  g_return_if_fail (imcontact != NULL && EMPATHY_IS_CONTACT (imcontact));
  
  const gchar *id = empathy_contact_get_id (imcontact);
  McAccount *account = empathy_contact_get_account (imcontact);
  MissionControl *mc = empathy_mission_control_new ();
  mission_control_request_channel_with_string_handle (mc, account,
    TP_IFACE_CHANNEL_TYPE_TEXT, id, TP_HANDLE_TYPE_CONTACT, (McCallback) mc_chat_ready,
    self);

}

gboolean
sl_person_communicate_call (SlPerson *self, SlCall call_type, GError **error)
{
  g_warning ("%s not yet implemented", __FUNCTION__);
  return FALSE;
}

gboolean
sl_person_communicate_email (SlPerson *self, GError **error)
{
  g_warning ("%s not yet implemented", __FUNCTION__);
  return FALSE;
}

static void
mc_chat_ready (MissionControl *mc, GError *error, SlPerson *self)
{
  if (self->priv->cb != NULL)
    {
      SlPersonCommunicateChatCb cb = SL_PERSON_COMMUNICATION_CHAT_CB (self->priv->cb);
      cb (self, error, self->priv->cb_user_data);
    }
}
