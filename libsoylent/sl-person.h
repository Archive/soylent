/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef SL_PERSON_H
#define SL_PERSON_H

#include "sl-mutual-inclusion.h"

#include <glib.h>
#include <glib-object.h>

#define SL_PERSON_TYPE            (sl_person_get_type ())
#define SL_PERSON(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, \
  SL_PERSON_TYPE, SlPerson))
#define SL_PERSON_CLASS(cls)      (G_TPYE_CHECK_CLASS_CAST (cls, \
  SL_PERSON_TYPE, SlPersonClass))
#define SL_IS_PERSON(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, \
  SL_PERSON_TYPE))
#define SL_IS_PERSON_CLASS(cls)   (G_TYPE_CHECK_CLASS_TYPE (cls, \
  SL_PERSON_TYPE))
#define SL_PERSON_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS (obj, \
  SL_PERSON_TYPE))

/* well-known attributes */

/**
 * SL_ATTR_ID:
 *
 * Unique ID of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_ID "id"

/**
 * SL_ATTR_NAME:
 *
 * A structured name of a person (names, family-name etc.). For a
 * more convinient way to handle a persons name see the sl_person_get/set_name*
 * methods.
 *
 * type: #SlName
 */
#define SL_ATTR_NAME "name"

/**
 * SL_ATTR_FULL_NAME:
 *
 * A string representing the full, printable name of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_FULL_NAME "full-name"

/**
 * SL_ATTR_NICK:
 *
 * The nickname of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_NICK "nick"

/**
 * SL_ATTR_GROUP:
 *
 * A list of groups a person is in.
 *
 * type: #GList <type>(string)</type>
 */
#define SL_ATTR_GROUP "group"

/**
 * SL_ATTR_ADDRESS:
 *
 * An (postal) address of a person.
 *
 * type: #SlAddress
 */
#define SL_ATTR_ADDRESS "address"

/**
 * SL_ATTR_EMAIL:
 *
 * An email-address of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_EMAIL "email"

/**
 * SL_ATTR_TELEPHONE:
 *
 * A telephone-number of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_TELEPHONE "telephone"

/**
 * SL_ATTR_BIRTHDAY:
 *
 * The birthday of a person.
 *
 * type: #GDate
 */
#define SL_ATTR_BIRTHDAY "birthday"

/**
 * SL_ATTR_URL:
 *
 * An URL of a person. This typically represents something like a homepage.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_URL "url"

/**
 * SL_ATTR_BLOG:
 *
 * The blog-URL of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_BLOG "blog"

/**
 * SL_ATTR_CALENDAR_URL:
 *
 * An URL pointing to where a calendar of a person is located.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_CALENDAR_URL "calendar-url"

/**
 * SL_ATTR_FREE_BUSY_URL:
 *
 * An URL pointing to where free-busy-information of a person is located.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_FREE_BUSY_URL "free-busy-url"

/**
 * SL_ATTR_NOTE:
 *
 * A note attached to a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_NOTE "note"

/**
 * SL_ATTR_PHOTO:
 *
 * A photo of a person.
 *
 * type: #GByteArray
 */
#define SL_ATTR_PHOTO "photo"

/**
 * SL_ATTR_ICON:
 *
 * An icon of a person.
 *
 * type: #GByteArray
 */
#define SL_ATTR_ICON "icon"

/* TODO: job struct? */
#define SL_ATTR_JOB_ROLE      "job-role"
#define SL_ATTR_JOB_TITLE     "job-title"

/**
 * SL_ATTR_AIM:
 *
 * An AIM-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_AIM "aim"

/**
 * SL_ATTR_GADUGADU:
 *
 * A GaduGadu-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_GADUGADU "gadugadu"

/**
 * SL_ATTR_ICQ:
 *
 * An ICQ-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_ICQ "icq"

/**
 * SL_ATTR_JABBER:
 *
 * A Jabber-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_JABBER "jabber"

/**
 * SL_ATTR_MSN:
 *
 * A MSN-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_MSN "msn"

/**
 * SL_ATTR_YAHOO:
 *
 * A Yahoo-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_YAHOO "yahoo"

/**
 * SL_ATTR_GROUPWISE:
 *
 * A Groupwise-id of a person.
 *
 * type: <type>string</type>
 */
#define SL_ATTR_GROUPWISE "groupwise"

/*#define EVC_GEO		    "GEO"*/
/*#define EVC_ICSCALENDAR     "ICSCALENDAR"*/
/*#define EVC_KEY             "KEY"*/
/*#define EVC_LABEL           "LABEL"*/
/*#define EVC_MAILER          "MAILER"*/
/*#define EVC_ORG             "ORG"*/
/*#define EVC_PRODID          "PRODID"*/
/*#define EVC_QUOTEDPRINTABLE "QUOTED-PRINTABLE"*/
/*#define EVC_REV             "REV"*/
/*#define EVC_ROLE            "ROLE"*/
/*#define EVC_TITLE           "TITLE"*/
/*#define EVC_TYPE            "TYPE"*/
/*#define EVC_VALUE           "VALUE"*/
/*#define EVC_VERSION         "VERSION"*/
/*#define EVC_X_ANNIVERSARY      "X-EVOLUTION-ANNIVERSARY"*/
/*#define EVC_X_ASSISTANT        "X-EVOLUTION-ASSISTANT"*/
/*#define EVC_X_BIRTHDAY         "X-EVOLUTION-BIRTHDAY"*/
/*#define EVC_X_FILE_AS          "X-EVOLUTION-FILE-AS"*/
/*#define EVC_X_LIST_SHOW_ADDRESSES "X-EVOLUTION-LIST-SHOW_ADDRESSES"*/
/*#define EVC_X_LIST          	"X-EVOLUTION-LIST"*/
/*#define EVC_X_MANAGER       	"X-EVOLUTION-MANAGER"*/
/*#define EVC_X_SPOUSE        	"X-EVOLUTION-SPOUSE"*/
/*#define EVC_X_WANTS_HTML    	"X-MOZILLA-HTML"*/
/*#define EVC_X_BOOK_URI     	"X-EVOLUTION-BOOK-URI"*/
/*#define EVC_X_CALLBACK         "X-EVOLUTION-CALLBACK"*/
/*#define EVC_X_COMPANY          "X-EVOLUTION-COMPANY"*/
/*#define EVC_X_DEST_CONTACT_UID "X-EVOLUTION-DEST-CONTACT-UID"
#define EVC_X_DEST_EMAIL       "X-EVOLUTION-DEST-EMAIL"
#define EVC_X_DEST_EMAIL_NUM   "X-EVOLUTION-DEST-EMAIL-NUM"
#define EVC_X_DEST_HTML_MAIL   "X-EVOLUTION-DEST-HTML-MAIL"
#define EVC_X_DEST_NAME        "X-EVOLUTION-DEST-NAME"
#define EVC_X_DEST_SOURCE_UID  "X-EVOLUTION-DEST-SOURCE-UID"
#define EVC_X_RADIO         	"X-EVOLUTION-RADIO"
#define EVC_X_TELEX         	"X-EVOLUTION-TELEX"
#define EVC_X_TTYTDD        	"X-EVOLUTION-TTYTDD"
#define EVC_X_VIDEO_URL     	"X-EVOLUTION-VIDEO-URL"*/


  
void sl_entity_add_attribute (SlEntity *self, SlAttribute *attr);
void sl_entity_remove_attribute (SlEntity *self, SlAttribute *attr);

SlAttribute *sl_entity_get_attribute (SlEntity *self, const gchar *attrname);
GList *sl_entity_get_attributes (SlEntity *self);
  
#define sl_person_get_book(self) \
  (sl_entity_get_storage (SL_ENTITY (self)))
#define sl_person_commit(self, error) \
  (sl_entity_commit (SL_ENTITY (self), error))

#define sl_person_add_attribute(self, attr) \
  (sl_entity_add_attribute (SL_ENTITY (self), attr))
#define sl_person_remove_attribute(self, attr)  \
  (sl_entity_remove_attribute (SL_ENTITY (self), attr))
#define sl_person_has_attribute(self, attrname) \
  (sl_entity_has_attribute (SL_ENTITY (self), attrname))
#define sl_person_get_attribute(self, attrname) \
  (sl_entity_get_attribute (SL_ENTITY (self), attrname))
#define sl_person_get_attributes(self) \
  (sl_entity_get_attributes (SL_ENTITY (self)))
  
#define sl_person_add(self, attrname, value) \
  (sl_entity_add (SL_ENTITY (self), attrname, value))
#define sl_person_get(self, attrname) \
  (sl_entity_get (SL_ENTITY (self), attrname))
#define sl_person_set(self, attrname, value) \
  (sl_entity_set (SL_ENTITY (self), attrname, value))
#define sl_person_get_at(self, attrname, index) \
  (gpointer sl_entity_get_at (SL_ENTITY (self) attrname, index))
#define sl_person_set_at(self, attrname, index, value) \
  (sl_entity_set_at (SL_ENTITY (self), attrname, index, value))
#define sl_person_remove_at(self, attrname, index) \
  (sl_entity_remove_at (SL_ENTITY (self), attrname, index))
#define sl_person_set_all(self, attrname, values) \
  (sl_entity_set_all (SL_ENTITY (self), attrname, values))
#define sl_person_get_all(self, attrname) \
  (sl_entity_get_all (SL_ENTITY (self), attrname))
#define sl_person_remove_all(self, attrname) \
  (sl_entity_remove_all (SL_ENTITY (self), attrname))
#define sl_person_modified(self, attrname) \
  (sl_entity_modified (SL_ENTITY (self), attrname))
#define sl_person_modified_at(self, attrname, index) \
  (sl_entity_modified_at (SL_ENTITY (self), attrname, index))

#define sl_person_has_iminfo(self) \
  (sl_entity_has_iminfo (SL_ENTITY (self)))
#define sl_person_is_online(self) \
  (sl_entity_is_online (SL_ENTITY (self)))
#define sl_person_get_presence(self) \
  (sl_entity_get_presence (SL_ENTITY (self)))
#define sl_person_get_imcontacts(self) \
  (sl_entity_get_imcontacts (SL_ENTITY (self)))

#define SL_PERSON_COMMUNICATION_CHAT_CB(cb) ((SlPersonCommunicateChatCb) cb)

typedef void (*SlPersonCommunicateChatCb) (SlPerson *person, GError *error,
                                               gpointer user_data);

typedef struct _SlPersonClass SlPersonClass;
typedef struct _SlPersonPriv  SlPersonPriv;
  
typedef struct _SlAddress SlAddress;
typedef struct _SlName    SlName;
  
typedef enum _SlCall SlCall;

enum _SlCall
{
  SL_CALL_AUDIO = 1,
  SL_CALL_VIDEO
};
  
struct _SlPerson
{
  SlEntity parent;
  SlPersonPriv *priv;
};

struct _SlPersonClass
{
  SlEntityClass parent;
};

struct _SlAddress
{
  gchar *country;
  gchar *region;
  gchar *city;
  gchar *street;
  gchar *postal_code;
  gchar *addition;
  gchar *po_box;
};

/**
 * SlName:
 * @names: string-list of given names
 * @family_name: the family name
 * @honoric_prefixes: string-list of honoric prefixes (e.g. Dr.)
 * @honoric_suffixes: string-list of honoric suffixes (e.g. Jr.)
 *
 * A structured representation of a name.
 */
struct _SlName
{
  GList *names;
  gchar *family_name;
  GList *honoric_prefixes;
  GList *honoric_suffixes;
};

GType sl_person_get_type (void);

SlAddress *sl_address_new (void);
SlName *sl_name_new (void);

SlPerson *sl_person_new (const gchar *name, const gchar *family_name);
SlPerson *sl_person_new_with_econtact (EContact *econtact);
void sl_person_constr (SlPerson *self, const gchar *name, const gchar *family_name);
void sl_person_constr_with_econtact (SlPerson *self, EContact *econtact);

gchar *sl_person_get_name (SlPerson *self);
void sl_person_set_name (SlPerson *self, gchar *name);
gchar *sl_person_get_family_name (SlPerson *self);
void sl_person_set_family_name (SlPerson *self, gchar *family_name);
gchar *sl_person_get_full_name (SlPerson *self);
void sl_person_set_full_name (SlPerson *self, gchar *fullname);

gchar *sl_person_get_nick (SlPerson *self);
void sl_person_set_nick (SlPerson *self, gchar *nick);

gchar *sl_person_get_string (SlPerson *self);

gboolean sl_person_communicate_chat (SlPerson *self, GError **error);
gboolean sl_person_communicate_chat_to (SlPerson *self, const gchar *to_id, GError **error);
gboolean sl_person_communicate_chat_from (SlPerson *self, const gchar *from_id, GError **error);
void sl_person_communicate_chat_async (SlPerson *self, SlPersonCommunicateChatCb callback, gpointer user_data);
void sl_person_communicate_chat_to_async (SlPerson *self, const gchar *to_id, SlPersonCommunicateChatCb callback, gpointer user_data);
void sl_person_communicate_chat_from_async (SlPerson *self, const gchar *from_id, SlPersonCommunicateChatCb callback, gpointer user_data);
void sl_person_launch_chat (SlPerson *self, EmpathyContact *imcontact);

gboolean sl_person_communicate_call (SlPerson *self, SlCall call_type, GError **error);
gboolean sl_person_communicate_email (SlPerson *self, GError **error);

#endif
