/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * SECTION:sl-attributes
 * @short_description: a system where attributes can be registered and mapped to
 * runtime-types
 * @stability: Unstable
 * @include: sl-attributes.h
 *
 * TODO: add description and an example in libsoylent/example
 */

#include "sl-attributes.h"
#include "sl-priv-util.h"

#include <stdlib.h>

/**
 * SlAttributeHandler:
 * @type: the type of the handler
 * @writer: attribute writer function
 * @reader: attribute reader function
 *
 * An attribute-handler is responsible for reading and writing attributes (i.e.
 * converting them from their runtime-types to a string / binary form and back).
 */

static GHashTable *attr_defs = NULL;
static GHashTable *attr_mappers = NULL;
static SlAttributeMapper *attrmapper_default = NULL;
static SlAttributeToStringFunc to_string_default = NULL;

void
sl_attributes_init (void)
{
  attr_defs = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_free);
  attr_mappers = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, NULL);
  
  to_string_default = SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string);
  
  attrmapper_default = (SlAttributeMapper *) sl_install_attribute_mapper (
    "string",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_string),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_string));
  
  sl_install_attribute_mapper ("bytes",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_bytes),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_bytes));
  sl_install_attribute_mapper ("gint",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_gint),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_gint));
  sl_install_attribute_mapper ("glist",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_glist),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_glist));
  sl_install_attribute_mapper ("gdate",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_gdate),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_gdate));
  sl_install_attribute_mapper ("slname",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_slname),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_slname));
  sl_install_attribute_mapper ("sladdress",
    SL_ATTRIBUTE_WRITER_FUNC (sl_attribute_writer_sladdress),
    SL_ATTRIBUTE_READER_FUNC (sl_attribute_reader_sladdress));
  
  /* TODO: perhaps this should go to SlPerson? */
  /* TODO: does g_free fit for slname, sladdress etc? what is with their strings? */
  
  sl_install_attribute (SL_ATTR_ID, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_NAME, "slname", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_slname_to_string));
  sl_install_attribute (SL_ATTR_FULL_NAME, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_NICK, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_GROUP, "glist", (GDestroyNotify) g_list_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_glist_to_string));
  sl_install_attribute (SL_ATTR_ADDRESS, "sladdress", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_sladdress_to_string));
  sl_install_attribute (SL_ATTR_EMAIL, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_TELEPHONE, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_BIRTHDAY, "gdate", (GDestroyNotify) g_date_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_gdate_to_string));
  sl_install_attribute (SL_ATTR_URL, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_BLOG, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_CALENDAR_URL, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_FREE_BUSY_URL, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_NOTE, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_PHOTO, "bytes", (GDestroyNotify) g_byte_array_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_bytes_to_string));
  sl_install_attribute (SL_ATTR_ICON, "bytes", (GDestroyNotify) g_byte_array_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_bytes_to_string));
  sl_install_attribute (SL_ATTR_JOB_ROLE, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
  sl_install_attribute (SL_ATTR_JOB_TITLE, "string", g_free,
    SL_ATTRIBUTE_TO_STRING_FUNC (sl_attribute_string_to_string));
}

void
sl_attributes_cleanup (void)
{
  /* TODO */
  /*g_hash_table_remove_all (attribute_handlers);
  g_hash_table_unref (attribute_handlers);
  g_free (attrhandler_default);*/
}

const SlAttributeMapper *
sl_install_attribute_mapper (const gchar *mappername,
  SlAttributeWriterFunc writer, SlAttributeReaderFunc reader)
{
  SlAttributeMapper *mapper = g_new (SlAttributeMapper, 1);
  mapper->writer = writer;
  mapper->reader = reader;
  
  /* if the key already exists the old handler will automatically be freed by
   * GHashTable */
  g_hash_table_insert (attr_mappers, (gpointer) mappername, mapper);
  
  return mapper;
}

const SlAttributeDef *
sl_install_attribute (const gchar *attrname, const gchar *attr_mapper_name,
                      GDestroyNotify cleanup, SlAttributeToStringFunc to_string)
{
  SlAttributeMapper *attrmapper = g_hash_table_lookup (attr_mappers, attr_mapper_name);
  g_return_val_if_fail (attrmapper != NULL, NULL);
  
  SlAttributeDef *def = g_new (SlAttributeDef, 1);
  def->mapper = attrmapper;
  def->cleanup = cleanup;
  def->to_string = to_string;
  
  /* if the key already exists the old handler will automatically be freed by
   * GHashTable */
  g_hash_table_insert (attr_defs, (gpointer) attrname, def);
  
  return def;
}

void
sl_attribute_cleanup (const gchar *attrname, gpointer value)
{
  SlAttributeDef *attrdef = g_hash_table_lookup (attr_defs, attrname);
  GDestroyNotify cleanup = NULL;
  if (attrdef == NULL)
    {
      cleanup = g_free;
    }
  else
    {
      cleanup = attrdef->cleanup;
    }
  if (cleanup != NULL)
    {
      cleanup (value);
    }
}

gchar *
sl_attribute_to_string (const gchar *attrname, gpointer value)
{
  SlAttributeDef *attrdef = g_hash_table_lookup (attr_defs, attrname);
  SlAttributeToStringFunc to_string = NULL;
  if (attrdef == NULL)
    {
      to_string = to_string_default;
    }
  else
    {
      to_string = attrdef->to_string;
    }
  if (to_string == NULL)
    {
      return NULL;
    }
  else
    {
      return to_string (attrname, value);
    }
}

/* TODO: this has to go to EntityEDS, something like
 * attrmapper = get_attribute_mapper (attrname);
 * if (attrmapper == NULL) {
 *    find out what systype the current thing is stored
 *    systype = (string if unknown, string or bytes if known)
 *    get_default_attribute_mapper (systype);
 * }
 * if (systype == BYTES) encode base64
 * now store in EDS */
gpointer
sl_attribute_mapper_write (const gchar *attrname, gpointer value)
{
  SlAttributeMapper *mapper = NULL;
  SlAttributeDef *attrdef = g_hash_table_lookup (attr_defs, attrname);
  if (attrdef != NULL)
    {
      mapper = attrdef->mapper;
    }
  else
    {
      mapper = attrmapper_default;
    }

  return mapper->writer (attrname, value);
}

gpointer
sl_attribute_mapper_read (const gchar *attrname, gpointer value)
{
  SlAttributeMapper *mapper = NULL;
  SlAttributeDef *attrdef = g_hash_table_lookup (attr_defs, attrname);
  if (attrdef != NULL)
    {
      mapper = attrdef->mapper;
    }
  else
    {
      mapper = attrmapper_default;
    }

  return mapper->reader (attrname, value);
}

gchar *
sl_attribute_writer_string (const gchar *attrname, gchar *value)
{
  return g_strdup (value);
}

gchar *
sl_attribute_reader_string (const gchar *attrname, gchar *value)
{
  return g_strdup (value);
}

gchar *
sl_attribute_writer_bytes (const gchar *attrname, GByteArray *bytes)
{
  return g_base64_encode (bytes->data, bytes->len);
}

GByteArray *
sl_attribute_reader_bytes (const gchar *attrname, gchar *string)
{
  GByteArray *bytes = g_byte_array_new ();
  bytes->data = g_base64_decode (string, (gsize *) &bytes->len);
  return bytes;
}

gchar *
sl_attribute_writer_gint (const gchar *attrname, GValue *value)
{
  GString *str = g_string_new (NULL);
  g_string_printf (str, "%d", g_value_get_int (value));
  return str->str;
}

GValue *
sl_attribute_reader_gint (const gchar *attrname, gchar *value)
{
  GValue *gvalue = g_new (GValue, 1);
  g_value_init (gvalue, G_TYPE_INT);
  g_value_set_int (gvalue, strtol(value, NULL, 10));
  return gvalue;
}

gchar *
sl_attribute_writer_glist (const gchar *attrname, GList *list)
{
  return sl_priv_util_strjoin_list (",", list);
}

GList *
sl_attribute_reader_glist (const gchar *attrname, gchar *string)
{
  return sl_priv_util_strsplit_list (string, ";", 0);
}

gchar *
sl_attribute_writer_gdate (const gchar *attrname, GDate *date)
{
  return sl_attribute_gdate_to_string (attrname, date);
}

GDate *
sl_attribute_reader_gdate (const gchar *attrname, gchar *string)
{
  /* TODO: more error handling */
  gchar **tokens = g_strsplit (string, "-", 0);
  gint year = strtol (tokens[0], NULL, 10);
  gint month = strtol (tokens[1], NULL, 10);
  gint day = strtol (tokens[2], NULL, 10);
  g_strfreev (tokens);
  return g_date_new_dmy (day, month, year);
}

SlName *
sl_attribute_reader_slname (const gchar *attrname, gchar *string)
{
  g_return_val_if_fail (attrname != NULL && string != NULL, NULL);
  
  /*"Family Name;Given Name;Additional Names,a;Honorific Prefixes,b;Honorific Suffixes,c"*/
  /* TODO: error-tolerance */
  gchar **fields = g_strsplit (string, ";", 0);
  gint length = g_strv_length (fields);

  SlName *name = sl_name_new ();
  if (length > 0) {
    name->family_name = fields[0];
  }
  if (length > 2) {    
    name->names = sl_priv_util_strsplit_list (fields[2], ",", 0);
  }
  if (length > 1) {
    name->names = g_list_prepend (name->names, fields[1]);
  }
  if (length > 3) {
    name->honoric_prefixes = sl_priv_util_strsplit_list (fields[3], ",", 0);
  }
  if (length > 4) {
    name->honoric_suffixes = sl_priv_util_strsplit_list (fields[4], ",", 0);
  }
  g_free (fields);
  return name;
}

gchar *
sl_attribute_writer_slname (const gchar *attrname, SlName *name)
{
  /*"Family Name;Given Name;Additional Names,a;Honorific Prefixes,b;Honorific Suffixes,c"*/
  GString *gstr = g_string_new (NULL);
  gchar *given_name = "";
  gchar *additional_names = "";
  if (g_list_length (name->names) > 0)
    {
      given_name = name->names->data;
      if (g_list_length (name->names) > 1)
        {
          additional_names = sl_priv_util_strjoin_list (",", name->names->next);
        }
    }
  gchar *honoric_prefixes = sl_priv_util_strjoin_list (",", name->honoric_prefixes);
  gchar *honoric_suffixes = sl_priv_util_strjoin_list (",", name->honoric_prefixes);
  g_string_printf (gstr, "%s;%s;%s;%s;%s", name->family_name, given_name, additional_names, honoric_prefixes, honoric_suffixes);
  gchar *string = gstr->str;
  g_string_free (gstr, FALSE);
  return string;
}

gchar *
sl_attribute_writer_sladdress (const gchar *attrname, SlAddress *address)
{
  /* TODO: how to handle structs from eds? */
  /* to the post office box; the extended address; the street
   address; the locality (e.g., city); the region (e.g., state or
   province); the postal code; the country name */
  GString *str = g_string_new (NULL);
  g_string_printf (str, "%s;%s;%s;%s;%s;%s;%s", address->po_box, address->addition, address->street, address->city, address->region, address->postal_code, address->country);
  gchar *string = str->str;
  g_string_free (str, FALSE);
  return string;
}

SlAddress *
sl_attribute_reader_sladdress (const gchar *attrname, gchar *string)
{
  gchar **tokens = g_strsplit (string, ";", 0);
  gint length = g_strv_length (tokens);
  SlAddress *addr = sl_address_new ();
  if (length > 0) addr->po_box = tokens[0];
  if (length > 1) addr->addition = tokens[1];
  if (length > 2) addr->street = tokens[2];
  if (length > 3) addr->city = tokens[3];
  if (length > 4) addr->region = tokens[4];
  if (length > 5) addr->postal_code = tokens[5];
  if (length > 6) addr->country = tokens[6];
  g_free (tokens);
  return addr;
}

#define STRING_TO_STRING_MAX_LEN 60

gchar *
sl_attribute_string_to_string (const gchar *attrname, gchar *string)
{
  if (g_utf8_strlen (string, -1) > STRING_TO_STRING_MAX_LEN)
    {
      GString *gstr = g_string_new_len (string, STRING_TO_STRING_MAX_LEN - 3);
      g_string_append (gstr, "...");
      string = gstr->str;
      g_string_free (gstr, FALSE);
    }
  else
    {
      string = g_strdup (string);
    }
  return string;
}

gchar *
sl_attribute_bytes_to_string (const gchar *attrname, GByteArray *bytes)
{
  GString *gstr = g_string_new (NULL);
  g_string_printf (gstr, "GByteArray [&0x%.8x, length: %db]",
                   GPOINTER_TO_UINT (bytes->data), (guint) (bytes->len));
  gchar *str = gstr->str;
  g_string_free (gstr, FALSE);
  return str;
}

gchar *
sl_attribute_gint_to_string (const gchar *attrname, GValue *value)
{
  GString *gstr = g_string_new (NULL);
  g_string_printf (gstr, "%d", g_value_get_int (value));
  gchar *str = gstr->str;
  g_string_free (gstr, FALSE);
  return str;
}

gchar *
sl_attribute_glist_to_string (const gchar *attrname, GList *list)
{
  GString *gstr = g_string_new (NULL);
  for (; list != NULL; list = list->next)
    {
      g_string_append_printf (gstr, "(%s)", (gchar *) list->data);
      if (list->next != NULL)
        {
          g_string_append (gstr, "->");
        }
    }
  gchar *string = gstr->str;
  g_string_free (gstr, FALSE);
  return string;
}

gchar *
sl_attribute_gdate_to_string (const gchar *attrname, GDate *date)
{
  GString *gstr = g_string_new (NULL);
  g_string_printf (gstr, "%.4d-%.2d-%.2d", g_date_get_year (date),
                   g_date_get_month (date), g_date_get_day (date));
  gchar *str = gstr->str;
  g_string_free (gstr, FALSE);
  return str;
}

gchar *
sl_attribute_slname_to_string (const gchar *attrname, SlName *name)
{
  g_return_val_if_fail (attrname != NULL && name != NULL, NULL);

  /* TODO: improve */
  GString *gstr = g_string_new ("");
  if (name->family_name != NULL)
    {
      g_string_printf (gstr, "%s", (gchar *) name->family_name);
    }
  gchar *string = gstr->str;
  g_string_free (gstr, FALSE);
  return string;
}

gchar *
sl_attribute_sladdress_to_string (const gchar *attrname, SlAddress *address)
{
  g_return_val_if_fail (attrname != NULL && address != NULL, NULL);
  GString *gstr = g_string_new (NULL);
  gboolean first = TRUE;
  if (address->street != NULL)
    {
      g_string_append (gstr, address->street);
      first = FALSE;
    }
  if (address->addition != NULL)
    {
      if (!first) g_string_append (gstr, ", ");
      g_string_append (gstr, address->addition);
      first = FALSE;
    }
  if (address->postal_code != NULL)
    {
      if (!first) g_string_append (gstr, ", ");
      g_string_append (gstr, address->postal_code);
      first = FALSE;
    }
  if (address->city != NULL)
    {
      if (!first && address->postal_code == NULL) g_string_append (gstr, ", ");
      g_string_append (gstr, address->city);
      first = FALSE;
    }
  if (address->region != NULL)
    {
      if (!first) g_string_append (gstr, ", ");
      g_string_append (gstr, address->region);
      first = FALSE;
    }
  if (address->country != NULL)
    {
      if (!first) g_string_append (gstr, ", ");
      g_string_append (gstr, address->country);
      first = FALSE;
    }
  if (address->po_box != NULL)
    {
      if (!first) g_string_append (gstr, "; ");
      g_string_append (gstr, address->po_box);
      first = FALSE;
    }
  gchar *str = gstr->str;
  g_string_free (gstr, FALSE);
  return str;
}
