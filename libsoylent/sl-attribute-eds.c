/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * SECTION:sl-attribute
 * @short_description: an attribute of a #SlEntity
 * @stability: Unstable
 * @include: sl-attribute.h
 *
 * #SlAttribute is an attribute that holds a list of values. Arbitrary values
 * can be added, removed and modified.
 */

#include "sl-mutual-inclusion.h"
#include "sl-priv-util.h"
#include "sl-attributes.h"
#include "soylent.h"

struct _SlAttributePriv
{
  SlEntity *entity;
  const gchar *name;
  GList *values;
  GList *eattributes;
};

static GObjectClass *attribute_parent_class = NULL;

static void sl_attribute_class_init (gpointer g_class, gpointer class_data);
static void sl_attribute_init (GTypeInstance *instance, gpointer g_class);
static void sl_attribute_dispose (GObject *object);
static void sl_attribute_set_property (GObject *object, guint property_id,
  const GValue *value, GParamSpec *pspec);
static void sl_attribute_get_property (GObject *object, guint property_id,
  GValue *value, GParamSpec *pspec);

GType
sl_attribute_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info =
    {
      sizeof (SlAttributeClass),
      NULL,
      NULL,
      sl_attribute_class_init,
      NULL,
      NULL,
      sizeof (SlAttribute),
      0,
      sl_attribute_init,
      NULL
   };
  
  if (type == 0)
    {
      type = g_type_register_static(G_TYPE_OBJECT, "SlAttribute", &info, 0);
    }
  return type;
}

static void
sl_attribute_class_init (gpointer g_class, gpointer class_data)
{
  attribute_parent_class = g_type_class_peek (g_type_parent
    (SL_ATTRIBUTE_TYPE));
  g_assert (attribute_parent_class != NULL);
  
  GObjectClass *obj_class = G_OBJECT_CLASS (g_class);
  obj_class->dispose = sl_attribute_dispose;
  obj_class->get_property = sl_attribute_get_property;
  obj_class->set_property = sl_attribute_set_property;
}

static void
sl_attribute_init (GTypeInstance *instance, gpointer g_class)
{
  SlAttribute *self = SL_ATTRIBUTE (instance);
  SlAttributePriv *priv = g_new (SlAttributePriv, 1);
  self->priv = priv;
  self->disposed = FALSE;
}

static void
sl_attribute_dispose (GObject *object)
{
  SlAttribute *self = SL_ATTRIBUTE (object);
  g_return_if_fail (!self->disposed);
  
  g_free (self->priv);
  self->disposed = TRUE;
  
  attribute_parent_class->dispose (object);
}

static void
sl_attribute_set_property (GObject *object, guint property_id,
  const GValue *value, GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

static void
sl_attribute_get_property (GObject *object, guint property_id,
  GValue *value, GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

gboolean sl_is_ignored_eattr (const gchar *eattrname)
{
  static GList *ignored = NULL;
  if (ignored == NULL)
    {
      ignored = g_list_append (ignored, EVC_VERSION);
      ignored = g_list_append (ignored, EVC_REV);
      ignored = g_list_append (ignored, EVC_LABEL);
    }
  return sl_priv_util_list_contains (ignored, (gpointer) eattrname, g_str_equal);
}

static GHashTable *eattrname_to_name_map = NULL;
static GHashTable *name_to_eattrname_map = NULL;

const gchar *
sl_attribute_name_to_eattrname (const gchar *name)
{
  if (name_to_eattrname_map == NULL)
    {
      name_to_eattrname_map = g_hash_table_new (g_str_hash, g_str_equal);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_ID, EVC_UID);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_NAME, EVC_N);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_FULL_NAME, EVC_FN);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_NICK, EVC_NICKNAME);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_GROUP, EVC_CATEGORIES);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_ADDRESS, EVC_ADR);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_EMAIL, EVC_EMAIL);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_TELEPHONE, EVC_TEL);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_BIRTHDAY, EVC_BDAY);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_URL, EVC_URL);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_BLOG, EVC_X_BLOG_URL);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_CALENDAR_URL, EVC_CALURI);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_FREE_BUSY_URL, EVC_FBURL);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_NOTE, EVC_NOTE);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_PHOTO, EVC_PHOTO);
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_ICON, EVC_LOGO); 
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_JOB_ROLE, EVC_ROLE); 
      g_hash_table_insert (name_to_eattrname_map, SL_ATTR_JOB_TITLE, EVC_TITLE);
    }
  
  const gchar *eattrname = g_hash_table_lookup (name_to_eattrname_map, name);
  if (eattrname == NULL)
    {
      GString *str = g_string_new (NULL);
      g_string_printf (str, "X-%s", name);
      eattrname = str->str;
    }
  return eattrname;
}

const gchar *
sl_attribute_eattrname_to_name (const gchar *eattrname)
{
  if (eattrname_to_name_map == NULL)
    {
      eattrname_to_name_map = g_hash_table_new (g_str_hash, g_str_equal);
      g_hash_table_insert (eattrname_to_name_map, EVC_UID, SL_ATTR_ID);
      g_hash_table_insert (eattrname_to_name_map, EVC_N, SL_ATTR_NAME);
      g_hash_table_insert (eattrname_to_name_map, EVC_FN, SL_ATTR_FULL_NAME);
      g_hash_table_insert (eattrname_to_name_map, EVC_NICKNAME, SL_ATTR_NICK);
      g_hash_table_insert (eattrname_to_name_map, EVC_CATEGORIES, SL_ATTR_GROUP);
      g_hash_table_insert (eattrname_to_name_map, EVC_ADR, SL_ATTR_ADDRESS);
      g_hash_table_insert (eattrname_to_name_map, EVC_EMAIL, SL_ATTR_EMAIL);
      g_hash_table_insert (eattrname_to_name_map, EVC_TEL, SL_ATTR_TELEPHONE);
      g_hash_table_insert (eattrname_to_name_map, EVC_BDAY, SL_ATTR_BIRTHDAY);
      g_hash_table_insert (eattrname_to_name_map, EVC_URL, SL_ATTR_URL);
      g_hash_table_insert (eattrname_to_name_map, EVC_X_BLOG_URL, SL_ATTR_BLOG);
      g_hash_table_insert (eattrname_to_name_map, EVC_CALURI, SL_ATTR_CALENDAR_URL);
      g_hash_table_insert (eattrname_to_name_map, EVC_FBURL, SL_ATTR_FREE_BUSY_URL);
      g_hash_table_insert (eattrname_to_name_map, EVC_NOTE, SL_ATTR_NOTE);
      g_hash_table_insert (eattrname_to_name_map, EVC_PHOTO, SL_ATTR_PHOTO);
      g_hash_table_insert (eattrname_to_name_map, EVC_LOGO, SL_ATTR_ICON);
      g_hash_table_insert (eattrname_to_name_map, EVC_ROLE, SL_ATTR_JOB_ROLE);
      g_hash_table_insert (eattrname_to_name_map, EVC_TITLE, SL_ATTR_JOB_TITLE);
    }
  
  const gchar *name = g_hash_table_lookup (eattrname_to_name_map, eattrname);
  if (name == NULL)
    {
      if (!g_str_has_prefix (eattrname, "X-"))
        {
          /* workaround for illegal vCard fields */
          g_warning ("EDS: illegal or not implemented vCard field: %s", eattrname);
          name = g_utf8_strdown (eattrname, -1);
          if (name_to_eattrname_map == NULL) sl_attribute_name_to_eattrname ("foo");
          g_hash_table_insert (eattrname_to_name_map, (gchar *) eattrname, (gchar *) name);
          g_hash_table_insert (name_to_eattrname_map, (gchar *) name, (gchar *) eattrname);
        }
      else
        {
          /* FIXME: this leaks, cause name must be freed somehow */
          name = g_utf8_strdown (&eattrname[2], -1);
        }
    }
  return name;
}

void
sl_attribute_constr (SlAttribute *self, const gchar *name, gpointer value)
{
  self->priv->entity = NULL;
  self->priv->name = name;
  self->priv->eattributes = NULL;
  self->priv->values = NULL;
  if (value != NULL)
    {
      /* TODO: write value to eattr */
      EVCardAttribute *eattr = e_vcard_attribute_new (NULL, sl_attribute_name_to_eattrname (name));
      self->priv->eattributes = g_list_append (self->priv->eattributes, eattr);
      self->priv->values = g_list_append (self->priv->values, value);
    }
}

void
sl_attribute_constr_with_values (SlAttribute *self, const gchar *name, 
                                 GList *values)
{
  sl_attribute_constr (self, name, NULL);
  /* TODO: write value to eattr */
  self->priv->values = values;
}

void
sl_attribute_constr_with_eattr (SlAttribute *self, EVCardAttribute *eattr)
{
  /* TODO: all attributes twice in memory, once in their "vcard form" and once
   * in their "runtime form". This is clearly not optimal. */
  self->priv->entity = NULL;
  self->priv->eattributes = NULL;
  self->priv->values = NULL;
  self->priv->name = sl_attribute_eattrname_to_name (e_vcard_attribute_get_name (eattr));

  sl_debug_attribute ("creating attribute \"%s\" from eattribute", self->priv->name);

  sl_attribute_add_from_eattr (self, eattr);
}

void
sl_attribute_constr_with_eattributes (SlAttribute *self, GList *eattributes)
{
  EVCardAttribute *eattr = eattributes->data;
  sl_attribute_constr_with_eattr (self, eattr);
  eattributes = eattributes->next;
  for (; eattributes != NULL; eattributes = eattributes->next)
    {
      eattr = eattributes->data;
      sl_attribute_add_from_eattr (self, eattr);
    }
}

SlAttribute *
sl_attribute_new (const gchar *name, gpointer value)
{
  SlAttribute *self = g_object_new (SL_ATTRIBUTE_TYPE, NULL);
  sl_attribute_constr (self, name, value);
  return self;
}

SlAttribute *
sl_attribute_new_empty (const gchar *name)
{
  SlAttribute *self = g_object_new (SL_ATTRIBUTE_TYPE, NULL);
  sl_attribute_constr (self, name, NULL);
  return self;
}

SlAttribute *
sl_attribute_new_with_values (const gchar *name, GList *values)
{
  SlAttribute *self = g_object_new (SL_ATTRIBUTE_TYPE, NULL);
  sl_attribute_constr_with_values (self, name, values);
  return self;
}

SlAttribute *
sl_attribute_new_with_eattr (EVCardAttribute *eattr)
{
  SlAttribute *self = g_object_new (SL_ATTRIBUTE_TYPE, NULL);
  sl_attribute_constr_with_eattr (self, eattr);
  return self;
}

SlAttribute *
sl_attribute_new_with_eattributes (GList *eattributes)
{
  SlAttribute *self = g_object_new (SL_ATTRIBUTE_TYPE, NULL);
  sl_attribute_constr_with_eattributes (self, eattributes);
  return self;
}

void
sl_attribute_add_from_eattr (SlAttribute *self, EVCardAttribute *eattr)
{
  sl_debug_attribute ("adding eattribute to attribute \"%s\"", self->priv->name);
  
  GList *evalue_tokens = e_vcard_attribute_get_values (eattr);
  GString *evalue = g_string_new ("");
  for (; evalue_tokens != NULL; evalue_tokens = evalue_tokens->next)
    {
      gchar *evalue_token = evalue_tokens->data;
      if (!g_str_equal (evalue->str, ""))
        {
          g_string_append (evalue, ";");
        }
      g_string_append (evalue, evalue_token);
    }
  
  gpointer value = sl_attribute_mapper_read (self->priv->name, evalue->str);
  self->priv->values = g_list_append (self->priv->values, value);
  self->priv->eattributes = g_list_append (self->priv->eattributes, eattr);
}

void
sl_attribute_update_eattributes (SlAttribute *self, GList *eattributes)
{
  sl_debug_attribute ("updating eattributes of attribute \"%s\"", self->priv->name);
  /* TODO: this method may only be called if eattributes have exactly the same
   * order */
  g_list_free (self->priv->eattributes);
  self->priv->eattributes = g_list_copy (eattributes);
}

void
sl_attribute_set_all_from_eattributes (SlAttribute *self, GList *eattributes)
{
  GList *values = self->priv->values;
  for (; values != NULL; values = values->next)
    {
      sl_attribute_cleanup (self->priv->name, values->data);
    }
  g_list_free (values);
  self->priv->values = NULL;
  
  g_list_free (self->priv->eattributes);
  self->priv->eattributes = NULL;
  
  for (; eattributes != NULL; eattributes = eattributes->next)
    {
      sl_attribute_add_from_eattr (self, eattributes->data);
    }
}

SlEntity *
sl_attribute_get_entity (SlAttribute *self)
{
  return self->priv->entity;
}

void
sl_attribute_set_entity (SlAttribute *self, SlEntity *entity)
{
  if (entity != NULL)
    {
      g_object_ref (entity);
    }
  self->priv->entity = entity;
}

GList *sl_attribute_get_eattributes (SlAttribute *self)
{
  return self->priv->eattributes;
}

EVCardAttribute *
sl_attribute_create_eattr (SlAttribute *self, gpointer value)
{
  sl_debug_attribute ("creating eattribute from value of \"%s\"", self->priv->name);
  
  gchar *evalue = sl_attribute_mapper_write (self->priv->name, value);
  const gchar *eattrname = sl_attribute_name_to_eattrname (self->priv->name);
  EVCardAttribute *eattr = e_vcard_attribute_new (NULL, eattrname);

  gchar **tokens = g_strsplit (evalue, ";", 0);
  gchar **tokenp = NULL;
  for (tokenp = tokens; *tokenp != NULL; tokenp++)
    {
      e_vcard_attribute_add_value (eattr, *tokenp);
    }
  g_free (tokens);

  if (self->priv->entity != NULL)
    {
      EContact *econtact = sl_entity_get_econtact (self->priv->entity);
      e_vcard_add_attribute (E_VCARD (econtact), eattr);
    }

  return eattr;
}

void
sl_attribute_add (SlAttribute *self, gpointer value)
{
  sl_attribute_add_emit (self, value, TRUE);
}

void
sl_attribute_add_emit (SlAttribute *self, gpointer value, gboolean emit)
{
  EVCardAttribute *eattr = sl_attribute_create_eattr (self, value);
  self->priv->values = g_list_append (self->priv->values, value);
  self->priv->eattributes = g_list_append (self->priv->eattributes, eattr);
  
  /* im stuff */
  if (self->priv->entity != NULL)
    {
      if (sl_im_is_im_attribute (self->priv->name))
        {
          sl_entity_associate_imcontacts (self->priv->entity);
        }
    }
  /* /im stuff */
  
  if (emit)
    {
      sl_attribute_emit_modified (self);
    }
}

gpointer
sl_attribute_get (SlAttribute *self)
{
  return sl_attribute_get_at (self, 0);
}

void
sl_attribute_set (SlAttribute *self, gpointer value)
{
  if (!sl_attribute_set_at (self, 0, value))
    {
      sl_attribute_add (self, value);
    }
}

gpointer
sl_attribute_get_at (SlAttribute *self, gint index)
{
  GList *value_nth = g_list_nth (self->priv->values, index);
  if (value_nth == NULL)
    {
      return NULL;
    }
  else
    {
      return value_nth->data;
    }
}

gboolean
sl_attribute_set_at (SlAttribute *self, gint index, gpointer value)
{
  return sl_attribute_set_at_emit (self, index, value, TRUE);
}

gboolean
sl_attribute_set_at_emit (SlAttribute *self, gint index, gpointer value, gboolean emit)
{
  GList *value_nth = g_list_nth (self->priv->values, index);
  if (value_nth == NULL)
    {
      return FALSE;
    }

  /* im stuff */
  if (self->priv->entity != NULL)
    {
      if (sl_im_is_im_attribute (self->priv->name))
        {
          sl_entity_associate_imcontacts (self->priv->entity);
        }
    }
  /* /im stuff */
  
  sl_attribute_cleanup (self->priv->name, value_nth->data);
  value_nth->data = value;
  
  return sl_attribute_modified_at_emit (self, index, emit);
}

gboolean
sl_attribute_remove_at (SlAttribute *self, gint index)
{
  return sl_attribute_remove_at_emit (self, index, TRUE);
}

gboolean
sl_attribute_remove_at_emit (SlAttribute *self, gint index, gboolean emit)
{
  gpointer value = sl_attribute_get_at (self, index);
  GList *eattr_nth = g_list_nth (self->priv->eattributes, index);
  if (value == NULL)
    {
      return FALSE;
    }
  g_assert (eattr_nth != NULL);
  EVCardAttribute *eattr = eattr_nth->data;
  
  /* im stuff */
  if (self->priv->entity != NULL)
    {
      if (sl_im_is_im_attribute (self->priv->name))
        {
          sl_entity_associate_imcontacts (self->priv->entity);
        }
    }
  /* /im stuff */
  
  self->priv->values = g_list_remove (self->priv->values, value);
  sl_attribute_cleanup (self->priv->name, value);
  
  self->priv->eattributes = g_list_remove (self->priv->eattributes, eattr);
  
  if (self->priv->entity != NULL)
    {
      EContact *econtact = sl_entity_get_econtact (self->priv->entity);
      e_vcard_remove_attribute (E_VCARD (econtact), eattr);
    }
  
  if (emit)
    {
      sl_attribute_emit_modified (self);
    }
  
  return TRUE;
}

/**
 * sl_attribute_get_all:
 * @self: a #SlAttribute
 *
 * Returns a list of all values of the attribute @self.
 *
 * Returns: a list of all values of the attribute. It is owned by the attribute
 * and should neither be modified nor freed. The list will reflect all changes
 * of the attribute (e.g. if a value is added to the attribute).
 */
GList *
sl_attribute_get_all (SlAttribute *self)
{
  g_return_val_if_fail (self != NULL && SL_IS_ATTRIBUTE (self), NULL);
  return self->priv->values;
}

/**
 * sl_attribute_set_all:
 * @self: a #SlAttribute
 * @values: a list of values
 *
 * Replaces all values of the attribute @self with @values.
 */
void
sl_attribute_set_all (SlAttribute *self, GList *values)
{
  g_return_if_fail (self != NULL && SL_IS_ATTRIBUTE (self));
  
  sl_attribute_remove_all_emit (self, FALSE);
  for (; values != NULL; values = values->next)
    {
      sl_attribute_add_emit (self, values->data, FALSE);
    }
}

void
sl_attribute_remove_all (SlAttribute *self)
{
  sl_attribute_remove_all_emit (self, TRUE);
}

void
sl_attribute_remove_all_emit (SlAttribute *self, gboolean emit)
{
  while (self->priv->values != NULL)
    {
      g_assert (sl_attribute_remove_at (self, 0));
    }
  
  if (emit)
    {
      sl_attribute_emit_modified (self);
    }
}

void
sl_attribute_modified (SlAttribute *self)
{
  sl_attribute_modified_at (self, 0);
}

gboolean
sl_attribute_modified_at (SlAttribute *self, gint index)
{
  return sl_attribute_modified_at_emit (self, index, TRUE);
}

gboolean
sl_attribute_modified_at_emit (SlAttribute *self, gint index, gboolean emit)
{
  GList *value_nth = g_list_nth (self->priv->values, index);
  GList *eattr_nth = g_list_nth (self->priv->eattributes, index);
  if (value_nth == NULL)
    {
      return FALSE;
    }
  g_assert (eattr_nth != NULL);
  
  EVCardAttribute *eattr = sl_attribute_create_eattr (self, value_nth->data);
  if (self->priv->entity != NULL)
    {
      EContact *econtact = sl_entity_get_econtact (self->priv->entity);
      e_vcard_remove_attribute (E_VCARD (econtact), eattr_nth->data);
    }
  eattr_nth->data = eattr;
  
  if (emit)
    {
      sl_attribute_emit_modified (self);
    }

  return TRUE;
}

void
sl_attribute_emit_modified (SlAttribute *self)
{
  g_return_if_fail (self != NULL && SL_IS_ATTRIBUTE (self));
  
  /* TODO: implement signals for SlAttribute and SlPerson */
  SlBook *book = NULL;
  if (self->priv->entity != NULL && (book = sl_person_get_book (SL_PERSON (self->priv->entity))) != NULL)
    {
      /* TODO: old values? */
      sl_book_emit_person_attribute_modified (book, SL_PERSON (self->priv->entity), self, NULL, TRUE);
    }
}

gint
sl_attribute_get_value_count (SlAttribute *self)
{
  return g_list_length (self->priv->values);
}

const gchar *
sl_attribute_get_name (SlAttribute *self)
{
  return self->priv->name;
}
