/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef SL_ATTRIBUTES_H
#define SL_ATTRIBUTES_H

#include "sl-person.h"

#include <glib.h>
#include <glib-object.h>

#define SL_ATTRIBUTE_WRITER_FUNC(func)  ((SlAttributeWriterFunc) (func))
#define SL_ATTRIBUTE_READER_FUNC(func)  ((SlAttributeReaderFunc) (func))

#define SL_ATTRIBUTE_TO_STRING_FUNC(func) ((SlAttributeToStringFunc) (func))

typedef gpointer (*SlAttributeWriterFunc)(const gchar *attrname,
                                          gpointer value);
typedef gpointer (*SlAttributeReaderFunc)(const gchar *attrname,
                                          gpointer value);

typedef gchar * (*SlAttributeToStringFunc)(const gchar *attrname,
                                           gpointer value);

typedef enum _SlAttributeSysType SlAttributeSysType;

typedef struct _SlAttributeDef    SlAttributeDef;
typedef struct _SlAttributeMapper SlAttributeMapper;

struct _SlAttributeMapper
{
  SlAttributeWriterFunc writer;
  SlAttributeReaderFunc reader;
};

struct _SlAttributeDef
{
  SlAttributeMapper *mapper;
  GDestroyNotify cleanup;
  SlAttributeToStringFunc to_string;
};

void sl_attributes_init (void);
void sl_attributes_cleanup (void);

const SlAttributeMapper *sl_install_attribute_mapper (const gchar *mappername,
  SlAttributeWriterFunc writer,SlAttributeReaderFunc reader);
const SlAttributeDef *sl_install_attribute (const gchar *attrname,
  const gchar *attr_mapper_name, GDestroyNotify cleanup,
  SlAttributeToStringFunc to_string);

gchar *sl_attribute_to_string (const gchar *attrname, gpointer value);
void sl_attribute_cleanup (const gchar *attrname, gpointer value);

gpointer sl_attribute_mapper_write (const gchar *attrname, gpointer value);
gpointer sl_attribute_mapper_read (const gchar *attrname, gpointer value);

gchar *sl_attribute_writer_string (const gchar *attrname, gchar *value);
gchar *sl_attribute_reader_string (const gchar *attrname, gchar *value);
gchar *sl_attribute_writer_bytes (const gchar *attrname, GByteArray *bytes);
GByteArray *sl_attribute_reader_bytes (const gchar *attrname,
                                       gchar *string);
gchar *sl_attribute_writer_gint (const gchar *attrname, GValue *value);
GValue *sl_attribute_reader_gint (const gchar *attrname, gchar *value);
gchar *sl_attribute_writer_glist (const gchar *attrname, GList *list);
GList *sl_attribute_reader_glist (const gchar *attrname, gchar *string);
gchar *sl_attribute_writer_gdate (const gchar *attrname, GDate *date);
GDate *sl_attribute_reader_gdate (const gchar *attrname, gchar *string);
SlName *sl_attribute_reader_slname (const gchar *attrname, gchar *string);
gchar *sl_attribute_writer_slname (const gchar *attrname, SlName *name);
gchar *sl_attribute_writer_sladdress (const gchar *attrname,
                                      SlAddress *address);
SlAddress *sl_attribute_reader_sladdress (const gchar *attrname, gchar *string);

gchar *sl_attribute_default_to_string (const gchar *attrname, gpointer value);
gchar *sl_attribute_string_to_string (const gchar *attrname, gchar *string);
gchar *sl_attribute_bytes_to_string (const gchar *attrname, GByteArray *bytes);
gchar *sl_attribute_gint_to_string (const gchar *attrname, GValue *value);
gchar *sl_attribute_glist_to_string (const gchar *attrname, GList *list);
gchar *sl_attribute_gdate_to_string (const gchar *attrname, GDate *date);
gchar *sl_attribute_slname_to_string (const gchar *attrname, SlName *name);
gchar *sl_attribute_sladdress_to_string (const gchar *attrname,
                                         SlAddress *address);

#endif
