/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * SECTION:soylent
 * @short_description: global libsoylent functions
 * @stability: Unstable
 * @include: soylent.h
 *
 * soylent contains some global functions for libsoylent, like initializing
 * and cleaning up libsoylent.
 */

#include "soylent.h"
#include "sl-priv-util.h"

#include <glib-object.h>
#include <libempathy/empathy-utils.h>
#include <libmissioncontrol/mc-account-monitor.h>

/* TODO: the whole communications stuff probably should go into its own
 * file */

static EmpathyContactManager *contactmgr = NULL;
static McAccountMonitor *account_monitor = NULL;
static GHashTable *im_protocols = NULL;

static void
mc_account_created (McAccountMonitor *account_manager, gchar *name, gpointer user_data)
{
  McAccount *account = mc_account_lookup (name);
  const gchar *protocol_name = sl_priv_util_get_protocol_name_from_account (account);
  g_hash_table_insert (im_protocols, (gpointer) protocol_name, "");
  sl_debug_book ("discovered im attribute \"%s\"", protocol_name);
}

static void
sl_im_init (void)
{  
  MissionControl *mc = empathy_mission_control_new ();
  
  im_protocols = g_hash_table_new (g_str_hash, g_str_equal);
  GList *accounts = mc_accounts_list ();
  for (; accounts != NULL; accounts = accounts->next)
    {
      const gchar *protocol_name = sl_priv_util_get_protocol_name_from_account (accounts->data);
      g_hash_table_insert (im_protocols, (gpointer) protocol_name, "");
      sl_debug_book ("discovered im attribute \"%s\"", protocol_name);
    }
  accounts = g_list_first (accounts);
  mc_accounts_list_free (accounts);
  
  account_monitor = mc_account_monitor_new ();
  g_signal_connect (account_monitor, "account-created",
                    G_CALLBACK (mc_account_created), NULL);
  
  
  /*DBusGConnection *dbus_con = dbus_g_bus_get(DBUS_BUS_SESSION, error);
	if (dbus_con == NULL) {
		g_error("fail");
	}
	mc = mission_control_new(dbus_con);*/
  contactmgr = empathy_contact_manager_new ();  
  mission_control_set_presence(mc, MC_PRESENCE_AVAILABLE, NULL, NULL, NULL);
}

EmpathyContactManager *
sl_im_get_contact_manager (void)
{
  return contactmgr;
}

gboolean
sl_im_is_im_attribute (const gchar *attrname)
{
  return (g_hash_table_lookup (im_protocols, attrname) != NULL);
}

gboolean
sl_init (GError **error)
{
	g_type_init ();
  sl_attributes_init ();
  sl_im_init ();
	return sl_book_setup (error);
}

void
sl_cleanup (void)
{
  sl_attributes_cleanup ();
}
