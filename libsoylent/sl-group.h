/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* TODO: groups are be ignored atm */

#ifndef SL_GROUP_H
#define SL_GROUP_H

#include "sl-group.h"

#include <glib.h>
#include <glib-object.h>

#define SL_GROUP_TYPE           (sl_group_get_type ())
#define SL_GROUP(obj)           (G_TYPE_CHECK_INSTANCE_CAST (obj, \
  SL_GROUP_TYPE, SlGroup))
#define SL_GROUP_CLASS(cls)     (G_TPYE_CHECK_CLASS_CAST (cls, SL_GROUP_TYPE, \
  SlGroupClass))
#define SL_IS_GROUP(obj)        (G_TYPE_CHECK_INSTANCE_TYPE (obj, \
  SL_GROUP_TYPE))
#define SL_IS_GROUP_CLASS(cls)  (G_TYPE_CHECK_CLASS_TYPE (cls, SL_GROUP_TYPE))
#define SL_GROUP_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS (obj, SL_GROUP_TYPE))

typedef struct _SlGroup       SlGroup;
typedef struct _SlGroupClass  SlGroupClass;
typedef struct _SlGroupPriv   SlGroupPriv;

struct _SlGroup
{
  GObject parent;
  SlGroupPriv *priv;
};

struct _SlGroupClass
{
  GObjectClass parent;
};

GType sl_group_get_type (void);

SlGroup *sl_group_new (void);

#endif
