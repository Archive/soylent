/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* TODO: will be splitted to SlEntity (interface), SlEntityBase (abstract-
 * base class), SlEntityEDS. for now this will be misused as SlEntityEDS */

/**
 * SECTION:sl-entity
 * @short_description: a entity (holding attributes)
 * @stability: Unstable
 * @include: sl-entity.h
 *
 * #SlEntity is a storage for attributes (#SlAttribute). Attributes can be
 * added, removed and modified.
 */

#include "sl-mutual-inclusion.h"
#include "sl-priv-util.h"
#include "soylent.h"

#include <libempathy/empathy-contact.h>
#include <libempathy/empathy-contact-list.h>

/* private structs and fields */

struct _SlEntityPriv
{
  SlBook *storage;
  EContact *econtact;
  GList *imcontacts;
  GList *attributes;
  GHashTable *attribute_table;
  McPresence presence;
};

static GObjectClass *parent_class = NULL;

static void sl_entity_class_init (gpointer g_class, gpointer class_data);
static void sl_entity_init (GTypeInstance *instance, gpointer g_class);
static void sl_entity_dispose (GObject *object);
static void sl_entity_set_property (SlEntity *self, guint property_id,
	const GValue *value, GParamSpec *pspec);
static void sl_entity_get_property (SlEntity *self, guint property_id,
	GValue *value, GParamSpec *pspec);

static void sl_entity_mix_presences (SlEntity *self);
static void empathy_contact_notify_presence (EmpathyContact *imcontact,
                                             GParamSpec *pspec, SlEntity *self);

GType
sl_entity_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info =
    {
      sizeof (SlEntityClass),
      NULL,
      NULL,
      sl_entity_class_init,
      NULL,
      NULL,
      sizeof (SlEntity),
      0,
      sl_entity_init,
      NULL
   };
  
  if (type == 0)
    {
      type = g_type_register_static(G_TYPE_OBJECT, "SlEntity", &info, 0);
    }
  return type;
}

static void
sl_entity_class_init (gpointer g_class, gpointer class_data)
{
	parent_class = g_type_class_peek (g_type_parent (SL_ENTITY_TYPE));
	g_assert (parent_class != NULL);
	
  GObjectClass *obj_class = G_OBJECT_CLASS (g_class);
  obj_class->dispose = sl_entity_dispose;
  obj_class->set_property = (void (*)(GObject *, guint, const GValue *,
                              GParamSpec *)) sl_entity_set_property;
  obj_class->get_property = (void (*)(GObject *, guint, GValue *, GParamSpec *))
                              sl_entity_get_property;
  
  /*GParamSpec *pspec = NULL;
  pspec = g_param_spec_pointer ("entity-handler", "entity-handler",
            "Entity handler", G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);
  
  g_object_class_install_property (obj_class, SL_ENTITY_PROPERTY_ENTITY_HANDLER,
    pspec);*/
}

static void
sl_entity_init (GTypeInstance *instance, gpointer g_class)
{
	SlEntity *self = SL_ENTITY (instance);
	SlEntityPriv *priv = g_new (SlEntityPriv, 1);
	self->priv = priv;
	self->priv->attributes = NULL;
	self->disposed = FALSE;
}

static void
sl_entity_dispose (GObject *object) {
	SlEntity *self = SL_ENTITY (object);
	g_return_if_fail (!self->disposed);
	
  g_list_free (self->priv->attributes);
	g_free (self->priv);
	self->disposed = TRUE;
  
  parent_class->dispose (object);
}

static void
sl_entity_set_property (SlEntity *self, guint property_id, const GValue *value,
  GParamSpec *pspec)
{
  switch (property_id)
    {
      //case SL_ENTITY_PROPERTY_ENTITY_HANDLER:
        //self->priv->entity_handler = g_object_ref (g_value_get_pointer (value));
        //break;
      default:
        g_assert_not_reached();
    }
}

static void
sl_entity_get_property (SlEntity *self, guint property_id, GValue *value,
  GParamSpec *pspec)
{
	g_warning("%s not implemented", __FUNCTION__);
}

void
sl_entity_constr (SlEntity *self)
{
  self->priv->storage = NULL;
  self->priv->econtact = e_contact_new ();
  self->priv->imcontacts = NULL;
  self->priv->attribute_table = g_hash_table_new (g_str_hash, g_str_equal);
  self->priv->attributes = NULL;
  self->priv->presence = MC_PRESENCE_UNSET;
}

void
sl_entity_constr_with_econtact (SlEntity *self, EContact *econtact)
{
  self->priv->storage = NULL;
  self->priv->imcontacts = NULL;
  self->priv->attribute_table = g_hash_table_new (g_str_hash, g_str_equal);
  self->priv->attributes = NULL;
  self->priv->presence = MC_PRESENCE_UNSET;
  self->priv->econtact = g_object_ref (econtact);
  
  sl_debug_entity ("creating entity from econtact%s", "");
  
  GList *eattributes = g_list_copy (e_vcard_get_attributes (E_VCARD (self->priv->econtact)));
  eattributes = g_list_sort (eattributes, (GCompareFunc) sl_priv_util_compare_eattributes);
  
  for (; eattributes != NULL; eattributes = eattributes->next)
    {
      EVCardAttribute *eattr = eattributes->data;
      const gchar *eattrname = e_vcard_attribute_get_name (eattr);
      if (sl_is_ignored_eattr (eattrname))
        {
          sl_debug_entity ("ignored eattribute \"%s\"", eattrname);
          continue;
        }
      
      SlAttribute *attr = sl_attribute_new_with_eattr (eattr);
      g_assert (attr != NULL);
      
      /* TODO: make one function for the next two calls */
      self->priv->attributes = g_list_append (self->priv->attributes, g_object_ref (attr));
      g_hash_table_insert (self->priv->attribute_table, (const gpointer) sl_attribute_get_name (attr), g_object_ref (attr));
      
      sl_attribute_set_entity (attr, self);
      
      while (TRUE)
        {
          GList *next = eattributes->next;
          if (next == NULL || !g_str_equal(e_vcard_attribute_get_name (next->data), eattrname))
            {
              break;
            }
          eattributes = eattributes->next;
          eattr = eattributes->data;
          sl_attribute_add_from_eattr (attr, eattr);
        }
      
      g_object_unref (attr);
    }
  
  g_list_free (eattributes);
}

EContact
*sl_entity_get_econtact (SlEntity *self)
{
  return self->priv->econtact;
}

void
sl_entity_set_econtact (SlEntity *self, EContact *econtact)
{
  self->priv->econtact = g_object_ref (econtact);
}

void
sl_entity_associate_imcontacts (SlEntity *self)
{
  sl_debug_entity ("associating imcontacts%s", "");
  g_list_free (self->priv->imcontacts);
  self->priv->imcontacts = NULL;
  EmpathyContactManager *contactmgr = sl_im_get_contact_manager ();
  GList *imcontacts = empathy_contact_list_get_members (EMPATHY_CONTACT_LIST (contactmgr));
  for (; imcontacts != NULL; imcontacts = imcontacts->next)
    {
      sl_entity_associate_imcontact (self, imcontacts->data, FALSE);
    }
  sl_entity_mix_presences (self);
}

void
sl_entity_associate_imcontact (SlEntity *self, EmpathyContact *imcontact, gboolean mix_presences)
{
  McAccount *account = empathy_contact_get_account (imcontact);
  const gchar *protocol_name = sl_priv_util_get_protocol_name_from_account (account);
  const gchar *imcontactid = empathy_contact_get_id (imcontact);
  if (imcontactid == NULL)
    {
      /*g_warning ("id of imcontact \"%s\" is NULL", empathy_contact_get_name (imcontact));*/
      return;
    }
  
  GList *ids = sl_entity_get_all (self, protocol_name);
  for (; ids != NULL; ids = ids->next)
    {
      gchar *id = ids->data;
      if (g_str_equal (id, imcontactid))
        {
          self->priv->imcontacts = g_list_append (self->priv->imcontacts, imcontact);
          g_signal_connect (imcontact, "notify::presence", G_CALLBACK (empathy_contact_notify_presence), self);
          if (mix_presences)
            {
              sl_entity_mix_presences (self);
            }
        }
    }
}

void
sl_entity_deassociate_imcontact (SlEntity *self, EmpathyContact *imcontact)
{
  if (g_list_index (self->priv->imcontacts, imcontact) != -1) 
    {
      sl_entity_associate_imcontacts (self);
    }
}

gboolean sl_entity_has_iminfo (SlEntity *self)
{
  return (self->priv->imcontacts != NULL);
}

gboolean sl_entity_is_online (SlEntity *self)
{
  return (self->priv->presence != MC_PRESENCE_UNSET &&
          self->priv->presence != MC_PRESENCE_OFFLINE &&
          self->priv->presence != MC_PRESENCE_HIDDEN);
}

McPresence sl_entity_get_presence (SlEntity *self)
{
  return self->priv->presence;
}

GList *
sl_entity_get_imcontacts (SlEntity *self)
{
  return self->priv->imcontacts;
}

EmpathyContact *
sl_entity_get_imcontact_from_id (SlEntity *self, const gchar *id)
{
  GList *imcontacts = sl_entity_get_imcontacts (self);
  for (; imcontacts != NULL; imcontacts = imcontacts->next)
    {
      EmpathyContact *imcontact = imcontacts->data;
      const gchar *imcontact_id = empathy_contact_get_id (imcontact);
      if (g_str_equal (id, imcontact_id))
        {
          return imcontact;
        }
    }
  return NULL;
}

EmpathyContact *
sl_entity_get_imcontact_from_account_id (SlEntity *self, const gchar *account_id)
{
  GList *imcontacts = sl_entity_get_imcontacts (self);
  for (; imcontacts != NULL; imcontacts = imcontacts->next)
    {
      EmpathyContact *imcontact = imcontacts->data;
      gchar *imcontact_account_id = sl_priv_util_get_account_id_from_imcontact (imcontact);
      if (g_str_equal (account_id, imcontact_account_id))
        {
          return imcontact;
        }
    }
  
  return NULL;
}

SlBook *
sl_entity_get_storage (SlEntity *self)
{
  return self->priv->storage;
}

void
sl_entity_set_storage (SlEntity *self, SlBook *storage)
{
  self->priv->storage = storage;
}

gboolean
sl_entity_commit (SlEntity *self, GError **error)
{
  if (self->priv->storage == NULL)
    {
      /* TODO: return error */
      return FALSE;
    }
  return sl_book_commit_person (self->priv->storage, SL_PERSON (self), error);
}

void
sl_entity_add_attribute_shallow (SlEntity *self, SlAttribute *attr)
{
  /* TODO: what if attribute already exists? add values?*/
  self->priv->attributes = g_list_append (self->priv->attributes, g_object_ref (attr));
  g_hash_table_insert (self->priv->attribute_table, (const gpointer) sl_attribute_get_name (attr), g_object_ref (attr));
  sl_attribute_set_entity (attr, self);
  
  /* im stuff */
  if (sl_im_is_im_attribute (sl_attribute_get_name (attr)))
    {
      sl_entity_associate_imcontacts (self);
    }
  /* /im stuff */
}

void
sl_entity_remove_attribute_shallow (SlEntity *self, SlAttribute *attr)
{
  self->priv->attributes = g_list_remove (self->priv->attributes, attr);
  g_hash_table_remove (self->priv->attribute_table, (const gpointer) sl_attribute_get_name (attr));
  sl_attribute_set_entity (attr, NULL);
  g_object_unref (attr);
  g_object_unref (attr);
  
  /* im stuff */
  if (sl_im_is_im_attribute (sl_attribute_get_name (attr)))
    {
      sl_entity_associate_imcontacts (self);
    }
  /* /im stuff */
}

void
sl_entity_add_attribute (SlEntity *self, SlAttribute *attr)
{
  sl_entity_add_attribute_shallow (self, attr);
  
  GList *eattributes = sl_attribute_get_eattributes (attr);
  for (; eattributes != NULL; eattributes = eattributes->next)
    {
      EVCardAttribute *eattr = eattributes->data;
      e_vcard_add_attribute (E_VCARD (self->priv->econtact), eattr);
    }
}

void
sl_entity_remove_attribute (SlEntity *self, SlAttribute *attr)
{
  GList *eattributes = sl_attribute_get_eattributes (attr);
  for (; eattributes != NULL; eattributes = eattributes->next)
    {
      EVCardAttribute *eattr = eattributes->data;
      e_vcard_remove_attribute (E_VCARD (self->priv->econtact), eattr);
    }
  
  sl_entity_remove_attribute_shallow (self, attr);
}

/* ABSTRACT
SlEntity *
sl_entity_new (SlEntityHandler *entity_handler)
{
  g_return_val_if_fail (entity_handler != NULL &&
    SL_IS_ENTITY_HANDLER (entity_handler), NULL);
  
	SlEntity *self = g_object_new (SL_ENTITY_TYPE, "entity-handler",
                      entity_handler, NULL);
  self->priv->econtact = e_contact_new ();
  
	return self;
}*/

/*SlEntityHandler *sl_entity_get_handler (SlEntity *self)
{
  return self->priv->entity_handler;
}*/

gboolean sl_entity_has_attribute (SlEntity *self, const gchar *attrname)
{
  return (sl_entity_get_attribute (self, attrname) != NULL);
}

SlAttribute *sl_entity_get_attribute (SlEntity *self, const gchar *attrname)
{
  return g_hash_table_lookup (self->priv->attribute_table, attrname);
}

SlAttribute *
sl_entity_get_attribute_from_eattr (SlEntity *self, EVCardAttribute *eattr)
{
  const gchar *eattrname = e_vcard_attribute_get_name (eattr);
  const gchar *attrname = sl_attribute_eattrname_to_name (eattrname);
  return sl_entity_get_attribute (self, attrname);
}

GList *sl_entity_get_attributes (SlEntity *self)
{
  return self->priv->attributes;
}

SlAttribute *
sl_entity_get_attribute_create (SlEntity *self, const gchar *attrname)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL) 
    {
      attr = sl_attribute_new_empty (attrname);
      sl_entity_add_attribute (self, attr);
    }
  return attr;
}

void
sl_entity_add (SlEntity *self, const gchar *attrname, gpointer value)
{
  SlAttribute *attr = sl_entity_get_attribute_create (self, attrname);
  sl_attribute_add (attr, value);
}

gpointer
sl_entity_get (SlEntity *self, const gchar *attrname)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL)
    {
      return NULL;
    }
  return sl_attribute_get (attr);
}

void
sl_entity_set (SlEntity *self, const gchar *attrname, gpointer value)
{
  SlAttribute *attr = sl_entity_get_attribute_create (self, attrname);
  sl_attribute_set (attr, value);
}

gpointer
sl_entity_get_at (SlEntity *self, const gchar *attrname, gint index)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL)
    {
      return NULL;
    }
  return sl_attribute_get_at (attr, index);
}

gboolean
sl_entity_set_at (SlEntity *self, const gchar *attrname, gint index, gpointer value)
{
  SlAttribute *attr = sl_entity_get_attribute_create (self, attrname);
  if (attr == NULL)
    {
      return FALSE;
    }
  return sl_attribute_set_at (attr, index, value);
}

gboolean
sl_entity_remove_at (SlEntity *self, const gchar *attrname, gint index)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL)
    {
      return FALSE;
    }
  return sl_attribute_remove_at (attr, index);
}

void
sl_entity_set_all (SlEntity *self, const gchar *attrname, GList *values)
{
  SlAttribute *attr = sl_entity_get_attribute_create (self, attrname);
  sl_attribute_set_all (attr, values);
}

GList *
sl_entity_get_all (SlEntity *self, const gchar *attrname)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL)
    {
      return NULL;
    }
  return sl_attribute_get_all (attr);
}

void
sl_entity_remove_all (SlEntity *self, const gchar *attrname)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  if (attr == NULL)
    {
      return;
    }
  sl_attribute_remove_all (attr);
}

void sl_entity_modified (SlEntity *self, const gchar *attrname)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  g_return_if_fail (attr != NULL);
  sl_attribute_modified (attr);
}

gboolean sl_entity_modified_at (SlEntity *self, const gchar *attrname, gint index)
{
  SlAttribute *attr = sl_entity_get_attribute (self, attrname);
  g_return_val_if_fail (attr != NULL, FALSE);
  return sl_attribute_modified_at (attr, index);
}

static gint
sl_priv_util_presence_to_gint (McPresence presence)
{
  switch (presence)
    {
      case MC_PRESENCE_UNSET:
        return 1;
      case MC_PRESENCE_OFFLINE:
        return 2;
      case MC_PRESENCE_HIDDEN:
        return 3;
      case MC_PRESENCE_EXTENDED_AWAY:
        return 4;
      case MC_PRESENCE_AWAY:
        return 5;
      case MC_PRESENCE_DO_NOT_DISTURB:
        return 6;
      case MC_PRESENCE_AVAILABLE:
        return 7;
      default:
        g_assert_not_reached ();
    }
}

static void
sl_entity_mix_presences (SlEntity *self)
{
  g_return_if_fail (self != NULL && SL_IS_PERSON (self));
  
  GList *imcontacts_iter = self->priv->imcontacts;
  EmpathyContact *imcontact = NULL;
  McPresence old_presence = self->priv->presence;

  if (imcontacts_iter == NULL)
    {
      self->priv->presence = MC_PRESENCE_UNSET;
    }
  else
    {
      imcontact = imcontacts_iter->data;
      imcontacts_iter = imcontacts_iter->next;
      self->priv->presence = empathy_contact_get_presence (imcontact);
    }

  for (; imcontacts_iter != NULL; imcontacts_iter = imcontacts_iter->next)
    {
      McPresence presence = MC_PRESENCE_UNSET;
      
      imcontact = imcontacts_iter->data;
      presence = empathy_contact_get_presence (imcontact);
      if (sl_priv_util_presence_to_gint (presence) >
          sl_priv_util_presence_to_gint (self->priv->presence))
        {
          self->priv->presence = presence;
        }
    }

  if (old_presence != self->priv->presence)
    {
      sl_debug_entity ("presence %d -> %d", sl_priv_util_presence_to_gint (old_presence),
                       sl_priv_util_presence_to_gint (self->priv->presence));
      if (self->priv->storage != NULL)
        {
          sl_book_emit_person_presence_changed (self->priv->storage, SL_PERSON (self));
        }
    }
}

static void
empathy_contact_notify_presence (EmpathyContact *imcontact, GParamSpec *pspec,
                                 SlEntity *self)
{
  /* TODO: on account deactivate + active again we get this 3 times, why? */
  sl_entity_mix_presences (self);
}
