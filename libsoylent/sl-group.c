/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* TODO: groups are be ignored atm */

#include "sl-group.h"

/* private structs and fields */

struct _SlGroupPriv
{
  gpointer foo;
};

static GObjectClass *parent_class = NULL;

static void sl_group_class_init (gpointer g_class, gpointer class_data);
static void sl_group_init (GTypeInstance *instance, gpointer g_class);
static void sl_group_dispose (GObject *object);
static void sl_group_get_property (GObject *object, guint property_id,
  GValue *value, GParamSpec *pspec);
static void sl_group_set_property (GObject *object, guint property_id,
  const GValue *value, GParamSpec *pspec);

GType
sl_group_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info =
    {
      sizeof (SlGroupClass),
      NULL,
      NULL,
      sl_group_class_init,
      NULL,
      NULL,
      sizeof (SlGroup),
      0,
      sl_group_init,
      NULL
    };
  
  if (type == 0)
    {
      type = g_type_register_static (G_TYPE_OBJECT, "SlGroup", &info, 0);
    }
  return type;
}

static void
sl_group_class_init (gpointer g_class, gpointer class_data)
{
  parent_class = g_type_class_peek (g_type_parent (SL_GROUP_TYPE));
  g_assert (parent_class != NULL);
  
  GObjectClass *obj_class = G_OBJECT_CLASS (g_class);
  obj_class->dispose = sl_group_dispose;
  obj_class->get_property = sl_group_get_property;
  obj_class->set_property = sl_group_set_property;
}

static void
sl_group_init (GTypeInstance *instance, gpointer g_class)
{
  SlGroup *self = SL_GROUP (instance);
  SlGroupPriv *priv = g_new (SlGroupPriv, 1);
  self->priv = priv;
  //SL_ENTITY (self)->disposed = FALSE;
}

static void
sl_group_dispose (GObject *object)
{
  SlGroup *self = SL_GROUP (object);
  //g_return_if_fail (!SL_ENTITY (self)->disposed);
  
  g_free (self->priv);
  
  parent_class->dispose (object);
}

static void
sl_group_get_property (GObject *object, guint property_id, GValue *value,
  GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

static void
sl_group_set_property (GObject *object, guint property_id, const GValue *value,
  GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

SlGroup *
sl_group_new (void)
{
  SlGroup *self = g_object_new (SL_GROUP_TYPE, NULL);
  return self;
}
