=== 2008.08.18: libsoylent v0.5.0 "let's talk about..." ===

Version 0.5.0 brings online-functionality to libsoylent. Want to launch a chat
with someone? One function call. Want to see who's online? One function call.
Want to see someone's online-status? You get it.

This release is also the last one for Google Summer of Code 2008. It's the
result of about four months of work. Phew.

The plan for the next release is that it will be a pure documentation and
bug-fixing release. Also in that version: libsoylent will stop taking control
of strings passed to it.

--- Changes ---

 * implemented Telepathy / Mission-Control / Empathy support
 * implemented various functions / methods for IM-information (get
   online-people, get presence etc.)
 * added communication functions (e.g. launching a chat with someone)
 * added an example for online-functionality
 * enhanced documentation
 * fixed some bugs

--------------------------------------------------------------------------------

=== 2008.08.12: libsoylent v0.4.0 "small step, giant leap" ===

Let me present you the newest version of libsoylent: 0.4.0. Three weeks of hard
work went into this release, and in fact so much was added and changed that we
decided to skip a version-number. Sorry 0.3.0.

So, what's in it? More or less a complete people-management-library. Our goal
was to create a simple-yet-powerful API that "just works". Hopefully we managed
that. If you have no clear picture of what libsoylent is or just want to know
more about it, look at the examples we've put up on the libsoylent-page [1].

What comes next? Because the library is that new and fresh, much testing will
be needed. If you want to help, just play around with libsoylent and report
bugs that you may encounter to our mailinglist. Thanks! Besides, next to the
need of more documentation and polishing, here is a list of features that will
follow in the next releases:
 * live-attributes (e.g. online-status)
 * communication (launch applications for chat / mail etc.)
 * address-book searching (atm you can only get all people)
 * people association / merging

Please leave a comment on my blog [2] if you have thoughts, ideas, criticism,
feature-requests or suggestions for changing something. Just let us hear your
opinion.

Anyway, if you're working on something that needs people-functionality, why not
give libsoylent a try?

[1] http://live.gnome.org/Soylent/libsoylent#head-350076c20ead379a4d797b273ff4db09e67c22ab
[2] http://www.kalterregen.de/blog/

--- Changes ---

 * creating and modifying attributes works
 * adding / removing attributes to / from people works
 * attribute-mapping (for C runtime-types) implemented
 * attribute-system (definition, cleanup and to-string functionality)
   implemented
 * changes to people can be commited
 * loading people from the addressbook implemented
 * signals for SlBook, SlPerson and SlAttribute implemented
 * integrated gtk-doc
 * added a bunch of documentation
 * added more tests
 * added example-code (three examples)
 * revised architecture
 * a lot of internal-code improvements
 * fixed a bunch of bugs
 * added debugging functions
 * a lot of polishing was done (code-cleanup, convinience functions and macros
   etc.)

--------------------------------------------------------------------------------

=== 2008.07.23: libsoylent v0.2.0 "management qualities" ===

The second release features the basic functionality one would expect from a
people-library. Create addressbooks and add some people to it. And then remove
them again. People management at its basic level.

--- Changes ---

 * creating, opening and deleting addressbooks implemented
 * added tests for addressbook functionality
 * creating people implemented
 * people can be added and removed to / from addressbooks
 * added tests for people functionality

--------------------------------------------------------------------------------

=== 2008.07.13: libsoylent v0.1.0 "the seed" ===

The first release of libsoylent is finally there. It contains the most important
GObject classes and many function and method stubs and should provide a good
ground for the next upcoming features. You can't do much with it, but you will
get a good impression of the whole libsoylent architecture.

--- Changes ---

 * created basic architecture (SlPerson, SlGroup, SlBook, SlEntity,
   SlEntityHandler)
 * created basic test-suite
 * implemented listing and creating addressbooks

--------------------------------------------------------------------------------
