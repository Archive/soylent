/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * TODO: architecture should be: SlStorage -> (SlStorageBase) -> SlBook
 */

#ifndef SL_BOOK_H
#define SL_BOOK_H

#include "sl-mutual-inclusion.h"

#include <glib.h>
#include <glib-object.h>

#define SL_BOOK_TYPE            (sl_book_get_type ())
#define SL_BOOK(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, \
  SL_BOOK_TYPE, SlBook))
#define SL_BOOK_CLASS(cls)      (G_TYPE_CHECK_CLASS_CAST (cls, SL_BOOK_TYPE, \
  SlBookClass))
#define SL_IS_BOOK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, SL_BOOK_TYPE))
#define SL_IS_BOOK_CLASS(cls)   (G_TYPE_CHECK_CLASS_TYPE (cls, SL_BOOK_TYPE))
#define SL_BOOK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS (obj, SL_BOOK_TYPE))

#define SL_BOOK_ERROR sl_book_error_quark ()

#define SL_BOOK_DEFAULT sl_book_default

typedef struct _SlBookClass SlBookClass;
typedef struct _SlBookPriv  SlBookPriv;

typedef enum _SlBookProperty  SlBookProperty;
typedef enum _SlBookError     SlBookError;

/* TODO: how does evolution do this? implement book_manager */
typedef void (*SlBookCBBookCreated)   (SlBook *book_manager, gchar *book_name, gpointer user_data);
typedef void (*SlBookCBBookDeleted)   (SlBook *book_manager, gchar *book_name, gpointer user_data);

typedef void (*SlBookCBModified)                (SlBook *book, gboolean on_purpose, gpointer user_data); /* means entity -added, -removed, -modified */
typedef void (*SlBookCBPersonAdded)             (SlBook *book, SlPerson *person, gboolean on_purpose, gpointer user_data);
typedef void (*SlBookCBPersonRemoved)           (SlBook *book, SlPerson *person, gboolean on_purpose, gpointer user_data);
typedef void (*SlBookCBPersonModified)          (SlBook *book, SlPerson *person, gboolean on_purpose, gpointer user_data);
typedef void (*SlBookCBPersonAttributeAdded)    (SlBook *book, SlPerson *person, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
typedef void (*SlBookCBPersonAttributeRemoved)  (SlBook *book, SlPerson *person, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
typedef void (*SlBookCBPersonAttributeModified) (SlBook *book, SlPerson *person, SlAttribute *attr, GList *old_values, gboolean on_purpose, gpointer user_data);

typedef void (*SlEntityCBModified)          (SlEntity *entity, gboolean on_purpose, gpointer user_data);
typedef void (*SlEntityCBAttributeAdded)    (SlEntity *entity, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
typedef void (*SlEntityCBAttributeRemoved)  (SlEntity *entity, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
typedef void (*SlEntityCBAttributeModified) (SlEntity *entity, SlAttribute *attr, gboolean on_purpose, GList *old_values, gpointer user_data);

typedef void (*SlBookPersonPresenceChangedCb) (SlBook *book, SlPerson *person, gpointer user_data);

enum SlBookProperty
{
  SL_BOOK_PROPERTY_EBOOK = 1
};

enum SlBookError
{
  SL_BOOK_ERROR_ALREADY_EXISTS,
  SL_BOOK_ERROR_NOT_EXISTING,
  SL_BOOK_ERROR_FAILED
};

struct _SlBook
{
  GObject parent;
  gboolean disposed;
  SlBookPriv *priv;
};

struct _SlBookClass
{
  GObjectClass parent;
};

SlBook *sl_book_default;

GType sl_book_get_type (void);
GQuark sl_book_error_quark (void);
gboolean sl_book_setup (GError **error);

gboolean sl_book_constr (SlBook *self, GError **error);

GList *sl_book_get_books (void);
gboolean sl_book_exists (const gchar *name);
SlBook *sl_book_create (const gchar *name, GError **error);
SlBook *sl_book_open (const gchar *name, GError **error);
SlBook *sl_book_open_default (GError **error);
gboolean sl_book_delete (const gchar *name, GError **error);

gboolean sl_book_add_person (SlBook *self, SlPerson *person, GError **error);
gboolean sl_book_remove_person (SlBook *self, SlPerson *person, GError **error);
gboolean sl_book_commit_person (SlBook *self, SlPerson *person, GError **error);
/* TODO: more to come, like searching for attributes */
GList *sl_book_get_people (SlBook *self);
GList *sl_book_get_online_people (SlBook *self);
SlPerson *sl_book_get_person (SlBook *self, const gchar *attrname, gpointer value);

void sl_book_emit_person_presence_changed (SlBook *self, SlPerson *person);
void sl_book_emit_person_added (SlBook *self, SlPerson *person, gboolean on_purpose);
void sl_book_emit_person_removed (SlBook *self, SlPerson *person, gboolean on_purpose);
void sl_book_emit_person_attribute_added (SlBook *self, SlPerson *person, SlAttribute *attr, gboolean on_purpose);
void sl_book_emit_person_attribute_removed (SlBook *self, SlPerson *person, SlAttribute *attr, gboolean on_purpose);
void sl_book_emit_person_attribute_modified (SlBook *self, SlPerson *person, SlAttribute *attr, GList *old_values, gboolean on_purpose);

#endif
