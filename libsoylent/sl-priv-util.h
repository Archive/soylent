/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef SL_PRIV_UTIL_H
#define SL_PRIV_UTIL_H

#include <libebook/e-book.h>
#include <libempathy/empathy-contact.h>

/*#define SL_ENABLE_DEBUG_ALL*/

#define sl_debug(unit, format, ...) g_printerr ("dbg[%s]: ", unit); \
                                    g_printerr (format, __VA_ARGS__); \
                                    g_printerr ("\n")

#ifdef SL_ENABLE_DEBUG_ALL
  #define SL_ENABLE_DEBUG_BOOK
  #define SL_ENABLE_DEBUG_ENTITY
  #define SL_ENABLE_DEBUG_ATTRIBUTE
  #define SL_ENABLE_DEBUG_UTIL
#endif

#ifdef SL_ENABLE_DEBUG_BOOK
  #define sl_debug_book(format, ...) sl_debug ("Book", format, __VA_ARGS__)
#else
  #define sl_debug_book(format, ...)
#endif
#ifdef SL_ENABLE_DEBUG_ENTITY
  #define sl_debug_entity(format, ...) sl_debug ("Entity", format, __VA_ARGS__)
#else
  #define sl_debug_entity(format, ...)
#endif
#ifdef SL_ENABLE_DEBUG_ATTRIBUTE
  #define sl_debug_attribute(format, ...) sl_debug ("Attribute", format, __VA_ARGS__)
#else
  #define sl_debug_attribute(format, ...)
#endif
#ifdef SL_ENABLE_DEBUG_UTIL
  #define sl_debug_util(format, ...) sl_debug ("Util", format, __VA_ARGS__)
#else
  #define sl_debug_util(format, ...)
#endif

typedef gchar * (*SlPrivUtilToStringFunc)(gpointer data);

ESource *sl_priv_util_get_source (ESourceList *source_tree, const gchar *name);
ESourceGroup *sl_priv_util_source_tree_get_default_group
  (ESourceList *source_tree);

gboolean sl_priv_util_lists_equal (GList *a, GList *b, GEqualFunc equal);
gboolean sl_priv_util_list_contains (GList *list, gpointer data, GEqualFunc equal);
void sl_priv_util_hash_table_print (GHashTable *table, const gchar *title);
void sl_priv_util_hash_table_print_keys (GHashTable *table, const gchar *title);
gchar *sl_priv_util_imcontact_to_string (EmpathyContact *imcontact);
void sl_priv_util_list_print (GList *list, const gchar *title, SlPrivUtilToStringFunc to_string);
void sl_priv_util_eattribute_list_print (GList *eattributes, const gchar *title);
void sl_priv_util_attribute_list_print (GList *attributes, const gchar *title);

gint sl_priv_util_compare_eattributes (EVCardAttribute *a, EVCardAttribute *b);

gboolean sl_priv_util_eattrlist_name_equal (GList *a, GList *b);
gboolean sl_priv_util_eattribute_equal (EVCardAttribute *a, EVCardAttribute *b);
gboolean sl_priv_util_eattrlist_modified (GList *a, GList *b);

void sl_priv_util_list_set_diff (GList *a, GList *b, GEqualFunc equal, GList **added, GList **removed);
void sl_priv_util_list_set_diff_full (GList *a, GList *b, GEqualFunc is_equal, GEqualFunc is_modified, GList **added, GList **removed, GList **modified, GList **rest);

GList *sl_priv_util_eattribute_flat_to_deep_list (GList *flat);
void sl_priv_util_eattribute_deep_list_free (GList *deep);

GList *sl_priv_util_strsplit_list (gchar *string, gchar *delimiter, gint max_tokens);
gchar *sl_priv_util_strjoin_list (gchar *seperator, GList *strings);
GList *sl_priv_util_strv_to_list (gchar **strv);
gchar **sl_priv_util_list_to_strv (GList *strings);

gchar *sl_priv_util_get_account_id_from_imcontact (EmpathyContact *imcontact);
const gchar *sl_priv_util_get_protocol_name_from_account (McAccount *account);

#endif
