/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef SL_ATTRIBUTE_EDS_H
#define SL_ATTRIBUTE_EDS_H

#include "sl-mutual-inclusion.h"

#include <glib.h>
#include <glib-object.h>
#include <libebook/e-book.h>

#define SL_ATTRIBUTE_TYPE           (sl_attribute_get_type ())
#define SL_ATTRIBUTE(obj)           (G_TYPE_CHECK_INSTANCE_CAST (obj, \
  SL_ATTRIBUTE_TYPE, SlAttribute))
#define SL_ATTRIBUTE_CLASS(cls)     (G_TYPE_CHECK_CLASS_CAST (cls, \
  SL_ATTRIBUTE_TYPE, SlAttributeClass))
#define SL_IS_ATTRIBUTE(obj)        (G_TYPE_CHECK_INSTANCE_TYPE (obj, \
  SL_ATTRIBUTE_TYPE))
#define SL_IS_ATTRIBUTE_CLASS(cls)  (G_TYPE_CHECK_CLASS_TYPE (cls, \
  SL_ATTRIBUTE_TYPE))
#define SL_ATTRIBUTE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS(obj, \
  SL_ATTRIBUTE_TYPE))

typedef struct _SlAttributeClass SlAttributeClass;
typedef struct _SlAttributePriv  SlAttributePriv;

struct _SlAttribute
{
  GObject parent;
  gboolean disposed;
  SlAttributePriv *priv;
};

struct _SlAttributeClass
{
  GObjectClass parent;
};

GType sl_attribute_get_type (void);

/* VCard / E-D-S related */
gboolean sl_is_ignored_eattr (const gchar *eattrname);
const gchar *sl_attribute_name_to_eattrname (const gchar *name);
const gchar *sl_attribute_eattrname_to_name (const gchar *eattrname);

/* construction */
void sl_attribute_constr (SlAttribute *self, const gchar *name, gpointer value);
void sl_attribute_constr_with_values (SlAttribute *self, const gchar *name,
                                      GList *values);
void sl_attribute_constr_with_eattr (SlAttribute *self, EVCardAttribute *eattr);
void sl_attribute_constr_with_eattributes (SlAttribute *self, GList *eattributes);
SlAttribute *sl_attribute_new (const gchar *name, gpointer value);
SlAttribute *sl_attribute_new_empty (const gchar *name);
SlAttribute *sl_attribute_new_with_values (const gchar *name, GList *values);
SlAttribute *sl_attribute_new_with_eattr (EVCardAttribute *eattr);
SlAttribute *sl_attribute_new_with_eattributes (GList *eattributes);

/* getters & setters */
const gchar *sl_attribute_get_name (SlAttribute *self);
gint sl_attribute_get_value_count (SlAttribute *self);

/* value manipulation */
void sl_attribute_add (SlAttribute *self, gpointer value);
gpointer sl_attribute_get (SlAttribute *self);
void sl_attribute_set (SlAttribute *self, gpointer value);
gpointer sl_attribute_get_at (SlAttribute *self, gint index);
gboolean sl_attribute_set_at (SlAttribute *self, gint index, gpointer value);
gboolean sl_attribute_remove_at (SlAttribute *self, gint index);
void sl_attribute_set_all (SlAttribute *self, GList *values);
GList *sl_attribute_get_all (SlAttribute *self);
void sl_attribute_remove_all (SlAttribute *self);
void sl_attribute_modified (SlAttribute *self);
gboolean sl_attribute_modified_at (SlAttribute *self, gint index);

void sl_attribute_add_emit (SlAttribute *self, gpointer value, gboolean emit);
gboolean sl_attribute_set_at_emit (SlAttribute *self, gint index, gpointer value, gboolean emit);
gboolean sl_attribute_remove_at_emit (SlAttribute *self, gint index, gboolean emit);
void sl_attribute_remove_all_emit (SlAttribute *self, gboolean emit);
gboolean sl_attribute_modified_at_emit (SlAttribute *self, gint index, gboolean emit);

void sl_attribute_emit_modified (SlAttribute *self);

/* internal getters & setters */
SlEntity *sl_attribute_get_entity (SlAttribute *self);
void sl_attribute_set_entity (SlAttribute *self, SlEntity *entity);
GList *sl_attribute_get_eattributes (SlAttribute *self);

/* eattributes */
void sl_attribute_update_eattributes (SlAttribute *self, GList *eattributes);
void sl_attribute_add_from_eattr (SlAttribute *self, EVCardAttribute *eattr);
void sl_attribute_set_all_from_eattributes (SlAttribute *self, GList *eattributes);

EVCardAttribute *sl_attribute_create_eattr (SlAttribute *self, gpointer value);

#endif
