
#ifndef __sl_marshal_MARSHAL_H__
#define __sl_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:OBJECT,BOOLEAN (marshal.list:1) */
extern void sl_marshal_VOID__OBJECT_BOOLEAN (GClosure     *closure,
                                             GValue       *return_value,
                                             guint         n_param_values,
                                             const GValue *param_values,
                                             gpointer      invocation_hint,
                                             gpointer      marshal_data);

/* VOID:OBJECT,OBJECT,BOOLEAN (marshal.list:2) */
extern void sl_marshal_VOID__OBJECT_OBJECT_BOOLEAN (GClosure     *closure,
                                                    GValue       *return_value,
                                                    guint         n_param_values,
                                                    const GValue *param_values,
                                                    gpointer      invocation_hint,
                                                    gpointer      marshal_data);

/* VOID:OBJECT,OBJECT,POINTER,BOOLEAN (marshal.list:3) */
extern void sl_marshal_VOID__OBJECT_OBJECT_POINTER_BOOLEAN (GClosure     *closure,
                                                            GValue       *return_value,
                                                            guint         n_param_values,
                                                            const GValue *param_values,
                                                            gpointer      invocation_hint,
                                                            gpointer      marshal_data);

G_END_DECLS

#endif /* __sl_marshal_MARSHAL_H__ */

