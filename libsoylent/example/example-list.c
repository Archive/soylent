/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * This example shows how to print a list of all attributes of all people in the
 * default addressbook.
 */

/* TODO: better handling of value printing */

#include "example.h"
#include "libsoylent/soylent.h"

void
list (void)
{
  GError *error = NULL;
  
  /* initialize libsoylent */
  if (!sl_init (&error))
    {
      example_error (error);
    }
  
  /* iterate through all people in the default addressbook */
  GList *people = sl_book_get_people (SL_BOOK_DEFAULT);
  for (; people != NULL; people = people->next)
    {
      
      /* print nick of ´person´ */
      SlPerson *person = people->data;
      g_print ("--- %s ---\n", sl_person_get_nick (person));
      
      /* iterate through all attributes of ´person´ */
      GList *attributes = sl_person_get_attributes (person);
      for (; attributes != NULL; attributes = attributes->next)
        {
          
          /* print attribute name */
          SlAttribute *attr = attributes->data;
          const gchar *attrname = sl_attribute_get_name (attr);
          g_print (" * %s\n", attrname);
          
          /* iterate through all values of ´attr´ */
          GList *values = sl_attribute_get_all (attr);
          int i;
          int length = sl_attribute_get_value_count (attr);
          for (i = 0; i < length; i++)
            {
              
              /* print the value (along with the index) */
              gchar *str = sl_attribute_to_string (attrname, values->data);
              g_print ("   * [%d] %s\n", i, str);
              g_free (str);
              values = values->next;
            }
        }
      
      g_print ("\n");
    }
  
  sl_cleanup ();
}
