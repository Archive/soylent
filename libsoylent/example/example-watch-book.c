/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "example.h"
#include "libsoylent/soylent.h"

#include <glib.h>

static void person_added (SlBook *book, SlPerson *person, gboolean on_purpose, gpointer user_data);
static void person_removed (SlBook *book, SlPerson *person, gboolean on_purpose, gpointer user_data);
static void person_attribute_added (SlBook *book, SlPerson *person, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
static void person_attribute_removed (SlBook *book, SlPerson *person, SlAttribute *attr, gboolean on_purpose, gpointer user_data);
static void person_attribute_modified (SlBook *book, SlPerson *person, SlAttribute *attr, GList *old_values, gboolean on_purpose, gpointer user_data);

static void person_added (SlBook *book, SlPerson *person, gboolean on_purpose,
                          gpointer user_data)
{
  g_print ("person \"%s\" added\n", sl_person_get_nick (person));
}

static void person_removed (SlBook *book, SlPerson *person, gboolean on_purpose,
                            gpointer user_data)
{
  g_print ("person \"%s\" removed\n", sl_person_get_nick (person));
}

static void person_attribute_added (SlBook *book, SlPerson *person,
  SlAttribute *attr, gboolean on_purpose, gpointer user_data)
{
  g_print ("attribute \"%s\" added to \"%s\"\n", sl_attribute_get_name (attr), sl_person_get_nick (person));
}

static void person_attribute_removed (SlBook *book, SlPerson *person,
  SlAttribute *attr, gboolean on_purpose, gpointer user_data)
{
  g_print ("attribute \"%s\" removed from \"%s\"\n", sl_attribute_get_name (attr), sl_person_get_nick (person));
}

static void person_attribute_modified (SlBook *book, SlPerson *person,
  SlAttribute *attr, GList *old_values, gboolean on_purpose, gpointer user_data)
{
  g_print ("attribute \"%s\" from \"%s\" modified\n", sl_attribute_get_name (attr), sl_person_get_nick (person));
}

void
watch_book (void)
{
  GError *error = NULL;
  
  /* initialize libsoylent */
  if (!sl_init (&error))
    {
      example_error (error);
    }
  
  g_object_connect (SL_BOOK_DEFAULT,
    "signal::person-added", person_added, NULL,
    "signal::person-removed", person_removed, NULL,
    "signal::person-attribute-added", person_attribute_added, NULL,
    "signal::person-attribute-removed", person_attribute_removed, NULL,
    "signal::person-attribute-modified", person_attribute_modified, NULL,
    NULL);
  
  GMainLoop *main_loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (main_loop);
  
  sl_cleanup ();
}
