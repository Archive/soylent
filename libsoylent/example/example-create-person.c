/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 *
 */

#include "example.h"
#include "libsoylent/soylent.h"

void
create_person (void)
{
  GError *error = NULL;
  
  /* initialize libsoylent */
  if (!sl_init (&error))
    {
      example_error (error);
    }

  /* create a new person named "Tim Smith" */
  SlPerson *tim = sl_person_new (g_strdup ("Tim"), g_strdup ("Smith"));

  /* add two email-addresses */
  sl_person_add (tim, SL_ATTR_EMAIL, g_strdup ("tim@example.org"));
  sl_person_add (tim, SL_ATTR_EMAIL, g_strdup ("tim@jabber.org"));

  /* store the person in the default addressbook */
  if (!sl_book_add_person (SL_BOOK_DEFAULT, tim, &error))
    {
      example_error (error);
    }

  /* set Tims phone-number */
  sl_person_set (tim, SL_ATTR_TELEPHONE, g_strdup ("007 123456789"));
  
  /* add another phone-number */
  sl_person_add (tim, SL_ATTR_TELEPHONE, g_strdup ("007 987654321"));
  
  /* set Tims address... */
  SlAddress *addr = sl_address_new ();
  addr->street = "Abbey Road";
  addr->country = "Free-World";
  sl_person_set (tim, SL_ATTR_ADDRESS, addr);
  
  /* ... and his birthday */
  GDate *bday = g_date_new_dmy (3, 2, 1980);
  sl_person_set (tim, SL_ATTR_BIRTHDAY, bday);
  
  /* Tims second email-address has changed */
  sl_person_set_at (tim, SL_ATTR_EMAIL, 1, g_strdup ("tim_smith@jabber.org"));
  
  /* give Tim a nice nick-name */
  sl_person_set_nick (tim, g_strdup ("Timmy"));
  /* is the same as sl_person_set (tim, SL_ATTR_NICK, g_strdup ("Timmy")) */
  
  /* add an arbitrary attribute */
  sl_person_set (tim, "mood", g_strdup ("happy"));
  
  /* commit changes of Tim */
  if (!sl_person_commit (tim, &error))
    {
      example_error (error);
    }
  
  /* cleanup */
  g_object_unref (tim);
  sl_cleanup ();
}
