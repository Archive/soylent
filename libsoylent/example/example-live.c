/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * This example shows the live-functionality of libsoylent, e.g. starting a
 * chat with someone or getting the online-presence of a person.
 */

#include "example.h"
#include <libsoylent/soylent.h>

static void print_person_with_presence (SlPerson *person);
static gboolean start_random_chat (gpointer data);
static void person_presence_changed (SlBook *book, SlPerson *person, gpointer user_data);

/*
 * Prints a person in the form:
 *  * [presence] name
 * e.g.:
 *  * [on] Tim
 */
static void
print_person_with_presence (SlPerson *person)
{
  g_return_if_fail (person != NULL && SL_IS_PERSON (person));

  gchar *presence_string = NULL;

  /* get a printable name and the online-presence of this person */
  gchar *name = sl_person_get_string (person);
  McPresence presence = sl_person_get_presence (person);
  
  /* print presence as on(line), aw(ay) or of(fline) */
  if (presence == MC_PRESENCE_UNSET || presence == MC_PRESENCE_OFFLINE ||
      presence == MC_PRESENCE_HIDDEN)
    {
      presence_string = "of";
    }
  if (presence == MC_PRESENCE_EXTENDED_AWAY || presence == MC_PRESENCE_AWAY ||
      presence == MC_PRESENCE_DO_NOT_DISTURB)
    {
      presence_string = "aw";
    }
  if (presence == MC_PRESENCE_AVAILABLE)
    {
      presence_string = "on";
    }
  
  /* print person with presence */
  g_print ("[%s] %s\n", presence_string, name);
}

/*
 * Starts a chat with a randomly choosen person that is currently online. This
 * is a timeout-function, i.e. returning TRUE means that the function should be
 * called again after 5 seconds.
 */
static gboolean
start_random_chat (gpointer data)
{
  GError *error = NULL;
  int index = 0;
  SlPerson *person = NULL;
  
  /* get a list of people that are online */
  GList *online_people = sl_book_get_online_people (SL_BOOK_DEFAULT);
  
  /* if nobody is online, retry in 5 seconds */
  if (online_people == NULL)
    {
      return TRUE;
    }
  
  /* choose a person randomly */
  index = g_random_int_range (0, g_list_length (online_people));
  person = g_list_nth_data (online_people, index);
  
  /* launch a chat with the chosen person */
  g_print ("launching a random chat with %s\n", sl_person_get_string (person));
  if (!sl_person_communicate_chat (person, &error))
    {
      example_error (error);
    }
  
  g_list_free (online_people);
  return FALSE;
}

static void
person_presence_changed (SlBook *book, SlPerson *person, gpointer user_data)
{
  print_person_with_presence (person);
}

void
live (void)
{
  GError *error = NULL;
  GMainLoop *main_loop = NULL;
  GList *people = NULL;
  
  /* initialize libsoylent */
  if (!sl_init (&error))
    {
      example_error (error);
    }
  
  /* get notified when the presence of someone changed */
  g_signal_connect (SL_BOOK_DEFAULT, "person-presence-changed", G_CALLBACK (person_presence_changed), NULL);
  
  /* print the name and presence of all people... */
  people = sl_book_get_people (SL_BOOK_DEFAULT);
  for (; people != NULL; people = people->next)
    {
      SlPerson *person = people->data;
      
      /* ... but only where this information is available */
      if (sl_person_has_iminfo (person))
        {
          print_person_with_presence (person);
        }
    }
  
  /* add \"Tom Beatle\" to the addressbook */
  SlPerson *tom = sl_person_new (g_strdup ("Tom"), g_strdup ("Beatle"));
  if (!sl_book_add_person (SL_BOOK_DEFAULT, tom, &error))
    {
      example_error (error);
    }
  
  /* Set Tom's Jabber account. Now online-information of Tom should be
   * available.*/
  sl_person_add (tom, SL_ATTR_JABBER, "foobar@jabber.org");
  
  /* Launch a random chat with someone. If no-one is online, retry every 5
   * seconds. */
  g_timeout_add_seconds (5, start_random_chat, NULL);
  
  /* run... */
  main_loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (main_loop);
  
  sl_cleanup ();
}
