/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* TODO: will be splitted to SlEntity (interface), SlEntityBase (abstract-
 * base class), SlEntityEDS. for now this will be misused as SlEntityEDS */

#ifndef SL_ENTITY_EDS_H
#define SL_ENTITY_EDS_H

#include "sl-mutual-inclusion.h"

#include <glib.h>
#include <glib-object.h>
#include <libebook/e-book.h>
#include <libempathy/empathy-contact.h>

#define SL_ENTITY_TYPE			 	    (sl_entity_get_type ())
#define SL_ENTITY(obj)				    (G_TYPE_CHECK_INSTANCE_CAST (obj, \
	SL_ENTITY_TYPE, SlEntity))
#define SL_ENTITY_CLASS(cls)		  (G_TYPE_CHECK_CLASS_CAST (cls, \
	SL_ENTITY_TYPE, SlEntityClass))
#define SL_IS_ENTITY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, \
	SL_ENTITY_TYPE))
#define SL_IS_ENTITY_CLASS(cls)	  (G_TYPE_CHECK_CLASS_TYPE (cls, \
  SL_ENTITY_TYPE))
#define SL_ENTITY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, \
	SL_ENTITY_TYPE))

typedef struct _SlEntityClass SlEntityClass;
typedef struct _SlEntityPriv  SlEntityPriv;

typedef enum _SlEntityProperty SlEntityProperty;

enum _SlEntityProperty
{
  SL_ENTITY_PROPERTY_ENTITY_HANDLER = 1
};

struct _SlEntity
{
  GObject parent;
  gboolean disposed;
  SlEntityPriv *priv;
};

struct _SlEntityClass
{
  GObjectClass parent;
};

GType sl_entity_get_type (void);

/*SlEntity *sl_entity_new (SlEntityHandler *entity_handler);*/
/*SlEntityHandler *sl_entity_get_handler (SlEntity *self);*/
void sl_entity_constr (SlEntity *self);
void sl_entity_constr_with_econtact (SlEntity *self, EContact *econtact);
EContact *sl_entity_get_econtact (SlEntity *self);
void sl_entity_set_econtact (SlEntity *self, EContact *econtact);

void sl_entity_associate_imcontacts (SlEntity *self);
void sl_entity_associate_imcontact (SlEntity *self, EmpathyContact *imcontact, gboolean mix_presences);
void sl_entity_deassociate_imcontact (SlEntity *self, EmpathyContact *imcontact);

SlBook *sl_entity_get_storage (SlEntity *self);
void sl_entity_set_storage (SlEntity *self, SlBook *storage);

gboolean sl_entity_commit (SlEntity *self, GError **error);

gboolean sl_entity_has_iminfo (SlEntity *self);
gboolean sl_entity_is_online (SlEntity *self);
McPresence sl_entity_get_presence (SlEntity *self);
GList *sl_entity_get_imcontacts (SlEntity *self);
EmpathyContact *sl_entity_get_imcontact_from_id (SlEntity *self, const gchar *id);
EmpathyContact *sl_entity_get_imcontact_from_account_id (SlEntity *self, const gchar *account_id);

void sl_entity_add_attribute_shallow (SlEntity *self, SlAttribute *attr);
void sl_entity_remove_attribute_shallow (SlEntity *self, SlAttribute *attr);

void sl_entity_add_attribute (SlEntity *self, SlAttribute *attr);
void sl_entity_remove_attribute (SlEntity *self, SlAttribute *attr);
gboolean sl_entity_has_attribute (SlEntity *self, const gchar *attrname);
SlAttribute *sl_entity_get_attribute (SlEntity *self, const gchar *attrname);
GList *sl_entity_get_attributes (SlEntity *self);

SlAttribute *sl_entity_get_attribute_from_eattr (SlEntity *self, EVCardAttribute *eattr);
SlAttribute *sl_entity_get_attribute_create (SlEntity *self, const gchar *attrname);

void sl_entity_add (SlEntity *self, const gchar *attrname, gpointer value);
gpointer sl_entity_get (SlEntity *self, const gchar *attrname);
void sl_entity_set (SlEntity *self, const gchar *attrname, gpointer value);
gpointer sl_entity_get_at (SlEntity *self, const gchar *attrname, gint index);
gboolean sl_entity_set_at (SlEntity *self, const gchar *attrname, gint index, 
                           gpointer value);
gboolean sl_entity_remove_at (SlEntity *self, const gchar *attrname,
                              gint index);
void sl_entity_set_all (SlEntity *self, const gchar *attrname, GList *values);
GList *sl_entity_get_all (SlEntity *self, const gchar *attrname);
void sl_entity_remove_all (SlEntity *self, const gchar *attrname);
void sl_entity_modified (SlEntity *self, const gchar *attrname);
gboolean sl_entity_modified_at (SlEntity *self, const gchar *attrname, gint index);

#endif
