/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* TODO
 * test1: add attributes and values to a predefined addressbrook and check if all
 * changes are as expected
 * test2: run test1 and check if all signals are emitted correctly; after that
 * check like in test1 if all changes are as expected
 *
 * signal-tests:
 * [x] add person
 * [x] remove person
 * [x] add attribute w/ 1 value
 * [x] add attribute w/ x values
 * [x] remove attribute w/ 1 value
 * [x] remove attribute w/ x values
 * [x] modify: modify value
 * [x] modify: add and remove values to / from attribute
 * [x] + some combinations of the above
 */

#include "test.h"

int
main (int argc, char **argv)
{
  /* disabled for release */
  test_success ();
  
  GError *error = NULL;
  
  test_init ("Playground");
  test_soylent_init ();
  
  SlBook *testbook = test_setup_testbook ();
  
  /* print all attributes from all people
  GList *people = sl_book_get_people (testbook);
  for (; people != NULL; people = people->next)
    {
      SlPerson *person = people->data;
      test_print_attributes (person);
    }
  */

  test_print ("creating some people");
  SlPerson *person_tim = sl_person_new ("Tim", "Smith");
  SlPerson *person_joe = sl_person_new ("Joe", "Miller");
  
  test_print ("adding \"Tim\" and \"Joe\" to the addressbook");
  if (!sl_book_add_person (testbook, person_tim, &error) ||
      !sl_book_add_person (testbook, person_joe, &error))
    {
      test_fail ("couldn't add person to addressbook: %s", error->message);
    }
  
  test_print ("removing \"Tim\" from the addressbook again");
  if (!sl_book_remove_person (testbook, person_tim, &error))
    {
      test_fail ("couldn't remove person from addressbook: %s", error->message);
    }
  
  SlAttribute *attr = sl_attribute_new_empty ("url");
  sl_attribute_add (attr, "http://www.kalterregen.de");
  sl_attribute_add (attr, "http://planet.gnome.org");
  
  if (!sl_attribute_set_at (attr, 0, "http://www.firefox.com"))
    {
      test_fail ("couldn't set value at a given index");
    }
  
  sl_entity_add_attribute (SL_ENTITY (person_joe), attr);
  g_object_unref (attr);
  
  if (!sl_person_commit (person_joe, &error))
    {
      test_fail ("couldn't commit changes of \"Joe\": %s", error->message);
    }
  
  g_object_unref (person_tim);
  g_object_unref (person_joe);
  
  test_clean_testbook (testbook);
  
  /*test_print ("foo\n");
  SlEntity *entity = sl_entity_new ();
  test_print ("%s\n", (gchar *) sl_entity_get (entity, "x"));
  
  SlEntityHandlerEDS *entity_handler = sl_entity_handler_eds_new ();
  sl_entity_handler_eds_get (entity_handler, "foo");
  sl_entity_handler_get (SL_ENTITY_HANDLER (entity_handler), "foo");*/
  
  test_success ();
}
