/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "config.h"
#include "test.h"

void
test_soylent_init (void)
{
  GError *error = NULL;
  if (!sl_init (&error))
    {
      /* TODO: quit test here... */
      g_warning ("failed to initialize libsoylent");
    }
}

SlBook *
test_setup_testbook (void)
{
  /* workaround for known bug 3 */
  SlBook *t = SL_BOOK_DEFAULT;
  return t;
  
  GError *error = NULL;
  const gchar *testbookname = "testbook";
  SlBook *testbook = sl_book_create (testbookname, &error);
  if (testbook == NULL)
    {
      /* TODO: quit test here...*/
      g_warning ("couldn't create addressbook: %s", error->message);
    }
  return testbook;
}

void 
test_clean_testbook (SlBook *testbook)
{
  /* workaround for known bug 3: do nothing */
  return;
  GError *error = NULL;
  if (!sl_book_delete ("testbook", &error))
    {
      /* TODO: quit test here...*/
      g_warning ("couldn't delete addressbook: %s", error->message);
    }
  g_object_unref (testbook);
}

void
test_print_attributes (SlPerson *person)
{
  test_print ("--- %s ---", sl_person_get_nick (person));
  GList *attributes = sl_entity_get_attributes (SL_ENTITY (person));
  for (; attributes != NULL; attributes = attributes->next)
    {
      SlAttribute *attr = attributes->data;
      test_print_attribute (attr);
    }
}

void
test_print_attribute (SlAttribute *attr)
{
  test_print (" * %s", sl_attribute_get_name (attr));
  GList *values = sl_attribute_get_all (attr);
  int i = 0;
  int length = g_list_length (values);
  for (i = 0; i < length; i++)
    {
      /* TODO: this is only possible because we know all attributes are
       * strings. In future some to_string () functionality is needed?
       */
      test_print ("   * %s", (gchar *) values->data);
      values = values->next;
    }
}
