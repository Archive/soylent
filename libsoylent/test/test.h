/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef TEST_H
#define TEST_H

#include <glib.h>
#include <stdlib.h>
#include <libsoylent/soylent.h>

/* TODO: would this work without multiple g_prints? */
#define test_init(name) test_name = name; \
                        g_print ("running test %s...\n", test_name)
#define test_success()  g_print ("test %s succeded\n", test_name); \
                        return EXIT_SUCCESS
#define test_fail(...)  g_print ("test %s failed: ", test_name); \
                        g_print (__VA_ARGS__); \
                        g_print ("\n"); \
                        return EXIT_FAILURE
#define test_print(...) g_print ("  "); \
                        g_print (__VA_ARGS__); \
                        g_print ("\n")

const gchar *test_name;

void test_soylent_init (void);
SlBook *test_setup_testbook (void);
void test_clean_testbook (SlBook *testbook);
void test_print_attributes (SlPerson *person);
void test_print_attribute (SlAttribute *attr);

#endif
