/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "test.h"

static void print_books (GList *books);

int
main (int argc, char **argv)
{
  /* disabled because of addressbook-bug */
  test_success ();
  
  GError *error = NULL;
  const gchar *testbookname = "testbook";
  const gchar *testbookname_wrong = "testbook23";
  
  test_init ("Book");
  
  GList *books = sl_book_get_books ();
  gint book_count = g_list_length (books);
  test_print ("%d book(s) already exist(s)", book_count);
  g_list_free (books);
  
  test_print ("creating addressbook \"%s\"", testbookname);
  if (!sl_book_create (testbookname, &error)) {
    test_fail("couldn't create addressbook: %s", error->message);
  }
  
  SlBook *book = sl_book_create (testbookname, &error);
  if (book)
    {
      test_fail("created book with same name twice");
    } 
  else
    {
      if (error->code != SL_BOOK_ERROR_ALREADY_EXISTS)
        {
          test_fail ("unexpected error: %s", error->message);
        }
      g_clear_error (&error);
    }
  
  if (!sl_book_exists (testbookname))
    {
      test_fail ("created addressbook, but it doesn't seem to exist");
    }
  
  books = sl_book_get_books ();
  print_books (books);
  if (g_list_length (books) != book_count + 1)
    {
      test_fail ("new addressbook not in list of addressbooks");
    }
  book_count = g_list_length (books);
  g_list_free (books);
  
  test_print ("opening addressbook");
  if (!sl_book_open (testbookname, &error))
    {
      test_fail ("couldn't open addressbook: %s", error->message);
    }
  
  if (!sl_book_open (testbookname_wrong, &error))
    {
      if (error->code != SL_BOOK_ERROR_NOT_EXISTING)
        {
          test_fail ("unexpected error: %s", error->message);
        }
      g_clear_error (&error);
    }
  else
    {
      test_fail ("opening a non-existant addressbook succeded");
    }
  
  test_print ("deleting addressbook again");
  if (!sl_book_delete (testbookname, &error))
    {
      test_fail ("couldn't delete addressbook: %s", error->message);
    }
    
  books = sl_book_get_books ();
  print_books (books);
  if (g_list_length (books) != book_count - 1)
    {
      /* TODO:
       * there seems to be a synchronizing problem here: sometimes the list
       * still contains the deleted addressbook, sometimes it does not...
       * needs further investigation
       */
      /* test_fail ("deleted addressbook is still in list of addressbooks"); */
      g_warning ("deleted addressbook is still in list of addressbooks");
    }
  book_count = g_list_length (books);
  g_list_free (books);
  
  if (sl_book_exists (testbookname))
    {
      /*test_fail ("addressbook deleted, but it seems to still exist");*/
      g_warning ("addressbook deleted, but it seems to still exist");
    }
  
  if (!sl_book_exists (testbookname_wrong))
    {
      if (!sl_book_delete (testbookname_wrong, &error))
        {
          if (error->code != SL_BOOK_ERROR_NOT_EXISTING)
            {
              test_print ("unexpected error: %s", error->message);
            }
          g_clear_error (&error);
        }
      else
        {
          test_fail ("deleting a non-existant addressbook succeded");
        }
    }
  else
    {
      test_print ("skipping addressbook-deletion-error-test because an \
addressbook \"%s\" exists", testbookname_wrong);
    }
  
  test_success ();
}

static
void print_books (GList *books)
{
  test_print ("addressbooks:");
  GList *books_iter = books;
  for (; books_iter != NULL; books_iter = books_iter->next)
    {
      gchar *book = books_iter->data;
      test_print (" * %s", book);
    }
}
