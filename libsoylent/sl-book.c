/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * SECTION:sl-book
 * @short_description: the addressbook
 * @stability: Unstable
 * @include: sl-book.h
 *
 * #SlBook is basically like a real-world addressbook, i.e. it is responsible
 * for managing people (adding and removing them to / from the addressbook).
 * Searching for people is also possible. Furthermore SlBook contains utility
 * functions to create, open and delete addressbooks.
 */

#include "sl-mutual-inclusion.h"
#include "sl-priv-util.h"
#include "sl-marshal.h"
#include "soylent.h"

#include <libempathy/empathy-contact-manager.h>
#include <libempathy/empathy-contact-list.h>
#include <libempathy/empathy-utils.h>
#include <dbus/dbus-glib.h>

/* private structs and fields */

struct _SlBookPriv
{
  GError *error;
  EBook *ebook;
  EBookView *eview;
  GMainLoop *view_wait_loop;
  GList *people;
};

static GObjectClass *parent_class = NULL;

static void sl_book_class_init (gpointer g_class, gpointer class_data);
static void sl_book_init (GTypeInstance *instance, gpointer g_class);
static void sl_book_dispose (GObject *object);
static void sl_book_set_property (SlBook *self, guint property_id,
  const GValue *value, GParamSpec *pspec);
static void sl_book_get_property (GObject *object, guint property_id,
  GValue *value, GParamSpec *pspec);
static SlBook *sl_book_new (EBook *ebook, GError **error);
static SlBook *sl_book_new_from_source (ESource *source, GError **error);

static void eview_contacts_added (EBookView *eview, GList *econtacts, 
                                  SlBook *self);
static void eview_contacts_changed (EBookView *eview, GList *econtacts, 
                                    SlBook *self);
static void eview_contacts_removed (EBookView *eview, GList *econtact_ids, 
                                    SlBook *self);
static void eview_sequence_complete (EBookView *eview, EBookViewStatus status, 
                                     SlBook *self);

static void empathy_contact_list_members_changed (
  EmpathyContactList *contactlist, EmpathyContact *contact,
  EmpathyContact *actor, guint reason, gchar *message, gboolean is_member,
  SlBook *self);

GType
sl_book_get_type (void)
{
  static GType type = 0;
  static const GTypeInfo info =
    {
      sizeof (SlBookClass),
      NULL,
      NULL,
      sl_book_class_init,
      NULL,
      NULL,
      sizeof (SlBook),
      0,
      sl_book_init,
      NULL
    };
  
  if (type == 0)
    {
      type = g_type_register_static (G_TYPE_OBJECT, "SlBook", &info, 0);
    }
  return type;
}

GQuark
sl_book_error_quark (void)
{
  return g_quark_from_static_string ("sl-book-error");
}

static void
sl_book_class_init (gpointer g_class, gpointer class_data)
{
  parent_class = g_type_class_peek (g_type_parent (SL_BOOK_TYPE));
  g_assert (parent_class != NULL);
  
  GObjectClass *obj_class = G_OBJECT_CLASS (g_class);
  obj_class->dispose = sl_book_dispose;
  obj_class->get_property = (void (*)(GObject *, guint, GValue *, GParamSpec *))
                              sl_book_get_property;
  obj_class->set_property = (void (*)(GObject *, guint, const GValue *,
                              GParamSpec *)) sl_book_set_property;
  
  GParamSpec *pspec = NULL;
  
  /**
   * SlBook:ebook:
   *
   * The underlying #EBook (private).
   */
  pspec = g_param_spec_pointer ("ebook", "ebook", "EBook backend", 
            G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);
  g_object_class_install_property (obj_class, SL_BOOK_PROPERTY_EBOOK, pspec);
  
  /**
   * SlBook::added:
   * @book: the book that received the signal
   * @foo: yeah, foo
   *
   * Emitted when a person is added to the addressbook.
   */
  
  
  g_signal_new ("modified", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__BOOLEAN, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
  g_signal_new ("person-added", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_BOOLEAN, G_TYPE_NONE, 2, SL_PERSON_TYPE, G_TYPE_BOOLEAN);
  g_signal_new ("person-removed", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_BOOLEAN, G_TYPE_NONE, 2, SL_PERSON_TYPE, G_TYPE_BOOLEAN);
  g_signal_new ("person-modified", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_BOOLEAN, G_TYPE_NONE, 2, SL_PERSON_TYPE, G_TYPE_BOOLEAN);
  g_signal_new ("person-attribute-added", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_OBJECT_BOOLEAN, G_TYPE_NONE, 3, SL_PERSON_TYPE, SL_ATTRIBUTE_TYPE, G_TYPE_BOOLEAN);
  g_signal_new ("person-attribute-removed", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_OBJECT_BOOLEAN, G_TYPE_NONE, 3, SL_PERSON_TYPE, SL_ATTRIBUTE_TYPE, G_TYPE_BOOLEAN);
  g_signal_new ("person-attribute-modified", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, sl_marshal_VOID__OBJECT_OBJECT_POINTER_BOOLEAN, G_TYPE_NONE, 4, SL_PERSON_TYPE, SL_ATTRIBUTE_TYPE, G_TYPE_POINTER, G_TYPE_BOOLEAN);
  g_signal_new ("person-presence-changed", SL_BOOK_TYPE, G_SIGNAL_RUN_LAST, 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, SL_PERSON_TYPE);
}

static void
sl_book_init (GTypeInstance *instance, gpointer g_class)
{
  SlBook *self = SL_BOOK (instance);
  SlBookPriv *priv = g_new (SlBookPriv, 1);
  self->priv = priv;
  self->priv->ebook = NULL;
  self->priv->error = NULL;
  self->disposed = FALSE;
}

static void
sl_book_dispose (GObject *object)
{
  SlBook *self = SL_BOOK (object);
  g_return_if_fail (!self->disposed);
  
  g_error_free (self->priv->error);
  g_object_unref (self->priv->ebook);
  g_free (self->priv);
  self->disposed = TRUE;
  
  parent_class->dispose (object);
}

static void
sl_book_set_property (SlBook *self, guint property_id, const GValue *value,
  GParamSpec *pspec)
{
  switch (property_id)
    {
      case SL_BOOK_PROPERTY_EBOOK:
        self->priv->ebook = g_object_ref (g_value_get_pointer (value));
        break;
      default:
        g_assert_not_reached ();
        break;
    }
}

static void
sl_book_get_property (GObject *object, guint property_id, GValue *value,
  GParamSpec *pspec)
{
  g_warning("%s not implemented", __FUNCTION__);
}

gboolean
sl_book_setup (GError **error)
{ 
  sl_book_default = sl_book_open_default (error);
  if (sl_book_default == NULL)
    {
      return FALSE;
    }
  return TRUE;
}

GList *
sl_book_get_books (void)
{
  GList *books = NULL;
  
  GError *error = NULL;
  ESourceList *source_tree = NULL;
  if (!e_book_get_addressbooks (&source_tree, &error))
    {
      g_critical ("failed get source tree: %s", error->message);
      return NULL;
    }
  
  GSList *groups = e_source_list_peek_groups (source_tree);
  for (; groups != NULL; groups = groups->next)
    {
      ESourceGroup *group = groups->data;
      GSList *sources = e_source_group_peek_sources (group);
      
      /* debug
      const char *group_name = e_source_group_peek_name (group); */
      
      for (; sources != NULL; sources = sources->next)
        {
          ESource *source = sources->data;
          const gchar *name = e_source_peek_name (source);
          books = g_list_append (books, (gpointer) name);
          
          /* debug
          const gchar *reluri = e_source_peek_relative_uri(source);
          const gchar *absuri = e_source_peek_absolute_uri(source);
          g_debug ("%s.%s [%s, %s]", group_name, name, reluri, absuri); */
        }
    }
    
  g_object_unref (source_tree);  
  return books;
}

/**
 * sl_book_exists:
 *
 * Deprecated:
 *
 * Since: v0.2.0
 */
gboolean
sl_book_exists (const gchar *name)
{
  GError *error = NULL;
  ESourceList *source_tree = NULL;
  if (!e_book_get_addressbooks (&source_tree, &error))
    {
      g_critical ("failed to get source-tree: %s", error->message);
      return FALSE;
    }
  
  ESource *source = sl_priv_util_get_source (source_tree, name);
  g_object_unref (source_tree);
  return (source != NULL);
}

/**
 * sl_book_create:
 * @name: name of the new addressbook
 * @error: return location for a GError or NULL
 *
 * Creates a new addressbook with the given @name.
 * There is also a sl_book_open() function. And have you seen our
 * #SlAttributeHandlerType? Or our %foobar?
 *
 * Returns: the created addressbook, or NULL on error. The created addressbook
 * is immediatly ready for use.
 */
SlBook *
sl_book_create (const gchar *name, GError **error)
{
  g_error ("%s disabled because of addressbook-bug", __FUNCTION__);
  
  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);
  
  if (sl_book_exists (name))
    {
      g_set_error (error, SL_BOOK_ERROR, SL_BOOK_ERROR_ALREADY_EXISTS,
        "The book \"%s\" already exists.", (gchar *) name);
      return NULL;
    }
  
  ESourceList *source_tree = NULL;
  if (!e_book_get_addressbooks (&source_tree, error))
    {
      return NULL;
    }
  ESourceGroup *default_group = sl_priv_util_source_tree_get_default_group
                                  (source_tree);
  
  ESource *source = e_source_new (name, "");
  e_source_group_add_source (default_group, source, -1);
  if (!e_source_list_sync (source_tree, error))
    {
      return NULL;
    }
  
  SlBook *book = sl_book_new_from_source (source, error);
  if (!book)
    {
      return NULL;
    }
  
  /* we don't need the source-tree anymore _after_ the book has been created */
  g_object_unref (source_tree);
    
  return book;
}

SlBook *sl_book_open (const gchar *name, GError **error)
{
  g_error ("%s disabled because of addressbook-bug", __FUNCTION__);
  
  ESourceList *source_tree = NULL;
  if (!e_book_get_addressbooks (&source_tree, error))
    {
      return NULL;
    }
  ESource *source = sl_priv_util_get_source (source_tree, name);
  if (source == NULL)
    {
      g_set_error (error, SL_BOOK_ERROR, SL_BOOK_ERROR_NOT_EXISTING,
        "There is no book \"%s\".", (gchar *) name);
      return NULL;
    }
  
  return sl_book_new_from_source (source, error);
}

SlBook *sl_book_open_default (GError **error)
{
  EBook *ebook = e_book_new_system_addressbook (error);
  if (ebook == NULL)
    {
      return NULL;
    }
  if (!e_book_open (ebook, FALSE, error))
    {
      return NULL;
    }
  
  return sl_book_new (ebook, error);
}

gboolean
sl_book_delete (const gchar *name, GError **error)
{
  g_error ("%s disabled because of addressbook-bug", __FUNCTION__);
  
  /* TODO: forbid to delete default addressbook */
  
  g_return_val_if_fail (name != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  
  ESourceList *source_tree = NULL;
  if (!e_book_get_addressbooks (&source_tree, error))
    {
      return FALSE;
    }
  
  ESource *source = sl_priv_util_get_source (source_tree, name);
  if (source == NULL)
    {
      g_set_error (error, SL_BOOK_ERROR, SL_BOOK_ERROR_NOT_EXISTING,
        "There is no book \"%s\".", (gchar *) name);
      return FALSE;
    }
  
  /* get ebook and delete its related data */
  EBook *ebook = e_book_new (source, error);
  if (ebook == NULL)
    {
      return FALSE;
    }
  gboolean removed = e_book_remove (ebook, error);
  g_object_unref (ebook);
  if (!removed)
    {
      return FALSE;
    }
  
  /* remove the source from the source-tree */
  ESourceGroup *group = e_source_peek_group (source);
  e_source_group_remove_source (group, source);
  if (!e_source_list_sync (source_tree, error))
    {
      return FALSE;
    }
  g_object_unref (source_tree);
  
  return TRUE;
}

gboolean
sl_book_constr (SlBook *self, GError **error)
{
  self->priv->people = NULL;
  self->priv->eview = NULL;
  self->priv->view_wait_loop = g_main_loop_new (NULL, FALSE);
  
  sl_debug_book ("starting to load people from book%s", "");
  
  /* this will query all contacts */
  EBookQuery *query = e_book_query_any_field_contains ("");
  
  /* TODO: Here you can query only certain fields, this would be a great
   * performance improvement. But we definitely want a list of all people and
   * for every person a list of all attributes. Is this possible without
   * querying all fields? */
  /* TODO: this would also need some refactoring for SlAttribute to support
   * lazy loading */
  if (!e_book_get_book_view (self->priv->ebook, query, NULL, 0, 
                             &self->priv->eview, error))
    {
      return FALSE;
    }
  
  g_object_connect (self->priv->eview,
    "signal::contacts-added", G_CALLBACK (eview_contacts_added), self,
    "signal::contacts-changed", G_CALLBACK (eview_contacts_changed), self,
    "signal::contacts-removed", G_CALLBACK (eview_contacts_removed), self,
    "signal::sequence-complete", G_CALLBACK (eview_sequence_complete), self,
    NULL);
  
  e_book_view_start (self->priv->eview);
  g_main_loop_run (self->priv->view_wait_loop);
  
  sl_debug_book ("associating loaded people with loaded IM-contacts%s", "");

  GList *people_iter = self->priv->people;
  for (; people_iter != NULL; people_iter = people_iter->next)
    {
      SlPerson *person = people_iter->data;
      sl_entity_associate_imcontacts (SL_ENTITY (person));
    }
  
  EmpathyContactManager *contactmgr = sl_im_get_contact_manager ();
  g_signal_connect (contactmgr, "members-changed", G_CALLBACK (empathy_contact_list_members_changed), self);
  
  return TRUE;
}

static SlBook *
sl_book_new (EBook *ebook, GError **error)
{
  g_return_val_if_fail (ebook != NULL && E_IS_BOOK (ebook), NULL);
  SlBook *self = g_object_new (SL_BOOK_TYPE, "ebook", ebook, NULL);
  sl_book_constr (self, error);
  return self;
}

static SlBook *
sl_book_new_from_source (ESource *source, GError **error)
{
  g_return_val_if_fail (source != NULL && E_IS_SOURCE (source), NULL);
  
  EBook *ebook = e_book_new (source, error);
  if (!ebook)
    {
      return NULL;
    }
  if (!e_book_open (ebook, FALSE, error))
    {
      g_object_unref (ebook);
      return NULL;
    }
  
  SlBook *self = sl_book_new (ebook, error);
  g_object_unref (ebook);
  return self;
}

gboolean
sl_book_add_person (SlBook *self, SlPerson *person, GError **error)
{
  /* TODO: adding a contact to a self-created addressbook kills E-D-S and hangs
     here... */
  
  SlEntity *entity = SL_ENTITY (person);
  if (!e_book_add_contact (self->priv->ebook, sl_entity_get_econtact (entity),
                           error))
    {
      return FALSE;
    }
  
  sl_entity_set_storage (entity, self);
  
  self->priv->people = g_list_append (self->priv->people, person);
  
  return TRUE;
}

gboolean
sl_book_remove_person (SlBook *self, SlPerson *person, GError **error)
{
  EContact *econtact = sl_entity_get_econtact (SL_ENTITY (person));
  const char* id = e_contact_get (econtact, E_CONTACT_UID);
  if (!e_book_remove_contact (self->priv->ebook, id, error))
    {
      return FALSE;
    }
  
  self->priv->people = g_list_remove (self->priv->people, person);
  
  return TRUE;
}

gboolean
sl_book_commit_person (SlBook *self, SlPerson *person, GError **error)
{
  EContact *econtact = sl_entity_get_econtact (SL_ENTITY (person));
  return e_book_commit_contact (self->priv->ebook, econtact, error);
}

GList *
sl_book_get_people (SlBook *self)
{
  /* TODO: make this with a libsoylent-query? */
  return self->priv->people;
}

/**
 * sl_book_get_online_people:
 * @self: a #SlBook
 *
 * Gets a list of all people from addressbook @self that are online.
 *
 * Returns: a #SlPerson list of online people. The list should be freed with
 * g_list_free().
 */
GList *
sl_book_get_online_people (SlBook *self)
{
  GList *online_people = NULL;
  GList *people = self->priv->people;
  for (; people != NULL; people = people->next)
    {
      SlPerson *person = people->data;
      if (sl_entity_is_online (SL_ENTITY (person)))
        {
          online_people = g_list_append (online_people, person);
        }
    }
  return online_people;
}

SlPerson *
sl_book_get_person (SlBook *self, const gchar *attrname, gpointer value)
{
  /* TODO: this works only for strings atm */
  GList *people = self->priv->people;
  for (; people != NULL; people = people->next)
    {
      SlPerson *person = people->data;
      if (g_str_equal ((gchar *) value, (gchar *) sl_person_get (person, attrname)))
        {
          return person;
        }
    }
  
  return NULL;
}

void
sl_book_emit_person_presence_changed (SlBook *self, SlPerson *person)
{
  g_return_if_fail (self != NULL && SL_IS_BOOK (self));
  g_return_if_fail (person != NULL && SL_IS_PERSON (person));

  g_signal_emit_by_name (self, "person-presence-changed", person);
}

void
sl_book_emit_person_added (SlBook *self, SlPerson *person, gboolean on_purpose)
{
  g_signal_emit_by_name (self, "person-added", person, on_purpose);
  g_signal_emit_by_name (self, "modified", on_purpose);
}

void
sl_book_emit_person_removed (SlBook *self, SlPerson *person, gboolean on_purpose)
{
  g_signal_emit_by_name (self, "person-removed", person, on_purpose);
  g_signal_emit_by_name (self, "modified", on_purpose);
}

void
sl_book_emit_person_attribute_added (SlBook *self, SlPerson *person, SlAttribute *attr, gboolean on_purpose)
{
  g_signal_emit_by_name (self, "person-attribute-added", person, attr, on_purpose);
  g_signal_emit_by_name (self, "person-modified", person, on_purpose);
  g_signal_emit_by_name (self, "modified", on_purpose);
}

void
sl_book_emit_person_attribute_removed (SlBook *self, SlPerson *person, SlAttribute *attr, gboolean on_purpose)
{
  g_signal_emit_by_name (self, "person-attribute-removed", person, attr, on_purpose);
  g_signal_emit_by_name (self, "person-modified", person, on_purpose);
  g_signal_emit_by_name (self, "modified", on_purpose);
}

void
sl_book_emit_person_attribute_modified (SlBook *self, SlPerson *person, SlAttribute *attr, GList *old_values, gboolean on_purpose)
{
  g_signal_emit_by_name (self, "person-attribute-modified", person, attr, old_values, on_purpose);
  g_signal_emit_by_name (self, "person-modified", person, on_purpose);
  g_signal_emit_by_name (self, "modified", on_purpose);
}

static void
eview_contacts_added (EBookView *eview, GList *econtacts, SlBook *self)
{
  /* TODO: consider to forget econtact here again */
  if (self->priv->view_wait_loop != NULL)
    {
      sl_debug_book ("loading %d person / people", g_list_length (econtacts));
    }
  
  sl_debug_book ("%d econtact(s) added", g_list_length (econtacts));
  
  for (; econtacts != NULL; econtacts = econtacts->next)
    {
      EContact *econtact = econtacts->data;
      SlPerson *person = sl_person_new_with_econtact (econtact);
      sl_entity_set_storage (SL_ENTITY (person), self);
      self->priv->people = g_list_append (self->priv->people, person);

      if (self->priv->view_wait_loop == NULL)
        {
          /* im stuff */
          sl_entity_associate_imcontacts (SL_ENTITY (person));
          
          sl_book_emit_person_added (self, person, FALSE);
        }
    }
}

static void
eview_contacts_changed (EBookView *eview, GList *econtacts, SlBook *self)
{
  g_assert (econtacts != NULL);
  sl_debug_book ("examining %d econtact(s) for modifications", g_list_length (econtacts));
  
  for (; econtacts != NULL; econtacts = econtacts->next)
    {
      
      EContact *econtact = econtacts->data;
      SlPerson *person = sl_book_get_person (self, SL_ATTR_ID, e_contact_get (econtact, E_CONTACT_UID));
      g_assert (person != NULL);
      SlEntity *entity = SL_ENTITY (person);
      EContact *econtact_old = sl_entity_get_econtact (entity);

      sl_entity_set_econtact (entity, econtact);
      
      GList *eattributes = sl_priv_util_eattribute_flat_to_deep_list (e_vcard_get_attributes (E_VCARD (econtact)));
      GList *eattributes_old = sl_priv_util_eattribute_flat_to_deep_list (e_vcard_get_attributes (E_VCARD (econtact_old)));
      
      GList *added = NULL;
      GList *removed = NULL;
      GList *modified = NULL;
      GList *rest = NULL;
      sl_priv_util_list_set_diff_full (eattributes, eattributes_old,
                                      (GEqualFunc) sl_priv_util_eattrlist_name_equal,
                                      (GEqualFunc) sl_priv_util_eattrlist_modified,
                                      &added, &removed, &modified, &rest);
      
      sl_debug_book ("processing added eattributes%s", "");
      for (; added != NULL; added = added->next)
        {
          GList *eattrlist = added->data;
          EVCardAttribute *eattr = eattrlist->data;
          const gchar *eattrname = e_vcard_attribute_get_name (eattr);
          if (sl_is_ignored_eattr (eattrname))
            {
              sl_debug_book ("ignoring eattribute \"%s\"", eattrname);
              continue;
            }
          
          sl_debug_book ("found new eattribute \"%s\"", eattrname);
          SlAttribute *attr = sl_attribute_new_with_eattributes (eattrlist);
          sl_entity_add_attribute_shallow (entity, attr);
          sl_book_emit_person_attribute_added (self, person, attr, FALSE);
          g_object_unref (attr);
        }
      
      sl_debug_book ("processing removed eattributes%s", "");
      for (; removed != NULL; removed = removed->next)
        {
          GList *eattrlist = removed->data;
          EVCardAttribute *eattr = eattrlist->data;
          const gchar *eattrname = e_vcard_attribute_get_name (eattr);
          if (sl_is_ignored_eattr (eattrname))
            {
              sl_debug_book ("ignoring eattribute \"%s\"", eattrname);
              continue;
            }
          
          sl_debug_book ("eattribute \"%s\" doesn't exist anymore", eattrname);
          SlAttribute *attr = g_object_ref (sl_entity_get_attribute_from_eattr (entity, eattr));
          g_assert (attr != NULL);
          sl_entity_remove_attribute_shallow (entity, attr);
          sl_book_emit_person_attribute_removed (self, person, attr, FALSE);
          g_object_unref (attr);
        }
      
      sl_debug_book ("processing modified eattributes%s", "");
      for (; modified != NULL; modified = modified->next)
        {
          GList *eattrlist = modified->data;
          EVCardAttribute *eattr = eattrlist->data;
          const gchar *eattrname = e_vcard_attribute_get_name (eattr);
          if (sl_is_ignored_eattr (eattrname))
            {
              sl_debug_book ("ignoring eattribute \"%s\"", eattrname);
              continue;
            }

          sl_debug_book ("found modified eattribute \"%s\"", eattrname);
          SlAttribute *attr = sl_entity_get_attribute_from_eattr (entity, eattr);
          g_assert (attr != NULL);
          GList *old_values = sl_attribute_get_all (attr);
          sl_attribute_set_all_from_eattributes (attr, eattrlist);
          sl_book_emit_person_attribute_modified (self, person, attr, old_values, FALSE);
        }
      
      sl_debug_book ("processing rest of eattributes%s", "");
      for (; rest != NULL; rest = rest->next)
        {
          GList *eattrlist = rest->data;
          EVCardAttribute *eattr = eattrlist->data;
          const gchar *eattrname = e_vcard_attribute_get_name (eattr);
          if (sl_is_ignored_eattr (eattrname))
            {
              sl_debug_book ("ignoring eattribute \"%s\"", eattrname);
              continue;
            }
          
          SlAttribute *attr = sl_entity_get_attribute_from_eattr (entity, eattr);
          g_assert (attr != NULL);
          sl_attribute_update_eattributes (attr, eattrlist);
        }
      
      g_list_free (g_list_first (added));
      g_list_free (g_list_first (removed));
      g_list_free (g_list_first (modified));
      sl_priv_util_eattribute_deep_list_free (eattributes);
      sl_priv_util_eattribute_deep_list_free (eattributes_old);
     
      //g_object_unref (econtact_old);
    }
}

static void
eview_contacts_removed (EBookView *eview, GList *ids, SlBook *self)
{
  sl_debug_book ("%d econtacts removed", g_list_length (ids));
  
  for (; ids != NULL; ids = ids->next)
    {
      SlPerson *person = sl_book_get_person (self, SL_ATTR_ID, ids->data);
      g_assert (person != NULL);
      
      self->priv->people = g_list_remove (self->priv->people, person);
      
      sl_book_emit_person_removed (self, person, FALSE);
      
      g_object_unref (person);
    }
}

static void
eview_sequence_complete (EBookView *eview, EBookViewStatus status, SlBook *self) {
  if (status != E_BOOK_VIEW_STATUS_OK)
    {
      g_critical ("eview status is: %d", status);
    }
  
  if (self->priv->view_wait_loop != NULL)
    {
      sl_debug_book ("loading people from book done%s", "");

      g_main_loop_quit (self->priv->view_wait_loop);
      g_main_loop_unref (self->priv->view_wait_loop);
      self->priv->view_wait_loop = NULL;
    }
}

static void
empathy_contact_list_members_changed (EmpathyContactList *contactlist,
  EmpathyContact *imcontact, EmpathyContact *actor, guint reason, gchar *message,
  gboolean is_member, SlBook *self)
{
  /*g_debug (sl_priv_util_imcontact_to_string (imcontact));*/
  
  GList *people = self->priv->people;
  for (; people != NULL; people = people->next)
    {
      SlPerson *person = people->data;
      if (is_member)
        {
          sl_entity_associate_imcontact (SL_ENTITY (person), imcontact, TRUE);
        }
      else
        {
          sl_entity_deassociate_imcontact (SL_ENTITY (person), imcontact);
        }
    }
}
