/*
 * libsoylent - people management
 * 
 * Copyright (C) 2008 Sven Pfaller, Noya <kalterregen@gmx.net>
 * Copyright (C) 2008 Travis Reitter <treitter-dev@netdrain.com>
 * Copyright (C) 2008 Rob Taylor <rob.taylor@codethink.co.uk>
 * Copyright (C) 2008 Chris Lord <chris@o-hand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * general things that have to be changed / added
 * ---
 * TODO: add an SlBookManager (responsible for receiving signals when a book is
 * created / deleted)
 * TODO: add an sl_book_open method (one should be able to connect signals
 * before a book is used, so no events get lost)
 * TODO: SlBook should be derived from SlStorage
 * TODO: add on_purpose to signals (TRUE if the user made a change explicitly
 * (e.g. by calling add_attribute), FALSE if not (i.e. the backend reports a
 * a change))                                                  
 */

#ifndef SOYLENT_H
#define SOYLENT_H

#include "config.h"
#include "sl-marshal.h"
#include "sl-mutual-inclusion.h"
#include "sl-entity.h"
#include "sl-attribute.h"
#include "sl-attributes.h"
#include "sl-group.h"

#include <libempathy/empathy-contact-manager.h>

EmpathyContactManager *sl_im_get_contact_manager (void);
gboolean sl_im_is_im_attribute (const gchar *attrname);

gboolean sl_init (GError **error);
void sl_cleanup (void);

#endif
