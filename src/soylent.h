/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *  Author: Rob Taylor <rob.taylor@codethink.co.uk>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_H_
#define _SOYLENT_H_

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>

#endif /* _SOYLENT_H_ */
