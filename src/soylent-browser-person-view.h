/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Rob Taylor <rob.taylor@codethink.co.uk>
 *
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2007 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_BROWSER_PERSON_VIEW_H_
#define _SOYLENT_BROWSER_PERSON_VIEW_H_

#include "soylent-defs.h"
#include "soylent-browser.h"
#include "soylent-person.h"

typedef struct cb_data_pre_person_tag cb_data_pre_person;
struct cb_data_pre_person_tag
{
  SoylentBrowser *browser;
  EContact *e_contact;
};

/* Updating the widgets in the person editor */
gboolean soylent_browser_person_view_update (SoylentBrowser *browser,
                                             SoylentPerson *person);

/* Signal handlers */
gboolean soylent_browser_person_view_save_finalize_cb (GtkWidget *widget,
                                                       GdkEvent *event,
                                                       gpointer user_data);
gboolean soylent_browser_person_action_new_person_cb (GtkButton *btn,
                                                      gpointer user_data);
gboolean soylent_browser_person_action_delete_selected_cb (GtkButton *btn,
                                                           gpointer user_data);
gboolean soylent_browser_person_action_delete_selected_finalize_cb
                                                          (GtkButton *btn,
                                                           gpointer user_data);
gboolean soylent_browser_person_action_delete_selected_hide_dialog_cb
                                                          (GtkButton *btn,
                                                           gpointer user_data);
gboolean soylent_browser_person_action_edit_selected_cb (GtkButton *btn,
                                                         gpointer user_data);
gboolean soylent_browser_person_action_chat_menu_tool_button_cb
                                                      (GtkMenuToolButton *btntb,
                                                       gpointer user_data);
gboolean soylent_browser_person_action_email_menu_tool_button_cb
                                                      (GtkMenuToolButton *btntb,
                                                       gpointer user_data);
gboolean soylent_browser_person_selection_changed_cb (GtkIconView *iv,
                                                      gpointer user_data);
gboolean soylent_browser_person_action_communicate_email
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
gboolean soylent_browser_person_action_communicate_chat
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);

void soylent_browser_person_error_chat_new_cb (MissionControl *mc,
                                               GError *error,
                                               gpointer user_data);

/* Committing changes from the above functions */
gboolean soylent_browser_person_detail_add_commit (GtkWidget *btn_detail_add,
                                                   gpointer user_data);

/* Setup/update sections of the person view UI with latest data from e-d-s */
gboolean soylent_browser_person_btn_detail_add_update_simple
                                                        (GtkEntry *entry_simple,
                                                         gpointer user_data);
gboolean soylent_browser_person_btn_detail_add_update_mail  
                                                        (GtkWidget *widget_mail,
                                                         gpointer user_data);
gboolean soylent_browser_person_detail_add_cbox_setup
                                                    (SoylentBrowser *browser,
                                                     gboolean skip_domain_cbox);
gboolean soylent_browser_person_detail_add_cbox_update
                                                (GtkComboBox *cbox_domain, 
                                                        gpointer user_data);

#endif /* _SOYLENT_BROWSER_PERSON_VIEW_H_ */
