/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_BROWSER_MENU_BAR_H_
#define _SOYLENT_BROWSER_MENU_BAR_H_

#include "soylent-browser.h"

gboolean soylent_browser_menu_bar_setup (SoylentBrowser *browser);
gboolean soylent_browser_menu_bar_update (SoylentBrowser *browser);

#endif /* _SOYLENT_BROWSER_MENU_BAR_H_ */
