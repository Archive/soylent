/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <string.h>
#include "eds-utils.h"
#include "soylent.h"
#include "soylent-browser.h"
#include "soylent-browser-menu-bar.h"
#include "soylent-browser-person-view.h"
#include "soylent-utils.h"

const EContactField E_CONTACT_FIELDS_PHONE[] = {E_CONTACT_PHONE_BUSINESS,
                                                E_CONTACT_PHONE_HOME,
                                                E_CONTACT_PHONE_MOBILE,};
const gchar *CONTEXT_LIST_PHONE[] = {"work", "home", "cell",};

const EContactField E_CONTACT_FIELDS_WEB[] = {E_CONTACT_HOMEPAGE_URL,
                                              E_CONTACT_BLOG_URL,
                                              E_CONTACT_CALENDAR_URI,
                                              E_CONTACT_VIDEO_URL,};
const gchar *CONTEXT_LIST_WEB[] = {"homepage", "blog", "calendar", "video",};

typedef struct pre_save_im_tag pre_save_im_t;
struct pre_save_im_tag
{
  /* one of the CONTEXT_* values */
  guint context;
  /* general field (eg, E_CONTACT_IM_JABBER) */
  EContactField e_contact_field;
  const gchar *value;
};

const gchar *CONTEXT_STRS[] = {"home", "work", "other",};

static gboolean soylent_browser_person_view_editor_hide
                                                      (SoylentBrowser *browser);
static gboolean soylent_browser_person_view_save_changes (GtkWidget *widget,
                                                          gpointer user_data);
static gboolean soylent_browser_person_edit_save_scroll_cb
                                                      (EBookView *book_view,
                                                       const GList *e_contacts,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_new_edit_cb
                                      (EBookView *book_view,
                                       const GList *e_contacts,
                                       cb_data_pre_person *pre_person_to_match);
static gboolean soylent_browser_person_action_chat_menu_shell_cb
                                                      (GtkMenuShell *menu_email,
                                                       gpointer user_data);
static gboolean soylent_browser_person_action_chat_menu_item_cb
                                                          (GtkMenuItem *item,
                                                           gpointer user_data);
static gboolean soylent_browser_person_selected_chat_menu_update_cb
                                                      (GtkMenuToolButton *btntb,
                                                       gpointer user_data);
static gboolean soylent_browser_person_action_email_menu_shell_cb
                                                      (GtkMenuShell *menu_email,
                                                       gpointer user_data);
static gboolean soylent_browser_person_action_email_menu_item_cb
                                                          (GtkMenuItem *item,
                                                           gpointer user_data);
static gboolean soylent_browser_person_selected_email_menu_update_cb
                                                (GtkMenuToolButton *btntb_email,
                                                 gpointer user_data);
static gboolean soylent_browser_person_set_mail_fields_from_widgets 
                                                      (gpointer e_contact_field,
                                                       gpointer widget_name,
                                                       gpointer user_data);
static gboolean soylent_browser_person_set_field_simple
                                                (EContact *e_contact,
                                                 EContactField e_contact_field,
                                                 const gchar *contents_new);
static gchar* soylent_browser_person_detail_add_get_cbox_val
                                                      (SoylentBrowser *browser,
                                                       const gchar *cbox_name);
static gchar* soylent_browser_person_detail_add_get_web_attr_name
                                                      (SoylentBrowser *browser);


/* Adding individual details to the VCard/EContact */
static gboolean soylent_browser_person_detail_add_person_add_attr
                                                      (SoylentBrowser *browser,
                                                       const gchar *attr_name,
                                                       const gchar *param_val);
static gboolean soylent_browser_person_detail_add_person_add_mail_attrs
                                                      (SoylentBrowser *browser,
                                                       const gchar *param_val);

/* Setup/update sections of the person view UI with latest data from e-d-s */
gboolean soylent_browser_person_view_update (SoylentBrowser *browser,
                                             SoylentPerson *person);
static gboolean soylent_browser_person_view_email_update
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_im_update (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_avatar_update
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_mail_update
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_name_update
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_detail_add_update
                                                      (SoylentBrowser *browser);
static gboolean soylent_browser_person_view_phone_update
                                                      (SoylentBrowser *browser,
                                                       SoylentPerson *person);
static gboolean soylent_browser_person_view_web_update (SoylentBrowser *browser,
                                                        SoylentPerson *person);
static gboolean soylent_browser_person_view_update_widgets_actions
                                                      (SoylentBrowser *browser);

/* Save unsaved changes to the person's details and return to Browse Mode
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_view_save_finalize_cb (GtkWidget *widget,
                                              GdkEvent *event,
                                              gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  EBookView *e_book_view = NULL;

  g_return_val_if_fail (user_data, retval);

  /* FIXME: when SoylentBrowser is a GObject, check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;

  e_book_view = soylent_browser_get_e_book_view (browser);
  if (e_book_view)
    {
      SoylentPerson *person = NULL;
      
      /* FIXME: when SoylentPerson is a GObject, check SOYLENT_IS_PERSON */
      person = soylent_browser_get_selected_person (browser);
      if (person)
        {
          gint handler_id = -1;

          /* Disconnected in callback */
          handler_id = g_signal_connect_after
                      (e_book_view, "contacts_changed",
                       G_CALLBACK (soylent_browser_person_edit_save_scroll_cb),
                       person);
          if (handler_id >= G_SIGNAL_HANDLER_MIN_VALID)
            {
              retval = soylent_browser_person_view_save_changes (NULL, browser);
            }
        }
      else
        {
          g_warning ("failed to get the selected person");
        }
    }
  else
    {
      g_critical ("failed to get the EBookView for the SoylentBrowser");
    }

  soylent_browser_person_view_editor_hide (browser);

  return retval;
}

/* Handle clicks on the Delete button in the Browse view
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_action_delete_selected_finalize_cb (GtkButton *btn,
                                                           gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  const GList *i = NULL;
  GList *e_uids = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: when SoylentBrowser is a GObject, check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;

  /* invert usual logic to simplify the loop below */
  retval = TRUE;

  e_uids = soylent_browser_get_selected_people_e_uid (browser);
  for (i = (GList*) e_uids; i; i = g_list_next (i))
    {
      EBook *e_book = NULL;
      gchar *e_uid = NULL;
      GError *error = NULL;

      e_book = soylent_browser_get_e_book (browser);
      e_uid = (gchar*) i->data;

      if (e_uid)
        {
          if (!e_book_remove_contact (e_book, (const gchar*) e_uid, &error))
            {
              g_warning ("Failed to remove contact: %s", error->message);
              retval = FALSE;
            } 

          g_clear_error (&error);
          g_free (e_uid);
        }
      else
        {
          g_warning ("list of selected people contains a NULL UID");
        }
    }

      /* Hide the "Delete person?" dialog whether or not we actually deleted them;
      * it shouldn't stick around (especially since it's modal) */
  {
    GladeXML *wtree = NULL;
    GtkWidget *dia_delete_person_confirm = NULL;

    wtree = soylent_browser_get_widget_tree (browser);
    dia_delete_person_confirm = glade_xml_get_widget
                                                  (wtree,
                                                   "dia_delete_person_confirm");
    gtk_widget_hide (dia_delete_person_confirm);
  }

  g_list_free (e_uids);

  return retval;
}

/* Prompt "Delete person?" when we select one or more people and hit Delete
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_action_delete_selected_cb (GtkButton *btn,
                                                  gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkWidget *dia_delete_person_confirm = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: when SoylentBrowser is a GObject, check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;

  wtree = soylent_browser_get_widget_tree (browser);
  dia_delete_person_confirm = glade_xml_get_widget
                                                  (wtree,
                                                   "dia_delete_person_confirm");
  gtk_widget_show (dia_delete_person_confirm);

  return retval;
}

/* Hide the "Confirm delete?" window when we click the "Cancel" button
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_action_delete_selected_hide_dialog_cb
                                                          (GtkButton *btn,
                                                           gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkWidget *dia_delete_person = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: when SoylentBrowser is a GObject, check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;

  wtree = soylent_browser_get_widget_tree (browser);

  dia_delete_person = glade_xml_get_widget (wtree, "dia_delete_person_confirm");
  gtk_widget_hide (dia_delete_person);

  retval = TRUE;

  return retval;
}

/* Handle clicks on the Edit button in the Browse view
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_action_edit_selected_cb (GtkButton *btn,
                                                gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  SoylentPerson *person = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: when SoylentBrowser is a GObject, check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;

  /* FIXME: when SoylentPerson is a GObject, check SOYLENT_IS_PERSON */
  person = soylent_browser_get_selected_person (browser);
  if (person)
    {
#ifdef HAVE_CONTACTS_APP
      {
        const gchar *e_uid = NULL;
        EContact *e_contact = NULL;
        
        e_contact = soylent_person_get_e_contact (person);
        if (e_contact && E_IS_CONTACT (e_contact))
          {
            e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
          }
        else
          {
            g_warning ("attempted to edit a Person with a NULL e_contact");
          }

        if (e_uid)
          {
            gchar *command_line = NULL;
            GError *error = NULL;
            
            command_line = g_strdup_printf ("contacts --edit-uid=%s", e_uid);
            g_spawn_command_line_async (command_line, &error);

            if (error)
              {
                g_warning ("Failed to launch Contacts to edit this person: %s",
                           error->message);
                g_clear_error (&error);
              }
            else
              {
                retval = TRUE;
              }

            g_free (command_line);
          }
        else
          {
            g_warning ("e-d-s doesn't know about this person");
          }
      }
#else
      {
        retval = soylent_browser_view_set_mode (browser,
                                                SB_PEOPLE_VIEW_MODE_EDIT);
      }
#endif /* HAVE_CONTACTS_APP */
    }
  else
    {
      g_warning ("Couldn't find the selected person(s) (!)");
    }

  return retval;
}

/* Handle clicks on the button part of the Email action MenuToolButton
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_action_email_menu_tool_button_cb
                                                      (GtkMenuToolButton *btntb,
                                                       gpointer user_data)
{
  gboolean retval = FALSE;
  GtkMenuShell *menu_shell = NULL;

  menu_shell = GTK_MENU_SHELL (gtk_menu_tool_button_get_menu (btntb));
  if (menu_shell)
    {
      retval = soylent_browser_person_action_email_menu_shell_cb (menu_shell,
                                                                  user_data);
    }

  return retval;
}

/* Open a chat session with the given person
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_action_communicate_email (SoylentBrowser *browser,
                                                 SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkMenuToolButton *btntb = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  btntb = GTK_MENU_TOOL_BUTTON (glade_xml_get_widget (wtree, "btntb_email")); 
  retval = soylent_browser_person_action_email_menu_tool_button_cb (btntb,
                                                                    browser);

  return retval;
}

/* Handle clicks on the button part of the Chat action MenuToolButton
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_action_chat_menu_tool_button_cb
                                                      (GtkMenuToolButton *btntb,
                                                       gpointer user_data)
{
  gboolean retval = FALSE;
  GtkMenuShell *menu_shell = NULL;

  menu_shell = GTK_MENU_SHELL (gtk_menu_tool_button_get_menu (btntb));
  if (menu_shell)
    {
      retval = soylent_browser_person_action_chat_menu_shell_cb (menu_shell,
                                                                 user_data);
    }

  return retval;
}

/* Open a chat session with the given person
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_action_communicate_chat (SoylentBrowser *browser,
                                                SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkMenuToolButton *btntb = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  btntb = GTK_MENU_TOOL_BUTTON (glade_xml_get_widget (wtree, "btntb_chat"));
  retval = soylent_browser_person_action_chat_menu_tool_button_cb (btntb,
                                                                   browser);

  return retval;
}

/* Handle a change of who is selected
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_selection_changed_cb (GtkIconView *iv,
                                             gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  browser = (SoylentBrowser*) user_data;

  retval = TRUE;
  retval &= soylent_browser_person_view_update_widgets_actions (browser);
  retval &= soylent_browser_menu_bar_update (browser);

  return retval;
}

/* Create a new SoylentPerson and matching EContact in e-d-s
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_action_new_person_cb (GtkButton *btn, gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkIconView *iv_people = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);

  iv_people = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  gtk_icon_view_unselect_all (iv_people);

#ifdef HAVE_CONTACTS_APP
  {
    gchar *command_line = NULL;
    GError *error_add = NULL;
    
    command_line = g_strdup_printf ("contacts --new-contact");
    g_spawn_command_line_async (command_line, &error_add);

    if (!error_add)
      {
        retval = TRUE;
      }
    else
      {
        g_warning ("Failed to launch Contacts to add a new Person: %s",
                   error_add->message);
        g_clear_error (&error_add);
      }

    g_free (command_line);
  }
#else
  {
    EBook *e_book = NULL;
    EBookView *e_book_view = NULL;
    EContact *e_contact = NULL;
    cb_data_pre_person *pre_person_to_match = NULL;
    GError *error_add = NULL;

    e_book = soylent_browser_get_e_book (browser);
    e_book_view = soylent_browser_get_e_book_view (browser);

    e_contact = e_contact_new ();

    e_contact_set (e_contact, E_CONTACT_FULL_NAME, SB_DEFAULT_NAME);
    e_contact_set (e_contact, E_CONTACT_NAME,
                   e_contact_name_from_string (SB_DEFAULT_NAME));

    /* Freed in the callback */
    pre_person_to_match = g_new (cb_data_pre_person, 1);
    pre_person_to_match->browser = browser;
    pre_person_to_match->e_contact = e_contact;
    g_signal_connect_after (e_book_view, "contacts_added",
                            G_CALLBACK (soylent_browser_person_new_edit_cb),
                            pre_person_to_match);

    if (e_book_add_contact (e_book, e_contact, &error_add))
      {
        retval = TRUE;
      }
    else
      {
        g_warning ("Failed to create a new contact: %s", error_add->message);
        g_clear_error (&error_add);
      }
  }
#endif /* HAVE_CONTACTS_APP */

  return retval;
}

/* Display a MissionControl chat error dialog */
void
soylent_browser_person_error_chat_new_cb (MissionControl *mc, GError *error,
                                          gpointer user_data)
{
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkMessageDialog *error_chat_new = NULL;

  g_return_if_fail (user_data);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_if_fail (SOYLENT_IS_BROWSER (browser));
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);
  error_chat_new = GTK_MESSAGE_DIALOG (glade_xml_get_widget (wtree,
                                                             "error_chat_new"));
  if (error)
    {
      gtk_message_dialog_format_secondary_text (error_chat_new,
                                                "Failed to open a chat: %s",
                                                error->message);
      g_clear_error (&error);

      gtk_widget_show (GTK_WIDGET (error_chat_new));
    }
}

/* Commit any new details to the person being edited
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_detail_add_commit (GtkWidget *btn_detail_add,
                                          gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkComboBox *cbox_domain = NULL; 
  GtkEntry *entry_add = NULL;
  gchar *domain = NULL;
  gchar *attr_name = NULL;
  gchar *param_val = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);

  cbox_domain = GTK_COMBO_BOX (glade_xml_get_widget (wtree,
                                                     "cbox_detail_domain")); 
  domain = gtk_combo_box_get_active_text (cbox_domain);

  /* Make sure any changes to existing details between editing the new detail
   * and hitting Add don't get lost */
  /* FIXME: cut: with the new "save everything" model, this is unnecessary */
  soylent_browser_person_view_save_changes (NULL, browser);

  if (g_str_equal (domain, "Email"))
    {
      attr_name = g_strdup (EVC_EMAIL);
      param_val = soylent_browser_person_detail_add_get_cbox_val
                                                          (browser,
                                                           "cbox_detail_type");
    }
  else if (g_str_equal (domain, "Instant Messaging"))
    {
      gchar *proto = NULL;

      proto = soylent_browser_person_detail_add_get_cbox_val
                                                          (browser,
                                                           "cbox_detail_proto");
      attr_name = g_strdup_printf ("X-%s", proto);
      param_val = soylent_browser_person_detail_add_get_cbox_val
                                                          (browser,
                                                           "cbox_detail_type");

      g_free (proto);
    }
  else if (g_str_equal (domain, "Phone"))
    {
      attr_name = g_strdup (EVC_TEL);
      param_val = soylent_browser_person_detail_add_get_cbox_val
                                                    (browser,
                                                     "cbox_detail_type_phone");
    }
  else if (g_str_equal (domain, "Web Address"))
    {
      attr_name = soylent_browser_person_detail_add_get_web_attr_name (browser);
    }
  else if (g_str_equal (domain, "Mailing Address"))
    {
      /* TODO: set the corresponding LABEL attribute that Evolution sets */
      param_val = soylent_browser_person_detail_add_get_cbox_val
                                                          (browser,
                                                           "cbox_detail_type");
      attr_name = g_strdup (EVC_ADR);
    }
  else
    {
      g_warning ("Unknown detail domain; ignoring addition");
    }

  if (attr_name)
    {
      if (g_str_equal (attr_name, EVC_ADR))
        {
          retval = soylent_browser_person_detail_add_person_add_mail_attrs
                                                                    (browser,
                                                                     param_val);
        }
      else
        {
          retval = soylent_browser_person_detail_add_person_add_attr
                                                                    (browser,
                                                                     attr_name,
                                                                     param_val);
        }
    }

  /* Make sure we actually commit these changes back */
  /* TODO: it would be nice if we could just add these changes to the UI and
   * rely on the window close to save the changes; it would cut out a call to
   * eds and avoid the side effect of adding a new detail saving any pending
   * changes to the other fields */
  if (retval)
    {
      SoylentPerson *person = NULL;

      /*
      soylent_browser_person_view_save_changes (NULL, browser);
      */

      person = soylent_browser_get_selected_person (browser);
      soylent_browser_person_view_update (browser, person);
    }

  entry_add = GTK_ENTRY (glade_xml_get_widget (wtree, "entry_detail_add")); 
  gtk_entry_set_text (entry_add, "");
  gtk_widget_set_sensitive (btn_detail_add, FALSE);

  g_free (domain);
  g_free (attr_name);
  g_free (param_val);

  return retval;
}

/* Hook up signal handlers for the Detail Add UI section widgets
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_detail_add_cbox_setup (SoylentBrowser *browser,
                                              gboolean skip_domain_cbox)
{
  const gchar *cbox_names[] = {"cbox_detail_domain", "cbox_detail_proto",
                               "cbox_detail_type", "cbox_detail_type_phone",
                               "cbox_detail_type_web",};
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  guint num_cbox = ARRAY_LEN (cbox_names);
  guint i = 0;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  /* invert usual logic to simplify the code below */
  retval = TRUE;

  wtree = soylent_browser_get_widget_tree (browser);

  /*  Glade lets us set these values, but they aren't honored */
  for (i = (skip_domain_cbox ? 1 : 0); i < num_cbox; i++)
    {
      GtkComboBox *cbox_cur = NULL;

      cbox_cur = GTK_COMBO_BOX (glade_xml_get_widget (wtree, cbox_names[i]));
      gtk_combo_box_set_active (cbox_cur, 0);
    }

  return retval;
}

/* Update the visibility of the Detail Add UI section "Type" combo boxes, based
 * on the current "Domain" combo box selection
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_detail_add_cbox_update (GtkComboBox *cbox_domain,
                                               gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkWidget *cbox_proto = NULL;
  GtkWidget *cbox_type = NULL;
  GtkWidget *cbox_type_phone = NULL;
  GtkWidget *cbox_type_web = NULL;
  GtkWidget *vbox_entry_detail_add = NULL;
  GtkWidget *align_detail_add = NULL;
  gchar *domain = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);

  cbox_proto = glade_xml_get_widget (wtree, "cbox_detail_proto"); 
  cbox_type = glade_xml_get_widget (wtree, "cbox_detail_type"); 
  cbox_type_phone = glade_xml_get_widget (wtree, "cbox_detail_type_phone");
  cbox_type_web = glade_xml_get_widget (wtree, "cbox_detail_type_web");
  vbox_entry_detail_add = glade_xml_get_widget (wtree, "vbox_entry_detail_add");
  align_detail_add = glade_xml_get_widget (wtree, "align_detail_mail");

  soylent_browser_person_detail_add_cbox_setup (browser, TRUE);

  /* Hide all of the widgets up-front to simplify the logic below */
  gtk_widget_hide (cbox_proto);
  gtk_widget_hide (cbox_type);
  gtk_widget_hide (cbox_type_phone);
  gtk_widget_hide (cbox_type_web);
  gtk_widget_hide (vbox_entry_detail_add);
  gtk_widget_hide (align_detail_add);

  domain = gtk_combo_box_get_active_text (cbox_domain);
  if (g_str_equal (domain, "Instant Messaging"))
    {
      gtk_widget_show (cbox_proto);
      gtk_widget_show (cbox_type);
    }
  else if (g_str_equal (domain, "Phone"))
    {
      gtk_widget_show (cbox_type_phone);
    }
  else if (g_str_equal (domain, "Web Address"))
    {
      gtk_widget_show (cbox_type_web);
    }
  else if (g_str_equal (domain, "Mailing Address"))
    {
      gtk_widget_show (cbox_type);
    }
  else /* Email */
    {
      gtk_widget_show (cbox_type);
    }

  /* Display either the mailing Address fields or the generic entry box,
   * depending on whether we're entering a Mailing Address or not */
  if (g_str_equal (domain, "Mailing Address"))
    {
      gtk_widget_show (align_detail_add);
    }
  else
    {
      gtk_widget_show (vbox_entry_detail_add);
    }

  /* There isn't anything to check between the validation block and here */
  retval = TRUE;

  return retval;
}

/* Set the sensitivity of the Detail Add button based on whether there are
 * unsaved changes in the simple entry field (non-Mailing Address).
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_btn_detail_add_update_simple (GtkEntry *entry_simple,
                                                     gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkWidget *btn_detail_add = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);
  btn_detail_add = (glade_xml_get_widget (wtree, "btn_detail_add")); 

  if (strlen (gtk_entry_get_text (entry_simple)) > 0)
    {
      gtk_widget_set_sensitive (btn_detail_add, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive (btn_detail_add, FALSE);
    }

  retval = TRUE;

  return retval;
}

/* Set the sensitivity of the Detail Add button based on whether there are
 * unsaved changes in the Mailing Address fields.
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_btn_detail_add_update_mail (GtkWidget *widget_mail,
                                                   gpointer user_data)
{
  const gchar *entry_names[] = {"entry_person_mail_add_po",
                                "entry_person_mail_add_locality",
                                "entry_person_mail_add_region",
                                "entry_person_mail_add_code",
                                "entry_person_mail_add_country",};
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  GtkWidget *btn_detail_add = NULL;
  gboolean new_details_non_empty = FALSE;
  guint num_entries = ARRAY_LEN (entry_names);
  guint i = 0;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);
  btn_detail_add = (glade_xml_get_widget (wtree, "btn_detail_add")); 

  for (i = 0; !new_details_non_empty && (i < num_entries); i++)
    {
      GtkEntry *entry_cur = NULL;

      entry_cur = GTK_ENTRY (glade_xml_get_widget (wtree, entry_names[i]));
      new_details_non_empty = (strlen (gtk_entry_get_text (entry_cur)) > 0);
    }

  if (!new_details_non_empty)
    {
      GtkTextView *tv_add_street = NULL;
      GtkTextBuffer *tb_add_street = NULL;
      GtkTextIter start;
      GtkTextIter end;

      tv_add_street = GTK_TEXT_VIEW (glade_xml_get_widget
                                                (wtree,
                                                 "tv_person_mail_add_street"));
      tb_add_street = gtk_text_view_get_buffer (tv_add_street);
      gtk_text_buffer_get_bounds (tb_add_street, &start, &end);
      new_details_non_empty = (strlen (gtk_text_buffer_get_text (tb_add_street,
                                                                 &start, &end,
                                                                 FALSE))
                               > 0);
    }

  if (new_details_non_empty)
    {
      gtk_widget_set_sensitive (btn_detail_add, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive (btn_detail_add, FALSE);
    }

  retval = TRUE;

  return retval;
}



/* Hide the Person editor window
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_view_editor_hide (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkWidget *window_person_view = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  window_person_view = (glade_xml_get_widget (wtree, "window_person_view"));
  gtk_widget_hide (window_person_view);
  retval = TRUE;

  return retval;
}

/* Add name field changes to selected EContact (but do not commit) */
static gboolean
soylent_browser_person_view_save_changes_prep_name (SoylentBrowser *browser,
                                                    EContact *e_contact)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  gchar *contents_new = NULL;
  GtkEntry *entry_name = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  entry_name = GTK_ENTRY (glade_xml_get_widget (wtree, "entry_person_name")); 

  contents_new = g_strdup (gtk_entry_get_text (entry_name));
  retval = soylent_browser_person_set_field_simple (e_contact,
                                                    E_CONTACT_FULL_NAME,
                                                    contents_new);

  g_free (contents_new);

  return retval;
}

/* Add email field changes to selected EContact (but do not commit) */
static gboolean
soylent_browser_person_view_save_changes_prep_email (SoylentBrowser *browser,
                                                     EContact *e_contact)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContactField e_contact_field = E_CONTACT_FIELD_FIRST - 1;
  gchar *contents_new = NULL;
  guint email_num = 1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  /* invert usual logic to simplify code below */
  retval = TRUE;

  /* XXX: potentially lossy; works around a bug where
   * e_contact_set(contact, {NULL,""}) doesn't always stick */
  e_vcard_remove_attributes (E_VCARD (e_contact), NULL, EVC_EMAIL);

  for (email_num = 1, e_contact_field = E_CONTACT_EMAIL_1;
       (email_num <= MAX_GUI_EMAIL)
       && (e_contact_field < (E_CONTACT_EMAIL_1 + MAX_GUI_EMAIL));
       email_num++)
    {
      gchar *widget_name = NULL;
      GtkHBox *hbox_email = NULL;

      widget_name = g_strdup_printf ("hbox_person_email_%d", email_num); 
      hbox_email = GTK_HBOX (glade_xml_get_widget (wtree, widget_name)); 
      g_free (widget_name);

      /* By convention, the hbox is only visible if there's something useful
        * in it */
      if (GTK_WIDGET_VISIBLE (hbox_email))
        {
          GtkEntry *entry_email = NULL;

          widget_name = g_strdup_printf ("entry_person_email_%d", email_num); 
          entry_email = GTK_ENTRY (glade_xml_get_widget (wtree, widget_name));
          g_free (widget_name);

          contents_new = g_strdup (gtk_entry_get_text (entry_email));

          /* Ensure name is set to something useful, to prevent blanking the
            * name the first time it's edited */

          if (contents_new && !g_str_equal (contents_new, ""))
            {
              retval &= soylent_browser_person_set_field_simple
                                                              (e_contact,
                                                               e_contact_field,
                                                               contents_new);
              /* only move on to the next field if the current one isn't
                * blank; this avoids a data aliasing problem */
              e_contact_field++;
            }

          g_free (contents_new);
        }
    }

    /* if we run through any iterations, we skipped over empty fields above;
     * clear out the remaining fields to overwrite any dangling values */
    for (;
         e_contact_field < (E_CONTACT_EMAIL_1 + MAX_GUI_EMAIL);
         e_contact_field++)
      {
        soylent_browser_person_set_field_simple (e_contact, e_contact_field,
                                                 NULL);
      }

  return retval;
}

/* Add field changes to selected EContact (but do not commit)
 *
 * Arguments:
 * browser -- SoylentBrowser
 * e_contact -- EContact to modify
 * widget_context -- widget name context (eg, 'phone', 'web')
 * e_contact_fields -- list of EContactFields corresponding to context_list
 * context_list -- widget name subcontexts (eg, 'home', 'work') corresponding to
 *                 e_contact_fields
 */
static gboolean
soylent_browser_person_view_save_changes_prep_list
                                         (SoylentBrowser *browser,
                                          EContact *e_contact,
                                          const gchar *widget_context,
                                          guint num_fields,
                                          const EContactField *e_contact_fields,
                                          const gchar **context_list)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  guint context = 0;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  /* invert usual logic to simplify code below */
  retval = TRUE;

  for (context = 0; context < num_fields; context++)
    {
      gchar *contents_new = NULL;
      gchar *widget_name = NULL;
      GtkHBox *hbox = NULL;

      widget_name = g_strdup_printf ("hbox_person_%s_%s", widget_context,
                                     context_list[context]); 
      hbox = GTK_HBOX (glade_xml_get_widget (wtree, widget_name)); 
      g_free (widget_name);

      /* By convention, the hbox is only visible if there's something
       * useful in it */
      if (GTK_WIDGET_VISIBLE (hbox))
        {
          GtkEntry *entry = NULL;

          widget_name = g_strdup_printf ("entry_person_%s_%s", widget_context,
                                         context_list[context]); 
          entry = GTK_ENTRY (glade_xml_get_widget (wtree, widget_name));
          g_free (widget_name);

          contents_new = g_strdup (gtk_entry_get_text (entry));
          retval &= soylent_browser_person_set_field_simple
                                                    (e_contact,
                                                     e_contact_fields[context],
                                                     contents_new);
          g_free (contents_new);
        }
    }

  return retval;
}

/* Add modified and new IM fields to the EContact (but do not commit) */
static gboolean
soylent_browser_person_view_save_changes_prep_im (SoylentBrowser *browser,
                                                  EContact *e_contact)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  guint context = CONTEXT_FIRST;
  GList *pre_save_list_im = NULL;
  GList *l = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  retval = TRUE;

  for (context = CONTEXT_IM_FIRST; context <= CONTEXT_IM_LAST; context++)
    {
      guint im_field_pos = 0;

      for (im_field_pos = 0;
           im_field_pos < IM_FIELDS_PER_CONTEXT;
           im_field_pos++)
        {
          GtkHBox *hbox_im = NULL;
          GtkEntry *entry_im = NULL;
          gchar *hbox_name = NULL;
          gchar *label_name = NULL;
          gchar *entry_name = NULL;

          hbox_name = g_strdup_printf ("hbox_person_im_%s_%d",
                                       CONTEXT_STRS[context], im_field_pos + 1);
          label_name = g_strdup_printf ("label_person_im_%s_%d",
                                        CONTEXT_STRS[context],
                                        im_field_pos + 1);
          entry_name = g_strdup_printf ("entry_person_im_%s_%d",
                                        CONTEXT_STRS[context],
                                        im_field_pos + 1);
          entry_im = GTK_ENTRY (glade_xml_get_widget (wtree, entry_name));
          hbox_im = GTK_HBOX (glade_xml_get_widget (wtree, hbox_name));
          if (GTK_WIDGET_VISIBLE(hbox_im))
            {
              GtkLabel *label = NULL;
              const gchar *label_text = NULL;
              gchar *proto_name_lc = NULL;
              gchar *field_name = NULL;
              pre_save_im_t *pre_save_im = NULL;
              gchar *value = NULL;

              label = GTK_LABEL (glade_xml_get_widget (wtree, label_name)); 
              label_text = gtk_label_get_text (label);
              proto_name_lc = g_ascii_strdown (label_text, strlen (label_text));
              field_name = g_strdup_printf ("im_%s", proto_name_lc);

              value = g_strdup (gtk_entry_get_text (entry_im));

              pre_save_im = g_new (pre_save_im_t, 1);
              pre_save_im->context = context;
              /* FIXME: actually use the correct IM field, based on proto */
              pre_save_im->e_contact_field = e_contact_field_id (field_name);
              pre_save_im->value = value;
              pre_save_list_im = g_list_prepend (pre_save_list_im, pre_save_im);

              e_contact_set_attributes (e_contact, pre_save_im->e_contact_field,
                                        NULL);

              g_free (proto_name_lc);
              g_free (field_name);
            }

          g_free (hbox_name);
          g_free (entry_name);
          g_free (label_name);
        }
    }

  /* Traverse in reverse order to preserve order from the editor */
  for (l = g_list_last (pre_save_list_im); l; l = g_list_previous (l))
    {
      pre_save_im_t *fields_im = NULL;

      fields_im = l->data;
      if (fields_im->value && !g_str_equal (fields_im->value, ""))
        {
          e_vcard_attr_list_prepend_value (e_contact,
                                           fields_im->e_contact_field,
                                           CONTEXT_STRS[fields_im->context],
                                           fields_im->value);
        }
    }

  /* FIXME: go through pre_save_list_im and g_free all datas' value field, each
   * struct */

  g_list_free (pre_save_list_im);

  return retval;
}

/* Add postal mail changes to selected EContact (but do not commit) */
static gboolean
soylent_browser_person_view_save_changes_prep_mail (SoylentBrowser *browser,
                                                    EContact *e_contact)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  guint context = 0;
  EContactField e_contact_field = E_CONTACT_FIELD_FIRST - 1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  /* invert usual logic to simplify code below */
  retval = TRUE;

  for (context = CONTEXT_FIRST, e_contact_field = E_CONTACT_FIRST_ADDRESS_ID;
       (context <= CONTEXT_LAST)
       && (e_contact_field <= E_CONTACT_LAST_ADDRESS_ID);
       context++, e_contact_field++)
    {
      GtkFrame *frame_mail = NULL;

      frame_mail = GTK_FRAME (soylent_browser_get_widget (browser, "frame",
                                                          "mail",
                                                          CONTEXT_STRS[context],
                                                          NULL));
      if (GTK_WIDGET_VISIBLE(frame_mail))
        {
          gchar *widget_name = NULL;

          widget_name = g_strdup_printf ("mail_%s", CONTEXT_STRS[context]);
          retval &= soylent_browser_person_set_mail_fields_from_widgets
                                                            (widget_name,
                                                             &e_contact_field,
                                                             browser);

          g_free (widget_name);
        }
    }
  
  return retval;
}

/* Save unsaved changes to the person's details
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_save_changes (GtkWidget *widget, gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);

  /* TODO: make this one of the parameters instead */
  e_contact = soylent_browser_get_selected_person_e_contact (browser);

  soylent_browser_person_view_save_changes_prep_name (browser, e_contact);
  soylent_browser_person_view_save_changes_prep_email (browser, e_contact);

  /* Prep the phone numbers for saving */
  soylent_browser_person_view_save_changes_prep_list
                                          (browser, e_contact, "phone",
                                           ARRAY_LEN (E_CONTACT_FIELDS_PHONE),
                                           E_CONTACT_FIELDS_PHONE,
                                           CONTEXT_LIST_PHONE);

  /* Prep the web URLs for saving */
  soylent_browser_person_view_save_changes_prep_list
                                              (browser, e_contact, "web",
                                               ARRAY_LEN (E_CONTACT_FIELDS_WEB),
                                               E_CONTACT_FIELDS_WEB,
                                               CONTEXT_LIST_WEB);

  soylent_browser_person_view_save_changes_prep_im (browser, e_contact);
  soylent_browser_person_view_save_changes_prep_mail (browser, e_contact);

  /* Commit the changes */
  if (e_contact)
    {
      EBook *e_book = NULL;

      /* TODO: wipe person's fields that we deal with above
        *      eg, wipe all Email, IM, mail, etc. fields */
      /* TODO: fill in person's fields with data collected above */

      e_book = soylent_browser_get_e_book (browser);
      if (e_book)
        {
          EBookView *e_book_view = NULL;

          e_book_view = soylent_browser_get_e_book_view (browser);
          if (e_book_view)
            {
              SoylentPerson *person = NULL;

              person = soylent_browser_get_person_from_e_contact
                                                            (browser,
                                                              e_contact);
              if (person)
                {
                  gboolean commit_retval = FALSE;
                  GError *error = NULL;

                  commit_retval = e_book_commit_contact (e_book,
                                                          e_contact,
                                                          &error);
                  if (commit_retval)
                    {
                      retval = TRUE;
                    }
                  else
                    {
                      g_warning ("Could not commit changes to address "
                                  "book: %s",
                                  error ? error->message : "no error given");
                      g_clear_error (&error);
                    }
                }
              else
                {
                  g_warning ("failed to get SoylentPerson from EContact");
                }
            }
          else
            {
              g_critical ("failed to get the EBookView for the "
                          "SoylentBrowser");
            }
        }
      else
        {
          g_critical ("failed to get EBook for the SoylentBrowser");
        }
    }
  else
    {
      g_warning ("failed to get EContact for the person");
    }

  return retval;
}

static gboolean
soylent_browser_person_edit_save_scroll_cb (EBookView *book_view,
                                            const GList *e_contacts,
                                            SoylentPerson *person)
{
  gboolean retval = FALSE;

  if (person)
    {
      SoylentBrowser *browser = NULL;

      browser = soylent_person_get_browser (person);
      if (browser)
        {
          /* Just for the scrolling;
           * XXX: maybe that should be a separate function */
          retval = soylent_browser_person_select (browser, person, TRUE);
        }
    }
  else
    {
      g_warning ("soylent_browser_person_edit_save_scroll_cb: invalid Person");
    }

  g_signal_handlers_disconnect_by_func
                                    (book_view,
                                     soylent_browser_person_edit_save_scroll_cb,
                                     person);

  return retval;
}

/* Handle activation of an item in the Email action MenuToolButton
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_action_email_menu_shell_cb (GtkMenuShell *menu_chat,
                                                   gpointer user_data)
{
  gboolean retval = FALSE;
  GtkMenu *menu = NULL;
  GtkMenuItem *selected_item = NULL;

  g_return_val_if_fail (menu_chat, retval);
  g_return_val_if_fail (GTK_IS_MENU_SHELL (menu_chat), retval);

  menu = GTK_MENU (menu_chat);
  selected_item = GTK_MENU_ITEM (gtk_menu_get_active (menu));
  if (selected_item)
    {
      retval = soylent_browser_person_action_email_menu_item_cb (selected_item,
                                                                 user_data);
    }
  else
    {
      g_warning ("failed to get the selected Email menu item");
    }

  return retval;
}

/* Perform the actual action for an Email action MenuToolButton item
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_action_email_menu_item_cb (GtkMenuItem *item,
                                                  gpointer user_data)
{
  gboolean retval = FALSE;
  GList *selection_children = NULL;

  selection_children = gtk_container_get_children (GTK_CONTAINER (item));
  if (selection_children)
    {
      GtkLabel *email_addr_label = NULL;

      email_addr_label = GTK_LABEL (selection_children->data);
      if (email_addr_label)
        {
          const gchar *email_addr = NULL;

          email_addr = gtk_label_get_text (email_addr_label);
          if (email_addr)
            {
              gchar *mailto = NULL;
              gboolean url_show_retval = FALSE;
              GError *error = NULL;

              mailto = g_strdup_printf ("mailto:%s", email_addr);
              url_show_retval = gnome_url_show (mailto, &error);
              if (url_show_retval)
                {
                  retval = TRUE;
                }
              else
                {
                  g_warning ("Failed to open email window to %s: %s",
                             email_addr, error->message);
                  g_clear_error (&error);
                }

              g_free (mailto);
            }
          else
            {
              g_warning ("failed to get Email menu's current email address");
            }
        }
      else
        {
          g_warning ("failed to get Email menu's current email address label");
        }
    }
  else
    {
      g_warning ("failed to get selected Email menu item");
    }

  return retval;
}

/* Handle activation of an item in the Chat action MenuToolButton
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_action_chat_menu_shell_cb (GtkMenuShell *menu_chat,
                                                  gpointer user_data)
{
  gboolean retval = FALSE;
  GtkMenu *menu = NULL;
  GtkMenuItem *selected_item = NULL;

  g_return_val_if_fail (menu_chat, retval);
  g_return_val_if_fail (GTK_IS_MENU_SHELL (menu_chat), retval);

  menu = GTK_MENU (menu_chat);
  selected_item = GTK_MENU_ITEM (gtk_menu_get_active (menu));
  if (selected_item)
    {
      retval = soylent_browser_person_action_chat_menu_item_cb (selected_item,
                                                                user_data);
    }
  else
    {
      g_warning ("failed to get the selected Chat menu item");
    }

  return retval;
}

/* FIXME: this function has way too many levels of logic; split it up */
/* Perform the actual action for an Chat action MenuToolButton item
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_action_chat_menu_item_cb (GtkMenuItem *item,
                                                 gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  McAccount *account = NULL;
  SoylentPerson *person = NULL;
  EmpathyContactManager *live_manager = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
  */

  browser = (SoylentBrowser*) user_data;
  live_manager = soylent_browser_get_live_manager (browser);
  if (live_manager)
    {
      MissionControl *mission_control = NULL;

      mission_control = empathy_mission_control_new ();
      if (mission_control)
        {
          GList *selection_children = NULL;

          selection_children = gtk_container_get_children
                                                        (GTK_CONTAINER (item));
          if (selection_children)
            {
              GtkLabel *im_handle_label = NULL;

              im_handle_label = GTK_LABEL (selection_children->data);
              if (im_handle_label && GTK_IS_LABEL (im_handle_label))
                {
                  const gchar *im_handle = NULL;

                  im_handle = gtk_label_get_text (im_handle_label);
                  if (im_handle)
                    {
                      person = soylent_browser_get_selected_person (browser);
                      if (person)
                        {
                          GList *empathy_contacts = NULL;
                          EmpathyContact *empathy_contact = NULL;

                          empathy_contacts = soylent_person_get_empathy_contacts
                                                                       (person);
                          for (; empathy_contacts;
                               empathy_contacts = g_list_next
                                                            (empathy_contacts))
                            {
                              empathy_contact = EMPATHY_CONTACT
                                                      (empathy_contacts->data);
                              if (empathy_contact
                                  && g_str_equal (empathy_contact_get_id
                                                              (empathy_contact),
                                                  im_handle))
                                {
                                  break;
                                }
                            }

                          if (empathy_contact)
                            {
                              account = empathy_contact_get_account
                                                                (empathy_contact);
                              if (account)
                                {
                              mission_control_request_channel_with_string_handle
                                      (mission_control, account,
                                       TP_IFACE_CHANNEL_TYPE_TEXT, im_handle,
                                       TP_HANDLE_TYPE_CONTACT,
                                       soylent_browser_person_error_chat_new_cb,
                                       browser);

                                  retval = TRUE;
                                }
                              else
                                {
                                  g_warning ("Chat account is invalid");
                                }
                            }
                          else
                            {
                              g_warning ("selected person doesn't have the "
                                         "selected IM name");
                            }
                        }
                      else
                        {
                          g_warning ("failed to get the currently-selected"
                                     "person");
                        }
                    }
                  else
                    {
                      g_warning ("failed to get the current Chat handle");
                    }
                }
              else
                {
                  g_warning ("failed to get the current Chat handle label");
                }
            }
          else
            {
              g_warning ("failed to get the current Chat menu selection");
            }

          g_object_unref (mission_control);
        }
      else
        {
          g_warning ("unable to create new MissionControl");
        }
    }
  else
    {
      g_warning ("failed to get the EmpathyContactList");
    }

  return retval;
}

/* Fill the Email action MenuToolButton's menu based on the selected people
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_selected_email_menu_update_cb
                                                (GtkMenuToolButton *btntb_email,
                                                 gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GtkMenu *menu_email = NULL;
  GList *e_contacts = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
  */

  browser = (SoylentBrowser*) user_data;

  menu_email = GTK_MENU (gtk_menu_tool_button_get_menu (btntb_email));

  /* Clear out the menutoolbutton's pull-down menu and make it insensitive */
  gtk_container_foreach (GTK_CONTAINER (menu_email),
                         (GtkCallback) (gtk_widget_destroy), NULL);
  gtk_widget_set_sensitive (GTK_WIDGET (btntb_email), FALSE);

  e_contacts = soylent_browser_get_selected_people_e_contact (browser);
  if (e_contacts)
    {
      if (!g_list_next (e_contacts))
        {
          EContact *e_contact = NULL;

          e_contact = e_contacts->data;
          if (e_contact && E_CONTACT (e_contact))
            {
              const GList *email_addrs = NULL;
              const gchar *email_addr_cur = NULL;

              for (email_addrs = e_contact_get (e_contact, E_CONTACT_EMAIL);
                   email_addrs && (email_addr_cur = email_addrs->data);
                   email_addrs = g_list_next (email_addrs))
                {
                  GtkWidget *item = NULL;

                  item = gtk_menu_item_new_with_label (email_addr_cur);
                  g_signal_connect
                          (G_OBJECT (item), "activate",
                           G_CALLBACK
                             (soylent_browser_person_action_email_menu_item_cb),
                           NULL);

                  gtk_menu_shell_append (GTK_MENU_SHELL (menu_email), item);
                  gtk_widget_show (GTK_WIDGET (item));
                  gtk_widget_set_sensitive (GTK_WIDGET (btntb_email), TRUE);
                }

            }
        }
      else
        {
          /* TODO: implement this */
          g_warning ("Unimplemented feature: multi-person email");
        }
    }

  retval = TRUE;

  return retval;
}

/* Fill the Chat action MenuToolButton's menu based on the selected people
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_selected_chat_menu_update_cb
                                                (GtkMenuToolButton *btntb_chat,
                                                 gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  GtkMenu *menu_chat = NULL;
  GList *people = NULL;

  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
  */

  browser = (SoylentBrowser*) user_data;

  menu_chat = GTK_MENU (gtk_menu_tool_button_get_menu (btntb_chat));

  /* Clear out the menutoolbutton's pull-down menu and make it insensitive */
  gtk_container_foreach (GTK_CONTAINER (menu_chat),
                         (GtkCallback) (gtk_widget_destroy), NULL);
  gtk_widget_set_sensitive (GTK_WIDGET (btntb_chat), FALSE);

  /* TODO: support multiple people */
  people = soylent_browser_get_selected_people (browser);
  if (people)
    {
      if (!g_list_next (people))
        {
          SoylentPerson *person = NULL;

          person = people->data;
          if (person)
            {
              const GList *empathy_contacts = NULL;
              EmpathyContact *empathy_contact_cur = NULL;


              /* FIXME: this needs to be sync'd for presence updates, and must
               * only contain online IM names */
              for (empathy_contacts = soylent_person_get_empathy_contacts
                                                                      (person);
                   empathy_contacts
                   && (empathy_contact_cur = empathy_contacts->data);
                   empathy_contacts = g_list_next (empathy_contacts))
                {
                  GtkWidget *item = NULL;

                  item = gtk_menu_item_new_with_label
                                                    (empathy_contact_get_id
                                                        (empathy_contact_cur));
                  g_signal_connect
                          (G_OBJECT (item), "activate",
                           G_CALLBACK
                             (soylent_browser_person_action_chat_menu_item_cb),
                           browser);

                  gtk_menu_shell_append (GTK_MENU_SHELL (menu_chat), item);
                  gtk_widget_show (GTK_WIDGET (item));
                  gtk_widget_set_sensitive (GTK_WIDGET (btntb_chat), TRUE);
                }
            }
        }
      else
        {
          /* TODO: implement this */
          g_warning ("Unimplemented feature: multi-person chat");
        }
    }

  retval = TRUE;

  return retval;
}

/* After we create this new Person, automatically select them and open an edit
 * window for them
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_new_edit_cb (EBookView *book_view,
                                    const GList *e_contacts,
                                    cb_data_pre_person *pre_person_to_match)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  EContact *e_contact = NULL;
  SoylentPerson *person = NULL;

  g_return_val_if_fail (e_contacts, retval);
  g_return_val_if_fail (pre_person_to_match, retval);

  /* FIXME: once SoylentBrowser is a GObject, also check SOYLENT_IS_BROWSER */
  browser = pre_person_to_match->browser;
  e_contact = pre_person_to_match->e_contact;
  if (browser && e_contact && E_IS_CONTACT (e_contact))
    {
      const GList *l = NULL;

      person = soylent_browser_get_person_from_e_contact (browser, e_contact);
      soylent_browser_person_select (browser, person, TRUE);

      for (l = e_contacts;
           l && l->data && E_IS_CONTACT (l->data) && (retval == FALSE);
           l = g_list_next (l))
        {
          EContact *e_contact_cur = NULL;
          const gchar *e_uid = NULL;
          const gchar *e_uid_cur = NULL;
          
          e_contact_cur = E_CONTACT (l->data);
          e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
          e_uid_cur = e_contact_get_const (e_contact_cur, E_CONTACT_UID);
          /* If we've found our match */
          if (g_str_equal (e_uid, e_uid_cur))
            {
              retval = soylent_browser_view_set_mode (browser,
                                                      SB_PEOPLE_VIEW_MODE_EDIT);
            }
        }
    }
  else
    {
      g_warning ("invalid SoylentBrowser or EContact");
    }

  g_signal_handlers_disconnect_by_func (book_view,
                                        soylent_browser_person_new_edit_cb,
                                        pre_person_to_match);
  g_free (pre_person_to_match);

  return retval;
}

/* Save a new simple (single GtkEntry-based) detail for the person being edited 
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_detail_add_person_add_attr (SoylentBrowser *browser,
                                                   const gchar *attr_name,
                                                   const gchar *param_val)
{
  gboolean retval = FALSE;
  EVCard *e_vcard = NULL;
  EVCardAttribute *attr = NULL;
  GladeXML *wtree = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */
  g_return_val_if_fail (attr_name, retval);

  wtree = soylent_browser_get_widget_tree (browser);

  e_vcard = E_VCARD (soylent_browser_get_selected_person_e_contact (browser));
  if (e_vcard && E_IS_VCARD (e_vcard))
    {
      attr = e_vcard_attribute_new (NULL, attr_name);
      if (attr)
        {
          GtkEntry *entry_add = NULL;
          const gchar *attr_val = NULL;

          entry_add = GTK_ENTRY (glade_xml_get_widget (wtree,
                                                       "entry_detail_add"));
          attr_val = gtk_entry_get_text (entry_add);      

          if (param_val)
            {
              EVCardAttributeParam *param = NULL;

              param = e_vcard_attribute_param_new (EVC_TYPE);
              e_vcard_attribute_add_param_with_value (attr, param, param_val);

              /* Mimicking the way Evolution sets up phone VCard entries (for
               * better or worse...) */
              if (g_str_equal (attr_name, EVC_TEL)
                  && (g_str_equal (param_val, "HOME")
                      || g_str_equal (param_val, "WORK")))
                {
                  param = e_vcard_attribute_param_new (EVC_TYPE);
                  e_vcard_attribute_add_param_with_value (attr, param, "VOICE");
                }
            }

          e_vcard_add_attribute_with_value (e_vcard, attr, attr_val);

          retval = TRUE;
        }
    }

  return retval;
}

/* Save a new mailing address for the person being edited
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_detail_add_person_add_mail_attrs
                                                      (SoylentBrowser *browser,
                                                       const gchar *param_val)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EVCard *e_vcard = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */
  
  wtree = soylent_browser_get_widget_tree (browser);

  e_vcard = E_VCARD (soylent_browser_get_selected_person_e_contact (browser));
  if (e_vcard && E_IS_VCARD (e_vcard))
    {
      EContactAddress *addr = NULL;

      addr = g_new (EContactAddress, 1);
      if (addr)
        {
          GtkTextView *tv_add_street = NULL;
          GtkTextBuffer *tb_add_street = NULL;
          GtkEntry *entry_add_po = NULL;
          GtkEntry *entry_add_locality = NULL;
          GtkEntry *entry_add_region = NULL;
          GtkEntry *entry_add_code = NULL;
          GtkEntry *entry_add_country = NULL;
          GtkTextIter start;
          GtkTextIter end;
          EVCardAttribute *attr_adr = NULL;
          EVCardAttributeParam *param = NULL;
          EVCardAttribute *attr_label = NULL;

          entry_add_po = GTK_ENTRY (glade_xml_get_widget
                                                  (wtree,
                                                   "entry_person_mail_add_po"));
          entry_add_locality = GTK_ENTRY (glade_xml_get_widget
                                            (wtree,
                                             "entry_person_mail_add_locality"));
          entry_add_region= GTK_ENTRY (glade_xml_get_widget
                                            (wtree,
                                             "entry_person_mail_add_region"));
          entry_add_code = GTK_ENTRY (glade_xml_get_widget
                                              (wtree,
                                               "entry_person_mail_add_code"));
          entry_add_country = GTK_ENTRY (glade_xml_get_widget
                                            (wtree,
                                             "entry_person_mail_add_country"));
          tv_add_street = GTK_TEXT_VIEW (glade_xml_get_widget
                                                (wtree,
                                                 "tv_person_mail_add_street"));
          tb_add_street = gtk_text_view_get_buffer (tv_add_street);
          gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (tb_add_street), &start,
                                      &end);

          /* next two are dummy - just so the e_contact_address_free() below
           * doesn't cause a segfault */
          addr->address_format = g_strdup ("");
          addr->ext = g_strdup ("");
          addr->po = g_strdup (gtk_entry_get_text (entry_add_po));
          addr->street = g_strdup (gtk_text_buffer_get_text (tb_add_street,
                                                             &start, &end,
                                                             FALSE));
          addr->locality = g_strdup (gtk_entry_get_text (entry_add_locality));
          addr->region = g_strdup (gtk_entry_get_text (entry_add_region));
          addr->country = g_strdup (gtk_entry_get_text (entry_add_country));
          addr->code = g_strdup (gtk_entry_get_text (entry_add_code));

          /* Create the multi-field VCard "ADR" address entry */
          attr_adr = e_vcard_attribute_new (NULL, EVC_ADR);

          if (param_val)
            {
              param = e_vcard_attribute_param_new (EVC_TYPE);
              e_vcard_attribute_add_param_with_value (attr_adr, param,
                                                      param_val);
            }

          e_vcard_add_attribute_with_values (e_vcard, attr_adr, addr->po, "",
                                             addr->street, addr->locality,
                                             addr->region, addr->code,
                                             addr->country, NULL);

          /* Create the single-string VCard "LABEL" address entry */
          attr_label = e_vcard_attribute_new (NULL, "LABEL");
          if (param)
            {
              e_vcard_attribute_add_param
                                        (attr_label,
                                         e_vcard_attribute_param_copy (param));
            }
          e_vcard_add_attribute_with_value (e_vcard, attr_label,
                                            e_contact_address_to_label (addr));

          e_contact_address_free (addr);

          retval = TRUE;
        }
    }

  return retval;
}

/* FIXME: convert to support multivalued mail fields/widgets */
/* Propagate existing mailing address changes to the person-being-edited's
 * EContact
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_set_mail_fields_from_widgets (gpointer key,
                                                     gpointer value,
                                                     gpointer user_data)
{
  gboolean retval = FALSE;
  const gchar *widget_name = NULL;
  EContactField e_contact_field = 0;
  SoylentBrowser *browser = NULL;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  EContactAddress *addr = NULL;
  const gchar *context_str = NULL;
  guint fieldnum = 0;

  g_return_val_if_fail (key, retval);
  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */

  browser = (SoylentBrowser*) user_data;
  wtree = soylent_browser_get_widget_tree (browser);

  widget_name = (const gchar*) key;
  e_contact_field = *((EContactField*) value);

  /* format of widget_name is "mail_<context>", so <context> starts at pos. 5 */
  context_str = widget_name + 5;
  e_contact = soylent_browser_get_selected_person_e_contact (browser);
  addr = e_contact_get (e_contact, e_contact_field);

  if (!addr)
    {
      addr = g_new0 (EContactAddress, 1);
    }

  for (fieldnum = 0; fieldnum < MAIL_FIELDS_NUM; fieldnum++)
    {
      GtkWidget *widget_cur = NULL;
      gchar *widget_name = NULL;
      const gchar *contents_new = NULL;

      widget_name = g_strdup_printf ("%s_person_mail_%s_%s",
                                     MAIL_WIDGET_TYPE_STRS[fieldnum],
                                     context_str, MAIL_FIELD_STRS[fieldnum]);
      widget_cur = glade_xml_get_widget (wtree, widget_name);
      if (GTK_IS_ENTRY (widget_cur))
        {
          contents_new = gtk_entry_get_text (GTK_ENTRY (widget_cur));
        }
      else if (GTK_IS_TEXT_VIEW (widget_cur))
        {
          GtkTextBuffer *buf = NULL;
          GtkTextIter start;
          GtkTextIter end;

          buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget_cur));
          gtk_text_buffer_get_bounds (buf, &start, &end);
          contents_new = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
        }

      e_contact_address_set_field (addr, MAIL_FIELD_STRS[fieldnum],
                                   contents_new);
      g_free (widget_name);
    }

  if (e_contact_address_is_empty (addr))
    {
      e_contact_set (e_contact, e_contact_field, NULL);
    }
  else
    {
      e_contact_set (e_contact, e_contact_field, (gpointer) addr);
      e_contact_set (e_contact,
                     e_contact_field
                        - E_CONTACT_FIRST_ADDRESS_ID + E_CONTACT_FIRST_LABEL_ID,
                     (gpointer) e_contact_address_to_label (addr));
    }

  retval = TRUE;

  return retval;
}

/* Save a simple EContact field for the person being edited
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_set_field_simple (EContact *e_contact,
                                         EContactField e_contact_field,
                                         const gchar *contents_new)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);
  g_return_val_if_fail (E_CONTACT_FIELD_IS_VALID (e_contact_field), retval);

  if (contents_new && g_str_equal (contents_new, ""))
    {
      contents_new = NULL;
    }
  e_contact_set (e_contact, e_contact_field, (gpointer) contents_new);

  if (e_contact_field == E_CONTACT_FULL_NAME)
    {
      gchar *name_file_as = NULL;
      gchar *name_given = NULL;
      gchar *name_family = NULL;

      name_file_as = eds_get_name_file_as_from_full (contents_new);
      name_given = name_get_given_from_full (contents_new);
      name_family = name_get_family_from_full (contents_new);

      e_contact_set (e_contact, E_CONTACT_FILE_AS, name_file_as);
      e_contact_set (e_contact, E_CONTACT_GIVEN_NAME, name_given);
      e_contact_set (e_contact, E_CONTACT_FAMILY_NAME, name_family);

      g_free (name_file_as);
      g_free (name_given);
      g_free (name_family);
    }

  retval = TRUE;

  return retval;
}

/* Update the entire Person View UI section
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_person_view_update (SoylentBrowser *browser,
                                    SoylentPerson *person)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  retval = TRUE;

  retval &= soylent_browser_person_view_phone_update (browser, person);
  retval &= soylent_browser_person_view_im_update (browser, person);
  retval &= soylent_browser_person_view_email_update (browser, person);
  retval &= soylent_browser_person_view_mail_update (browser, person);
  retval &= soylent_browser_person_view_web_update (browser, person);
  retval &= soylent_browser_person_view_name_update (browser, person);
  retval &= soylent_browser_person_view_avatar_update (browser, person);
  retval &= soylent_browser_person_view_detail_add_update (browser);

  return retval;
}

/* Update the Detail Add section widgets based on broader, related widgets
 *
 * Eg, if we set the Type of Detail combo box to Mailing Address, display the
 * mailing address field widgets and hide the single GtkEntry for single-value
 * details
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_detail_add_update (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  gboolean cbox_setup_retval = FALSE;
  GladeXML *wtree = NULL;
  GtkEntry *entry_detail_add = NULL;
  gint i = 0;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  entry_detail_add = GTK_ENTRY (glade_xml_get_widget (wtree,
                                                      "entry_detail_add"));
  cbox_setup_retval = soylent_browser_person_detail_add_cbox_setup (browser,
                                                                    FALSE);
  if (cbox_setup_retval)
    {
      gtk_entry_set_text (entry_detail_add, "");

      /* Set up 'changed' signals for the mail "Detail Add" fields */
      for (i = 0; i < MAIL_FIELDS_NUM; i++)
        {
          GtkWidget *widget_cur = NULL;
          gchar *widget_name = NULL;

          widget_name = g_strdup_printf ("%s_person_mail_add_%s",
                                         MAIL_WIDGET_TYPE_STRS[i],
                                         MAIL_FIELD_STRS[i]);
          widget_cur = glade_xml_get_widget (wtree, widget_name);
          if (GTK_IS_TEXT_VIEW (widget_cur))
            {
              GtkTextBuffer *buf = NULL;

              buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget_cur));
              gtk_text_buffer_set_text (buf, "", -1);
            }
          else if (GTK_IS_ENTRY (widget_cur))
            {
              gtk_entry_set_text (GTK_ENTRY (widget_cur), "");
            }

          g_free (widget_name);
        }

      retval = TRUE;
    }

  return retval;
}

/* Update the Person View:Email UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_email_update (SoylentBrowser *browser,
                                          SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkEntry *entry_email = NULL;
  GtkHBox *hbox_email = NULL;
  gchar *widget_name = NULL;
  gchar *email_str = NULL;
  guint email_list_pos = 0;
  GList *l = NULL;
  guint num_entries_set = 0;
  GtkFrame *frame_email = NULL;
  GtkFixed *fixed_email = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  frame_email = GTK_FRAME (glade_xml_get_widget (wtree, "frame_person_email"));
  fixed_email = GTK_FIXED (glade_xml_get_widget (wtree, "fixed_person_email"));

  e_contact = soylent_person_get_e_contact (person);
  if (e_contact && E_IS_CONTACT (e_contact))
    {
      GList *email_strs_head = NULL;

      /* Display up to MAX_GUI_EMAIL email addresses in the Email section of the
       * person editor */
      email_strs_head = e_contact_get (e_contact, E_CONTACT_EMAIL);
      for (l = email_strs_head;
           l && email_list_pos < MAX_GUI_EMAIL;
           l = g_list_next (l), email_list_pos++)
        {
          widget_name = g_strdup_printf ("entry_person_email_%c",
                                        '1' + email_list_pos);
          entry_email = GTK_ENTRY (glade_xml_get_widget (wtree, widget_name));
          g_free (widget_name);
    
          widget_name = g_strdup_printf ("hbox_person_email_%c",
                                        '1' + email_list_pos);
          hbox_email = GTK_HBOX (glade_xml_get_widget (wtree, widget_name));
          g_free (widget_name);

          email_str = (gchar*) l->data;
          if (email_str)
            {
              gtk_entry_set_text (entry_email, email_str);
              gtk_widget_show (GTK_WIDGET (hbox_email));
              num_entries_set++;
            }
          else
            {
              gtk_entry_set_text (entry_email, "");
              gtk_widget_hide (GTK_WIDGET (hbox_email));
            }

          g_free (email_str);
        }

      /* If the number of email addresses > MAX_GUI_EMAIL, free the strings we
       * couldn't display */
      for (; l; l = g_list_next (l))
        {
          email_str = (gchar*) l->data;
          g_free (email_str);
        }
      g_list_free (email_strs_head);

      /* If the number of email addresses < MAX_GUI_EMAIL, hide unused email
       * GtkEntrys */
      for (; email_list_pos < MAX_GUI_EMAIL; email_list_pos++)
        {
          widget_name = g_strdup_printf ("hbox_person_email_%c",
                                        '1' + email_list_pos);
          hbox_email = GTK_HBOX (glade_xml_get_widget (wtree, widget_name));
          g_free (widget_name);

          gtk_widget_hide (GTK_WIDGET (hbox_email));
        }

      if (num_entries_set == 0)
        {
          gtk_widget_hide (GTK_WIDGET (frame_email));
          gtk_widget_hide (GTK_WIDGET (fixed_email));
        }
      else
        {
          gtk_widget_show (GTK_WIDGET (frame_email));
          gtk_widget_show (GTK_WIDGET (fixed_email));
        }

      retval = TRUE;
    }

  return retval;
}

/* FIXME - this needs a stick of live dynamite throw its way */
/* Update the Person View:IM UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_im_update (SoylentBrowser *browser,
                                       SoylentPerson *person)
{
  gboolean retval = FALSE;
  const guint CONTEXT_NUM = 2;
  const guint PROTO_NUM = 6;
  const guint ENTRY_POS_INIT = 0;
  guint entry_pos[2];
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  const gchar *im_name = NULL;
  EVCardAttribute *attr = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  e_contact = soylent_person_get_e_contact (person);
  if (e_contact && E_IS_CONTACT (e_contact))
    {
      guint context = 0;
      guint proto = 0;


      soylent_debug ("\n%s\n", 
                     e_vcard_to_string (E_VCARD (e_contact),
                                        EVC_FORMAT_VCARD_30));



      entry_pos[CONTEXT_HOME] = entry_pos[CONTEXT_WORK] = ENTRY_POS_INIT;
      for (proto = E_CONTACT_IM_AIM;
           proto < (E_CONTACT_IM_AIM + PROTO_NUM);
           proto++)
        {
          for (context = CONTEXT_IM_FIRST;
               context <= CONTEXT_IM_LAST;
               context++)
            {
              GList *im_attr_list = NULL;
              GList *l = NULL;

              im_attr_list = e_contact_get_attributes (e_contact, proto);
              for (l = im_attr_list;
                   l && entry_pos[context] < IM_FIELDS_PER_CONTEXT;
                   l = g_list_next(l))
                {
                  gboolean param_type_is_work = FALSE;
                  GList *param_type = NULL;

                  attr = l->data;
                  im_name = e_vcard_attribute_get_value (attr);
                  param_type = e_vcard_attribute_get_param (attr, EVC_TYPE);

                  param_type_is_work = (param_type
                                        && !g_ascii_strcasecmp
                                              (param_type->data,
                                                CONTEXT_STRS[CONTEXT_WORK]));

                  /* "HOME" is the catch-all, since that's how Evo does it */
                  if (((context == CONTEXT_WORK) && param_type_is_work)
                      || ((context == CONTEXT_HOME) && !param_type_is_work))
                    {
                      GtkHBox *hbox_im = NULL;
                      GtkLabel *label_im = NULL;
                      GtkEntry *entry_im = NULL;
                      gchar *widget_name = NULL;

                      widget_name = g_strdup_printf ("hbox_person_im_%s_%c",
                                                     CONTEXT_STRS[context],
                                                     '1' + entry_pos[context]);
                      hbox_im = GTK_HBOX (glade_xml_get_widget (wtree,
                                                                widget_name));
                      g_free (widget_name);

                      widget_name = g_strdup_printf ("entry_person_im_%s_%c",
                                                     CONTEXT_STRS[context],
                                                     '1' + entry_pos[context]);
                      entry_im = GTK_ENTRY (glade_xml_get_widget (wtree,
                                                                  widget_name));
                      g_free (widget_name);

                      widget_name = g_strdup_printf ("label_person_im_%s_%c",
                                                     CONTEXT_STRS[context],
                                                     '1' + entry_pos[context]);
                      label_im = GTK_LABEL (glade_xml_get_widget (wtree,
                                                                  widget_name));
                      g_free (widget_name);

                      if (im_name)
                        {
                          /* The IM attributes are named "X-<IM PROTO NAME>", so
                           * we're skipping the first two characters to get to
                           * the actual IM protocol name */
                          gtk_label_set_text
                                      (label_im,
                                       e_vcard_attribute_get_name (attr) + 2);
                          gtk_entry_set_text (entry_im, im_name);

                          gtk_widget_show (GTK_WIDGET (hbox_im));
                        }

                      entry_pos[context]++;
                    }
                  else
                    {
                      /* The current IM field TYPE isn't what we're handling in
                       * this loop and we aren't handling HOME (the fallback for
                       * unknown/mangled TYPEs) */
                    }
                }
            }
        }

      /* Show or hide frames depending on whether their contents were updated
       * above */
        {
          GtkFrame *frame_im = NULL;
          guint i = 0;
          guint frames_visible = 0;

          frame_im = GTK_FRAME (glade_xml_get_widget (wtree,
                                                      "frame_person_im"));

          for (i = 0; i < CONTEXT_NUM; i++)
            {
              GtkFrame *frame_im_cur = NULL;
              gchar *widget_name = NULL;

              widget_name = g_strdup_printf ("frame_person_im_%s",
                                            CONTEXT_STRS[i]);
              frame_im_cur = GTK_FRAME (glade_xml_get_widget (wtree,
                                                              widget_name));

              if (entry_pos[i] == ENTRY_POS_INIT)
                {
                  gtk_widget_hide (GTK_WIDGET (frame_im_cur));
                }
              else
                {
                  frames_visible++;
                  gtk_widget_show (GTK_WIDGET (frame_im_cur));
                }

              g_free (widget_name);
            }

          if (frames_visible > 0)
            {
              gtk_widget_show (GTK_WIDGET (frame_im));
            }
          else
            {
              gtk_widget_hide (GTK_WIDGET (frame_im));
            }
        }

      /* Hide unfilled hboxes (if the lists combined contain fewer entries in
       * each context than IM_FIELDS_PER_CONTEXT) */
      for (context = CONTEXT_FIRST; context < CONTEXT_NUM; context++)
      {
        for (; entry_pos[context] < IM_FIELDS_PER_CONTEXT; entry_pos[context]++)
          {
            gchar *widget_name = NULL;
            GtkHBox *hbox_im = NULL;

            widget_name = g_strdup_printf ("hbox_person_im_%s_%c",
                                           CONTEXT_STRS[context],
                                           '1' + entry_pos[context]);
            hbox_im = GTK_HBOX (glade_xml_get_widget (wtree, widget_name));

            gtk_widget_hide (GTK_WIDGET (hbox_im));
            g_free (widget_name);
          }
      }

      /* If we got this far, we're handling a real person (whether they have any
       * IM fields on record doesn't really change whether this function
       * succeeded or not) */
      retval = TRUE;
    }

  return retval;
}

/* Update the Person View:Avatar UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_avatar_update (SoylentBrowser *browser,
                                           SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkImage *img_person_view = NULL;
  EContactPhoto *photo = NULL;
  GdkPixbuf *pixbuf = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  e_contact = soylent_person_get_e_contact (person);
  img_person_view = GTK_IMAGE (glade_xml_get_widget (wtree, "img_person_view"));
  photo = e_contact_get (e_contact, E_CONTACT_PHOTO);

  if (photo && photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
    {
      guint icon_height_max = 0;
      guint icon_width_max  = 0;

      icon_height_max = soylent_browser_get_icon_height_max (browser);
      icon_width_max  = soylent_browser_get_icon_width_max  (browser);

      pixbuf = gdk_pixbuf_from_inline_photo (photo, icon_width_max,
                                             icon_height_max);
      if (pixbuf)
        {
          gtk_image_set_from_pixbuf (img_person_view, pixbuf);
          gtk_widget_show (GTK_WIDGET (img_person_view));

          g_object_unref (pixbuf);

          retval = TRUE;
        }
      else
        {
          g_warning ("failed to get a person's avatar");
        }
    }
  else
    {
      gtk_image_clear (img_person_view);

      retval = TRUE;
    }

  return retval;
}

/* Update the Person View:Mail UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_mail_update (SoylentBrowser *browser,
                                         SoylentPerson *person)
{
  const guint CONTEXT_NUM = 3;
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkFrame *frame_mail = NULL;
  guint context = 0;
  guint frames_visible = 0;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  e_contact = soylent_person_get_e_contact (person);
  frame_mail = GTK_FRAME (glade_xml_get_widget (wtree, "frame_person_mail"));

  for (context = 0; context < CONTEXT_NUM; context++)
    {
      GtkTextView *tv_street = NULL;
      GtkTextBuffer *tb_street = NULL;
      GtkEntry *entry_po = NULL;
      GtkEntry *entry_locality = NULL;
      GtkEntry *entry_code = NULL;
      GtkEntry *entry_region = NULL;
      GtkEntry *entry_country = NULL;
      GtkFrame *frame_mail_cur = NULL;
      gchar *widget_name = NULL;
      const gchar *context_str = NULL;
      EContactAddress *addr = NULL;

      context_str = CONTEXT_STRS[context];

      frame_mail_cur = GTK_FRAME (soylent_browser_get_widget (browser, "frame",
                                                              "mail",
                                                              context_str,
                                                              NULL));

      widget_name = g_strdup_printf ("tv_person_mail_%s_street",
                                    CONTEXT_STRS[context]);
      tv_street = GTK_TEXT_VIEW (glade_xml_get_widget (wtree, widget_name));
      tb_street = gtk_text_view_get_buffer (tv_street);
      g_free (widget_name);

      entry_po = GTK_ENTRY (soylent_browser_get_widget (browser, "entry",
                                                        "mail", context_str,
                                                        "po"));
      entry_locality = GTK_ENTRY (soylent_browser_get_widget (browser, "entry",
                                                              "mail",
                                                              context_str,
                                                              "locality"));
      entry_code = GTK_ENTRY (soylent_browser_get_widget (browser, "entry",
                                                          "mail", context_str,
                                                          "code"));
      entry_region = GTK_ENTRY (soylent_browser_get_widget (browser, "entry",
                                                            "mail", context_str,
                                                            "region"));
      entry_country = GTK_ENTRY (soylent_browser_get_widget (browser, "entry",
                                                             "mail",
                                                             context_str,
                                                             "country"));

      addr = e_contact_get (e_contact, context + E_CONTACT_FIRST_ADDRESS_ID);
      if (addr)
        {
          gtk_widget_show (GTK_WIDGET (frame_mail_cur));
          frames_visible++;

          gtk_text_buffer_set_text (tb_street, addr->street, -1);
          gtk_entry_set_text (entry_po, addr->po);
          gtk_entry_set_text (entry_code, addr->code);
          gtk_entry_set_text (entry_locality, addr->locality);
          gtk_entry_set_text (entry_region, addr->region);
          gtk_entry_set_text (entry_country, addr->country);

          e_contact_address_free (addr);
        }
      else
        {
          gtk_widget_hide (GTK_WIDGET (frame_mail_cur));
        }
    }

  if (frames_visible > 0)
    {
      g_assert (GTK_IS_WIDGET (frame_mail));
      gtk_widget_show (GTK_WIDGET (frame_mail));
    }
  else
    {
      g_assert (GTK_IS_WIDGET (frame_mail));
      gtk_widget_hide (GTK_WIDGET (frame_mail));
    }

  /* if we get this far, this function succeeded */
  retval = TRUE;

  return retval;
}

/* Update the Person View:Name UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_name_update (SoylentBrowser *browser,
                                         SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkEntry *entry_name = NULL;
  const gchar *name = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */
  wtree = soylent_browser_get_widget_tree (browser);
  e_contact = soylent_person_get_e_contact (person);
  entry_name = GTK_ENTRY (glade_xml_get_widget (wtree, "entry_person_name"));
  /* TODO: use the <b></b> markup */
  name = display_name_from_e_contact (e_contact);

  gtk_entry_set_text (entry_name, name);

  /*
  soylent_debug ("\n%s\n", 
                 e_vcard_to_string (E_VCARD (e_contact), EVC_FORMAT_VCARD_30));
  */

  /* if we get this far, this function succeeded */
  retval = TRUE;

  return retval;
}

/* Update the Person View:Phone UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_phone_update (SoylentBrowser *browser,
                                          SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkEntry *entry_phone = NULL;
  GtkHBox *hbox_phone = NULL;
  GtkFrame *frame_phone = NULL;
  GtkFixed *fixed_phone = NULL;
  /* FIXME: Find a way to use the CONTEXT_STRS instea of re-inventing it
   * Maybe make CONTEXT_MOBILE equal to CONTEXT_OTHER. Or make it the 4th
   * position in CONTEXT_STRS and have this loop skip over CONTEXT_OTHER */
  gchar *widget_name = NULL;
  guint context = 0;
  guint num_entries_set = 0;
  const gchar *phone_str = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  e_contact = soylent_person_get_e_contact (person);
  frame_phone = GTK_FRAME (glade_xml_get_widget (wtree, "frame_person_phone"));
  fixed_phone = GTK_FIXED (glade_xml_get_widget (wtree, "fixed_person_phone"));

  for (context = 0; context < ARRAY_LEN (CONTEXT_LIST_PHONE); context++)
    {
      widget_name = g_strdup_printf ("entry_person_phone_%s",
                                     CONTEXT_LIST_PHONE[context]);

      entry_phone = GTK_ENTRY (glade_xml_get_widget (wtree, widget_name));
      g_free (widget_name);

      widget_name = g_strdup_printf ("hbox_person_phone_%s",
                                     CONTEXT_LIST_PHONE[context]);
      hbox_phone = GTK_HBOX (glade_xml_get_widget (wtree, widget_name));
      g_free (widget_name);

      phone_str = e_contact_get_const (e_contact,
                                       E_CONTACT_FIELDS_PHONE[context]);
      if (phone_str)
        {
          gtk_entry_set_text (entry_phone, phone_str);
          num_entries_set++;

          gtk_widget_show (GTK_WIDGET (hbox_phone));
        }
      else
        {
          gtk_widget_hide (GTK_WIDGET (hbox_phone));
        }
    }

  /* FIXME We should probably include at least some of the other phone numbers*/

  if (num_entries_set == 0)
    {
      gtk_widget_hide (GTK_WIDGET (frame_phone));
      gtk_widget_hide (GTK_WIDGET (fixed_phone));
    }
  else
    {
      gtk_widget_show (GTK_WIDGET (frame_phone));
      gtk_widget_show (GTK_WIDGET (fixed_phone));
    }

  /* if we get this far, this function succeeded */
  retval = TRUE;

  return retval;
}

/* Update the Person View:Web URLs UI section
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_web_update (SoylentBrowser *browser,
                                        SoylentPerson *person)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  EContact *e_contact = NULL;
  GtkEntry *entry_web = NULL;
  GtkHBox *hbox_web = NULL;
  GtkFrame *frame_web = NULL;
  gchar *widget_name = NULL;
  guint context = 0;
  guint num_entries_set = 0;
  const gchar *web_name = NULL;


  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (PERSON), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  e_contact = soylent_person_get_e_contact (person);
  frame_web = GTK_FRAME (glade_xml_get_widget (wtree, "frame_person_web"));

  for (context = 0; context < ARRAY_LEN (CONTEXT_LIST_WEB); context++)
    {
      widget_name = g_strdup_printf ("entry_person_web_%s",
                                     CONTEXT_LIST_WEB[context]);

      entry_web = GTK_ENTRY (glade_xml_get_widget (wtree, widget_name));
      g_free (widget_name);

      widget_name = g_strdup_printf ("hbox_person_web_%s",
                                     CONTEXT_LIST_WEB[context]);
      hbox_web = GTK_HBOX (glade_xml_get_widget (wtree, widget_name));
      g_free (widget_name);

      web_name = e_contact_get_const (e_contact, E_CONTACT_FIELDS_WEB[context]);

      /* Apparently e_contact_get_const sometimes returns a pointer to an empty
       * string instead of NULL */
      if (web_name && *web_name != '\0')
        {
          gtk_entry_set_text (entry_web, web_name);
          num_entries_set++;

          gtk_widget_show (GTK_WIDGET (hbox_web));
        }
      else
        {
          gtk_widget_hide (GTK_WIDGET (hbox_web));
        }
    }

  if (num_entries_set == 0)
    {
      gtk_widget_hide (GTK_WIDGET (frame_web));
    }
  else
    {
      gtk_widget_show (GTK_WIDGET (frame_web));
    }

  /* if we get this far, this function succeeded */
  retval = TRUE;

  return retval;
}

/* Return an uppercased copy of the contents of the given GtkComboBox or NULL
 * for any failure. */
static gchar*
soylent_browser_person_detail_add_get_cbox_val (SoylentBrowser *browser,
                                                const gchar *cbox_name)
{
  gchar *retval = NULL;
  gchar *retval_lc = NULL;
  GladeXML *wtree = NULL;
  GtkComboBox *cbox = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
   */
  g_return_val_if_fail (cbox_name, retval);

  wtree = soylent_browser_get_widget_tree (browser);
  cbox = GTK_COMBO_BOX (glade_xml_get_widget (wtree, cbox_name));
  retval_lc = gtk_combo_box_get_active_text (cbox);
  retval = g_ascii_strup (retval_lc, -1);

  g_free (retval_lc);

  return retval;
}

/* Return a copy of the VCard name for the active Detail Add:Web GtkComboBox or
 * NULL for any failure. */
static gchar*
soylent_browser_person_detail_add_get_web_attr_name (SoylentBrowser *browser)
{
  const gchar *menu_strs[] = {"Home Page", "Blog", "Calendar", "Video Phone",
                              NULL,};
  const gchar *attr_strs[] = {EVC_URL, EVC_X_BLOG_URL, EVC_CALURI,
                              EVC_X_VIDEO_URL, NULL,};

  gchar *retval = NULL;
  GladeXML *wtree = NULL;
  GtkComboBox *cbox = NULL;
  gchar *attr_val_menu = NULL;
  guint i;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  cbox = GTK_COMBO_BOX (glade_xml_get_widget (wtree, "cbox_detail_type_web"));
  attr_val_menu = gtk_combo_box_get_active_text (cbox);
  for (i = 0; menu_strs[i]; i++)
    {
      if (g_str_equal (attr_val_menu, menu_strs[i]))
        {
          retval = g_strdup (attr_strs[i]);
        }
    }

  if (!retval)
    {
      g_warning ("Didn't find a supported VCard attr. name in menu");
    }

  g_free (attr_val_menu);

  return retval;
}

/* Update the action widgets based on new Person View selection
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_view_update_widgets_actions (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  gboolean email_retval = FALSE;
  GladeXML *wtree = NULL;
  GtkIconView *iv = NULL;
  GtkMenuToolButton *btntb_email = NULL;
  GtkMenuToolButton *btntb_chat = NULL;
  GList *selected_people = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  iv = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  btntb_email = GTK_MENU_TOOL_BUTTON (glade_xml_get_widget (wtree,
                                                            "btntb_email"));
  btntb_chat = GTK_MENU_TOOL_BUTTON (glade_xml_get_widget (wtree,
                                                           "btntb_chat"));

  selected_people = gtk_icon_view_get_selected_items (iv);

  email_retval = soylent_browser_person_selected_email_menu_update_cb
                                                                  (btntb_email,
                                                                   browser);
  if (email_retval)
    {
      retval = soylent_browser_person_selected_chat_menu_update_cb (btntb_chat,
                                                                    browser);
    }

  if (selected_people)
    {
      g_list_foreach (selected_people, (GFunc) gtk_tree_path_free, NULL);
      g_list_free (selected_people);
    }

  return retval;
}
