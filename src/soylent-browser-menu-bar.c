/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <string.h>
#include "eds-utils.h"
#include "soylent.h"
#include "soylent-browser.h"
#include "soylent-browser-menu-bar.h"
#include "soylent-utils.h"

static gboolean soylent_browser_menu_bar_edit_update (SoylentBrowser *browser);

/* Initializes everything necessary for the functions in this file
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_menu_bar_setup (SoylentBrowser *browser)
{
  const widget_signal_args_t const signal_handlers[] =
    {
      /* File menu */
      {"imagemenuitem_new_person", "activate",
       soylent_browser_person_action_new_person_cb, CB_DATA_SIMPLE, browser},
      /* TODO: new group */
      {"imagemenuitem_quit", "activate",
       gtk_main_quit, CB_DATA_SIMPLE, browser},

      /* Edit menu */
      {"imagemenuitem_edit", "activate",
       soylent_browser_person_action_edit_selected_cb, CB_DATA_SIMPLE, browser},
      /* TODO: cut */
      /* TODO: copy */
      /* TODO: paste */
      {"imagemenuitem_delete", "activate",
       soylent_browser_person_action_delete_selected_cb, CB_DATA_SIMPLE,
       browser},

      /* Help menu */
      {"imagemenuitem_about", "activate",
       soylent_browser_dialog_about_show_cb, CB_DATA_SIMPLE, browser},
    };
  gboolean retval = FALSE;
  gint signals_num = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  signals_num = ARRAY_LEN (signal_handlers);
  retval = widget_signal_connect_mass (browser, signal_handlers, signals_num);

  return retval;
}

/* Update the menu bar's menus' items for current selection
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
soylent_browser_menu_bar_update (SoylentBrowser *browser)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  retval = TRUE;
  /* No need to update File menu */
  retval &= soylent_browser_menu_bar_edit_update (browser);
  /* No need to update Help menu */

  return retval;
}

/* Update the Edit menu's items for current selection
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_menu_bar_edit_update (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkWidget *menu_edit_edit = NULL;
  GtkWidget *menu_edit_delete = NULL;
  GList *people = NULL;
  gint people_num = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);
  menu_edit_edit = glade_xml_get_widget (wtree, "imagemenuitem_edit");
  menu_edit_delete = glade_xml_get_widget (wtree, "imagemenuitem_delete");

  people = soylent_browser_get_selected_people (browser);
  people_num = g_list_length (people);

  /* initialize the sensitivity of the Edit menu items for null selection */
  gtk_widget_set_sensitive (menu_edit_delete, FALSE);
  gtk_widget_set_sensitive (menu_edit_edit, FALSE);

  if (people_num >= 1)
    {
      /* the Soylent person editor only handles 1 person at a time */
      if (people_num == 1)
        {
          gtk_widget_set_sensitive (menu_edit_edit, TRUE);
        }

      gtk_widget_set_sensitive (menu_edit_delete, TRUE);

      retval = TRUE;
    }
  else if (people_num == 0)
    {
      retval = TRUE;
    }

  return retval;
}
