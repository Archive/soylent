/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_PERSON_H_
#define _SOYLENT_PERSON_H_

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnome/libgnome.h>

#include <libebook/e-book.h>

#include <libmissioncontrol/mc-account.h>
#include <libmissioncontrol/mc-account-monitor.h>
#include <libmissioncontrol/mission-control.h>

#include <libempathy/empathy-debug.h>
#include <libempathy/empathy-utils.h>
#include <libempathy/empathy-contact.h>
#include <libempathy/empathy-tp-chat.h>
#include <libempathy/empathy-idle.h>
#include <libempathy/empathy-contact-manager.h>
#include <libempathy/empathy-contact-list.h>
#include <libempathy-gtk/empathy-ui-utils.h>

typedef struct SoylentPerson_tag SoylentPerson;

#include "soylent-browser.h"
#include "soylent-defs.h"

SoylentPerson* soylent_person_new (SoylentBrowser *sb, EContact *e_contact);
EContact* soylent_person_get_e_contact (SoylentPerson *person);
GList* soylent_person_get_empathy_contacts (SoylentPerson *person);
SoylentPresence soylent_person_get_presence (SoylentPerson *person);
GtkTreeIter* soylent_person_get_icon_view_iter (SoylentPerson *person);
SoylentBrowser* soylent_person_get_browser (SoylentPerson *person);
GdkPixbuf* soylent_person_get_avatar (SoylentPerson *person,
                                      guint icon_width_max,
                                      guint icon_height_max);
gchar* soylent_person_get_display_name_with_presence (SoylentPerson *person);
gboolean soylent_person_set_e_contact (SoylentPerson *person,
                                       EContact *e_contact);
gboolean soylent_person_set_empathy_contacts (SoylentPerson *person,
                                              GList *empathy_contacts);
gboolean soylent_person_add_empathy_contact (SoylentPerson *person,
                                             EmpathyContact *empathy_contact);
gboolean soylent_person_remove_empathy_contact
                                              (SoylentPerson *person,
                                               EmpathyContact *empathy_contact);

gint soylent_person_live_presence_compare (SoylentPresence presence1,
                                           SoylentPresence presence2);
gboolean soylent_person_is_online (SoylentPerson *person);

void soylent_person_live_groups_updated_cb (EmpathyContact *empathy_contact,
                                            GParamSpec *param,
                                            SoylentPerson *person);
gboolean soylent_person_live_presence_updated_cb
                                              (EmpathyContact *empathy_contact,
                                               GParamSpec *param,
                                               SoylentPerson *person);
void soylent_person_live_name_updated_cb (EmpathyContact *empathy_contact,
                                          GParamSpec *param,
                                          SoylentPerson *person);
gboolean soylent_person_live_avatar_updated_cb (EmpathyContact *empathy_contact,
                                                GParamSpec *param,
                                                SoylentPerson *person);
void soylent_person_live_type_updated_cb (EmpathyContact *empathy_contact,
                                          GParamSpec *param,
                                          SoylentPerson *person);

/*
SoylentPresence soylent_person_presence_from_empathy_presence
                                                    (EmpathyPresence *presence);
                                                    */

SoylentPresence soylent_person_presence_from_mc_presence (McPresence presence);

gboolean soylent_person_action_communicate (SoylentPerson *person);

void soylent_person_free (SoylentPerson *person);

#endif /* _SOYLENT_PERSON_H_ */
