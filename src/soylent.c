/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *  Author: Rob Taylor <rob.taylor@codethink.co.uk>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <config.h>

#include "soylent.h"
#include "soylent-browser.h"
#include "soylent-utils.h"

int
main (int argc, char *argv[])
{
  int retval = -1;
  GOptionContext *context = NULL;
  SoylentBrowser *browser = NULL;

  /* TODO: figure out INTLTOOL setup
  * bindtextdomain (GETTEXT_PACKAGE, CONTACTS_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
  */

  context = g_option_context_new (PACKAGE);
  g_option_context_add_group (context, gtk_get_option_group (TRUE));
  g_option_context_parse (context, &argc, &argv, NULL);

  gnome_program_init (PACKAGE, VERSION, LIBGNOME_MODULE, argc,
                      argv, GNOME_PARAM_GOPTION_CONTEXT, context,
                      GNOME_PARAM_NONE);

  browser = soylent_browser_new ();
  if (browser)
    {
      /* start the event loop */
      gtk_main ();

      soylent_browser_destroy (browser);

      retval = 0;
    }
  else
    {
      g_critical ("failed to create the SoylentBrowser");
    }

  return retval;
}
