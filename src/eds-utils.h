/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *  Author: Rob Taylor <codethink.co.uk>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _EDS_UTILS_H_
#define _EDS_UTILS_H_

#include <glib.h>
#include <libebook/e-book.h>
#include "soylent-defs.h"

#define E_CONTACT_FIELD_IS_VALID(x) (((x) >= E_CONTACT_FIELD_FIRST) \
                                      && ((x) < E_CONTACT_FIELD_LAST))

GdkPixbuf* gdk_pixbuf_from_inline_photo (EContactPhoto *photo,
                                         guint icon_width_max,
                                         guint icon_height_max);
const gchar* display_name_from_e_contact (EContact *e_contact);

/* Editting existing VCard/EContact details */
gboolean e_contact_address_set_field (EContactAddress *addr,
                                      const gchar *field_name,
                                      const gchar *value);
gboolean e_contact_address_is_empty  (EContactAddress *addr);
gchar*   e_contact_address_to_label  (EContactAddress *addr);

gboolean e_vcard_attr_list_set_value (EContact *e_contact,
                                      EContactField field_id, const gchar *type,
                                      guint abs_num, const gchar *value);

gboolean e_vcard_attr_list_prepend_value (EContact *e_contact,
                                          EContactField field_id,
                                          const gchar *type,
                                          const gchar *value);

gchar* eds_get_name_file_as_from_full (const gchar *full_name);
const gchar* eds_im_field_id_to_vcard_attr (EContactField field_id);

#endif /* _EDS_UTILS_H_ */
