/*
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *  Author: Rob Taylor <codethink.co.uk>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gdk-pixbuf-loader.h>
#include <libebook/e-book.h>
#include <libebook/e-contact.h>
#include <libebook/e-vcard.h>

#include "eds-utils.h"
#include "soylent-utils.h"

/* Get the GdkPixbuf representation of a photo inlined in an EContact */
GdkPixbuf*
gdk_pixbuf_from_inline_photo (EContactPhoto *photo, guint icon_width_max,
                              guint icon_height_max)
{
  GdkPixbuf *retval = NULL;
  int width = -1;
  int height = -1;
  GdkPixbuf *pixbuf = NULL;
  GdkPixbufLoader *loader = NULL;

  g_return_val_if_fail (photo, retval);
  g_return_val_if_fail (photo->type == E_CONTACT_PHOTO_TYPE_INLINED, retval);
  g_return_val_if_fail (icon_width_max > 0, retval);
  g_return_val_if_fail (icon_height_max > 0, retval);

  loader = gdk_pixbuf_loader_new ();
  if (loader)
    {
      gboolean write_retval = FALSE;

      write_retval = gdk_pixbuf_loader_write (loader, photo->data.inlined.data,
                                              photo->data.inlined.length, NULL);
      if (write_retval)
        {
          /* XXX: we may need to handle the remainder within a callback to the
           * loader's "area-prepared" signal to avoid a race condition */
          pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
          if (pixbuf)
            {
              gboolean loader_close_retval = FALSE;

              g_object_ref (G_OBJECT (pixbuf));

              loader_close_retval = gdk_pixbuf_loader_close (loader, NULL);
              if (loader_close_retval)
                {
                  width = gdk_pixbuf_get_width (pixbuf);
                  height = gdk_pixbuf_get_height (pixbuf);

                  if (width > icon_width_max)
                    {
                      height = (height * icon_width_max) / width;
                      width = icon_width_max;
                    }
                  if (height > icon_height_max)
                    {
                      width = (width * icon_height_max) / height;
                      height = icon_height_max;
                    }

                  retval = gdk_pixbuf_scale_simple (pixbuf, width, height,
                                                    GDK_INTERP_BILINEAR);
                  g_object_ref (G_OBJECT (retval));
                }
              else
                {
                  g_warning ("closed the GdkPixbufLoader before it finished "
                             "writing data to the GdkPixbuf");
                }

              g_object_unref (pixbuf);
            }
          else
            {
              g_warning ("failed to get the GdkPixbuf from the "
                         "GdkPixbufLoader");
            }
        }
      else
        {
          g_warning ("failed to write image data to the GdkPixbufLoader");
        }

      g_object_unref (G_OBJECT (loader));
    }
  else
    {
      g_warning ("failed to create a new GdkPixbufLoader");
    }

  return retval;
}

/* Get a "pretty" version of an EContact's name */
const gchar*
display_name_from_e_contact (EContact *e_contact)
{
  const gchar *retval = NULL;

  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);

  retval = e_contact_get_const (e_contact, E_CONTACT_FULL_NAME);
  if (retval)
    {
      if (g_utf8_strlen (retval, -1) > 0)
        {
          /* success: got display name from Full Name */
        }
      else
        {
          retval = e_contact_get_const (e_contact, E_CONTACT_NICKNAME);
          if (retval)
            {
              if (g_utf8_strlen (retval, -1) > 0)
                {
                  /* success: got display name from Nickname */
                }
            }
          else
            {
              retval = "Unnamed";
            }
        }
    }

  return retval;
}

/* Set the value of an EContact's attribute with the given ID and context
 * ("type" in e-d-s terminology) */
gboolean
e_vcard_attr_list_set_value (EContact *e_contact, EContactField field_id,
                             const gchar *type, guint abs_num,
                             const gchar *value)
{
  gboolean retval = FALSE;
  GList *l = NULL;
  GList *attr_node = NULL;
  GList *attr_list_head = NULL;
  EVCardAttribute *attr = NULL;
  GList *type_existing = NULL;

  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);
  g_return_val_if_fail (E_CONTACT_FIELD_IS_VALID (field_id), retval);
  g_return_val_if_fail (type, retval);
  g_return_val_if_fail (!g_str_equal (type, ""), retval);
  g_return_val_if_fail (abs_num > 0, retval);
  /* value may be NULL */

  attr_list_head = e_contact_get_attributes (e_contact, field_id);
  if (attr_list_head)
    {
      /* FIXME: there's got to be a cleaner way to do this */
      /* iterate to the given 'abs_num'th item with the given type in the list
       */
      for (l = attr_list_head;
           l && (abs_num > 0);
           l = g_list_next (l))
        {
          EVCardAttribute *attr_cur = NULL;

          attr_cur = l->data;
          type_existing = e_vcard_attribute_get_param (attr_cur, EVC_TYPE);
          /* FIXME: apparently an attribute could have multiple types; handle it
           * */
          if (type_existing && g_str_equal (type, type_existing->data))
            {
              abs_num--;
              attr_node = l;
              attr = attr_cur;
            }
        }

      /* XXX: the abs_num >= 0 check seems unnecessary */
      if (attr && abs_num >= 0)
        {
          if (value && !g_str_equal (value, ""))
            {
              e_vcard_attribute_remove_values (attr);
              e_vcard_attribute_add_value (attr, value);
            }
          else
            {
              attr_list_head = g_list_delete_link (attr_list_head,
                                                   attr_node);
            }

          e_contact_set_attributes (e_contact, field_id, attr_list_head);

          /* FIXME: supposedly we own this -- why does freeing it give us a
          * double-free segfault? 
          g_free (attr_list_head);
          */
          retval = TRUE;
        }
      else
        {
          g_warning ("invalid attribute (context, number); skipping changes");
        }
    }
  else
    {
      g_warning ("unable to get the list of attributes for EContact field ID "
                 "%d", field_id);
    }

  return retval;
}

/* Append a value to an EContact's specific attribute list */
gboolean
e_vcard_attr_list_prepend_value (EContact *e_contact, EContactField field_id,
                                 const gchar *type, const gchar *value)
{
  gboolean retval = FALSE;
  GList *attr_list = NULL;
  EVCardAttribute *attr = NULL;
  const gchar *attr_name = NULL;

  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);
  g_return_val_if_fail (E_CONTACT_FIELD_IS_VALID (field_id), retval);
  g_return_val_if_fail (type, retval);
  g_return_val_if_fail (!g_str_equal (type, ""), retval);
  g_return_val_if_fail (value, retval);
  g_return_val_if_fail (!g_str_equal (value, ""), retval);

  attr_list = e_contact_get_attributes (e_contact, field_id);

  attr_name = eds_im_field_id_to_vcard_attr (field_id);
  attr = e_vcard_attribute_new (NULL, attr_name);
  e_vcard_attribute_add_value (attr, value);
  attr_list = g_list_prepend (attr_list, attr);
  e_contact_set_attributes (e_contact, field_id, attr_list);

  /* FIXME: supposedly we own this -- why does freeing it give us a
   * double-free segfault? 
  g_free (attr_list_head);
  */

  retval = TRUE;

  return retval;
}

/* Set an individual field within an EContactAddress.
 * 
 * Return TRUE for success, FALSE for failure. */
gboolean
e_contact_address_set_field (EContactAddress *addr, const gchar *field_name,
                             const gchar *value)
{
  gboolean retval = FALSE;
  gchar *value_copy = NULL;

  g_return_val_if_fail (addr, retval);
  g_return_val_if_fail (field_name, retval);

  value_copy = g_strdup (value);

  /* inverting standard retval logic to simplify the remaining code */
  retval = TRUE;

  /* FIXME: should we free any existing non-NULL value first? */
  if (g_str_equal (field_name, "street"))
    {
      addr->street = value_copy;
    }
  else if (g_str_equal (field_name, "po"))
    {
      addr->po = value_copy;
    }
  else if (g_str_equal (field_name, "locality"))
    {
      addr->locality = value_copy;
    }
  else if (g_str_equal (field_name, "code"))
    {
      addr->code = value_copy;
    }
  else if (g_str_equal (field_name, "region"))
    {
      addr->region = value_copy;
    }
  else if (g_str_equal (field_name, "country"))
    {
      addr->country = value_copy;
    }
  else
    {
      g_warning ("unknown address field '%s'", field_name);

      retval = FALSE;
      g_free (value_copy);
    }

  return retval;
}

/* Return TRUE if an EContactAddress is completely empty, FALSE otherwise. */
gboolean 
e_contact_address_is_empty (EContactAddress *addr) 
{ 
  gboolean retval = TRUE;

  g_return_val_if_fail (addr, retval);

  if (TRUE
      && STRING_NULL_OR_EMPTY (addr->address_format) 
      && STRING_NULL_OR_EMPTY (addr->po) 
      && STRING_NULL_OR_EMPTY (addr->ext) 
      && STRING_NULL_OR_EMPTY (addr->street) 
      && STRING_NULL_OR_EMPTY (addr->locality) 
      && STRING_NULL_OR_EMPTY (addr->region) 
      && STRING_NULL_OR_EMPTY (addr->code) 
      && STRING_NULL_OR_EMPTY (addr->country)) 
    { 
      retval = TRUE; 
    } 
  else
    {
      retval = FALSE;
    }
 
  return retval; 
}

/* FIXME: this is US-specific */
/* Generate a string from a given EContactAddress */
gchar*
e_contact_address_to_label (EContactAddress *addr)
{
  gchar *retval = NULL;
  GString *label = NULL;
  
  g_return_val_if_fail (addr, retval);
  /* An empty address should get an empty label */
  g_return_val_if_fail (!e_contact_address_is_empty (addr), retval);

  label = g_string_new (addr->street);
  
  if (!g_str_equal (addr->po, ""))
    {
      g_string_append_printf (label, "\n%s", addr->po);
    } 
    
  g_string_append_printf (label, "\n%s, %s %s\n%s", addr->locality,
                          addr->region, addr->code, addr->country);

  retval = g_string_free (label, FALSE);
                          
  return retval;
}

/* FIXME: doesn't handle titles properly */
/* Return a copy of the X-EVOLUTION-FILE-AS-formatted version of a person's full
 * name (must be freed by caller) if successful; otherwise, NULL */
gchar*
eds_get_name_file_as_from_full (const gchar *full_name)
{
  gchar *retval = NULL;
  GString *name_file_as = NULL;
  gchar **name_split = NULL;
  guint names_num = 0;

  g_return_val_if_fail (full_name, retval);
  g_return_val_if_fail (!g_str_equal (full_name, ""), retval);

  /* Construct the X-EVOLUTION-FILE-AS name in the format:
   *    "[<family_name>, ][<given_name>][ <middle name>][ <suffixes>]" */
  name_file_as = g_string_new ("");
  name_split = g_strsplit (full_name, " ", -1);
  if (name_split)
    {
      for (names_num = 0; name_split && name_split[names_num]; names_num++)
        {
        }

      if (names_num == 0)
        {
          /* do nothing; we'll use "" */
        }
      else if (names_num == 1)
        {
          g_string_append (name_file_as, name_split[0]);
        }
      else if (names_num == 2)
        {
          g_string_append (name_file_as, name_split[1]);
          g_string_append (name_file_as, ", ");
          g_string_append (name_file_as, name_split[0]);
        }
      else if (names_num >= 3)
        {
          g_string_append (name_file_as, name_split[2]);
          g_string_append (name_file_as, ", ");
          g_string_append (name_file_as, name_split[0]);

          g_string_append_c (name_file_as, ' ');
          g_string_append (name_file_as, name_split[1]);

          if (names_num >= 4)
            {
              /* FIXME: handle titles */
            }
        }

      g_strfreev (name_split);
    }

  retval = g_string_free (name_file_as, FALSE);

  return retval;
}

/* Return a vCard attribute name for the given EContactField ID. This string
 * must not be freed or modified.
 * Eg, E_CONTACT_IM_AIM -> "X-AIM" */
const gchar*
eds_im_field_id_to_vcard_attr (EContactField field_id)
{
  gchar *retval = NULL;
  const gchar *field_name = NULL;
  const gchar *proto_name = NULL;

  g_return_val_if_fail (E_CONTACT_FIELD_IS_VALID (field_id), retval);
  /* XXX: kind of ugly; breaks if this ordering changes and prevents us from
   * supporting Gadu-Gadu */
  g_return_val_if_fail (field_id >= E_CONTACT_IM_AIM, retval);
  g_return_val_if_fail (field_id <= E_CONTACT_IM_ICQ, retval);

  field_name = e_contact_field_name (field_id);

  /* format of the field name is "im_<protocol in lower case>" */
  proto_name = field_name + strlen("im_");

  if (!g_ascii_strcasecmp (proto_name, "AIM"))
    {
      retval = EVC_X_AIM;
    }
  else if (!g_ascii_strcasecmp (proto_name, "GROUPWISE"))
    {
      retval = EVC_X_GROUPWISE;
    }
  else if (!g_ascii_strcasecmp (proto_name, "JABBER"))
    {
      retval = EVC_X_JABBER;
    }
  else if (!g_ascii_strcasecmp (proto_name, "YAHOO"))
    {
      retval = EVC_X_YAHOO;
    }
  else if (!g_ascii_strcasecmp (proto_name, "MSN"))
    {
      retval = EVC_X_MSN;
    }
  else if (!g_ascii_strcasecmp (proto_name, "ICQ"))
    {
      retval = EVC_X_ICQ;
    }
  else if (!g_ascii_strcasecmp (proto_name, "GADUGADU"))
    {
      retval = EVC_X_GADUGADU;
    }

  return retval;
}
