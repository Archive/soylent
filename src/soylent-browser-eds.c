/*
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *  Author: Rob Taylor <codethink.co.uk>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gdk-pixbuf-loader.h>
#include <libebook/e-book.h>

#include "eds-utils.h"
#include "soylent-browser.h"
#include "soylent-browser-eds.h"
#include "soylent-person.h"
#include "soylent.h"

/* Callbacks */
static gboolean soylent_browser_eds_update_groups (SoylentBrowser *browser,
                                                   EContact *e_contact);
static gboolean soylent_browser_eds_contacts_added_cb (EBookView *book_view, 
                                                       const GList *e_contacts,
                                                       SoylentBrowser *browser);
static gboolean soylent_browser_eds_contacts_changed_cb
                                                 (EBookView *book_view, 
                                                  const GList *e_contacts,
                                                  SoylentBrowser *browser);
static gboolean soylent_browser_eds_contacts_removed_cb
                                                    (EBookView *book_view, 
                                                     const GList *e_ids,
                                                     SoylentBrowser *browser);
static gboolean soylent_browser_eds_contacts_sequence_complete_cb 
                                                    (EBookView *book_view,
                                                     const GList *e_ids,
                                                     SoylentBrowser *browser);
static void soylent_browser_eds_query_start (EBook *book, EBookStatus status,
                                             EBookView *book_view,
                                             gpointer closure);
static void soylent_browser_eds_book_opened (EBook *book, EBookStatus status,
                                             gpointer closure);
static gboolean soylent_browser_eds_book_open (gpointer data);

/* Check for groups on the given EContact, and union them with the existing
 * browser-visible group list.
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_eds_update_groups (SoylentBrowser *browser, EContact *e_contact)
{
  gboolean retval = FALSE;
  GList *e_contact_groups = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */
  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);

  /* inverting standard reval logic so we can easily set it FALSE for one or
   * more errors */
  retval = TRUE;

  e_contact_groups = e_contact_get (e_contact, E_CONTACT_CATEGORY_LIST);
  if (e_contact_groups)
    {
      GList *group = NULL;

      for (group = e_contact_groups; group; group = g_list_next (group))
        {
          gboolean add_group_retval = FALSE;

          add_group_retval = soylent_browser_check_add_group (browser,
                                                              group->data);

          if (add_group_retval == FALSE)
            {
              retval = FALSE;
            }
        }

      g_list_free (e_contact_groups);
    }

  return retval;
}

/* XXX: does this get called from main thread? how crazy is eds?  it looks
 * like this will always come from the mainloop thread */
/* Handle 'contacts-added' signal from e-d-s; add contacts to the
 * SoylentBrowser's internal lookup table. Note this is also how the original
 * contacts get added at startup (though all at once).
 *
 * Return TRUE for success. */
static gboolean
soylent_browser_eds_contacts_added_cb (EBookView *book_view,
                                       const GList *e_contacts,
                                       SoylentBrowser *browser)
{
  /* FIXME FIXME: we need to block the empathy_contacts callbacks for the body
   * of these EContact callbacks */

  gboolean retval = FALSE;
  GList *c = NULL;
  GHashTable *people = NULL;

  g_return_val_if_fail (e_contacts, retval);
  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment this once SoylentBrowser is a real GObject
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
  */

  /* Defaulting to TRUE so we can set it FALSE for one or more failures (we
   * could have mixed success and failure in this function) */
  retval = TRUE;

  people = soylent_browser_get_people (browser);
  if (people)
    {
      /* Iterate over new contacts and add them to the list */
      for (c = (GList *) e_contacts; c; c = g_list_next (c))
        {
          const gchar *name = NULL;
          const gchar *e_uid = NULL;
          EContact *e_contact = NULL;
          SoylentPerson *value = NULL;

          e_contact = E_CONTACT (c->data);
          if (E_IS_CONTACT (e_contact))
            {
              e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);

              /* If we don't already have an entry for this EContact, add one */
              if (!(g_hash_table_lookup (people, e_uid)))
                {
                  name = display_name_from_e_contact (e_contact);

                  /* Add contact to list */
                  value = soylent_person_new (browser, e_contact);
                  if (value)
                    {
                      gboolean update_icon_retval = FALSE;

                      g_hash_table_insert (people, (gchar*) e_uid, value);
                      update_icon_retval = soylent_browser_update_person_icon
                                                                      (browser,
                                                                       value);
                      if (update_icon_retval)
                        {
                          soylent_browser_eds_update_groups (browser,
                                                             e_contact);
                        }
                      else
                        {
                          g_warning ("failed to update the avatar for a "
                                     "person");
                          retval = FALSE;
                        }
                    }
                  else
                    {
                      g_warning ("failed to create a new SoylentPerson for the "
                                 "new EContact added to our addressbook");
                      retval = FALSE;
                    }
                }
              else
                {
                  /* EContact was manually added before we handled this signal
                   */
                }
            }
          else
            {
              g_warning ("list of new EContacts added contains a non-EContact");
              retval = FALSE;
            }
        }
    }
  else
    {
      g_critical ("browser's people hash is NULL");
      retval = FALSE;
    }

  return retval;
}

/* Handle 'contacts-changed' signal from e-d-s; update contacts in the
 * SoylentBrowser's internal lookup table.
 *
 * Return TRUE for success. */
static gboolean
soylent_browser_eds_contacts_changed_cb (EBookView *book_view,
                                         const GList *e_contacts,
                                         SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GList *c = NULL;
  GHashTable *people = NULL;

  g_return_val_if_fail (e_contacts, retval);
  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment this once SoylentBrowser is a real GObject
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
  */

  /* Defaulting to TRUE so we can set it FALSE for one or more failures (we
   * could have mixed success and failure in this function) */
  retval = TRUE;

  people = soylent_browser_get_people (browser);
  if (people)
    {
      /* Loop through changed contacts */
      for (c = (GList*) e_contacts; c && people; c = g_list_next (c))
        {
          EContact *e_contact = NULL;
          SoylentPerson *value = NULL;
          const gchar *e_uid = NULL;

          e_contact = E_CONTACT (c->data);
          /* Lookup if contact exists in internal list (it should) */
          e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
          value = g_hash_table_lookup (people, e_uid);
          if (value)
            {
              gboolean set_e_contact_retval = FALSE;

              /* Replace contact */
              set_e_contact_retval = soylent_person_set_e_contact (value,
                                                                   e_contact);
              if (set_e_contact_retval)
                {
                  gboolean update_icon_retval = FALSE;

                  g_hash_table_steal (people, e_uid);
                  g_hash_table_insert (people, (gchar*) e_uid, value);

                  /* Update list with possibly new name, photo */
                  update_icon_retval = soylent_browser_update_person_icon
                                                                      (browser,
                                                                       value);
                  if (update_icon_retval)
                    {
                      soylent_browser_eds_update_groups (browser, e_contact);
                    }
                  else
                    {
                      g_warning ("failed to update the avatar for a person");
                      retval = FALSE;
                    }
                }
              else
                {
                  g_warning ("failed to set the EContact for a person");
                  retval = FALSE;
                }
            }
          else
            {
              g_warning ("would update a person's EContact, but the person "
                         "is not in our people hash (this should not happen)");
              retval = FALSE;
            }
        }
    }
  else
    {
      g_critical ("browser's people hash is NULL");
      retval = FALSE;
    }

  return retval;
}

/* TODO: Remove groups that no longer contain contacts */
/* Handle 'contacts-removed' signal from e-d-s; remove contacts in the
 * SoylentBrowser's internal lookup table and perform any clean-up.
 *
 * Return TRUE for success. */
static gboolean
soylent_browser_eds_contacts_removed_cb (EBookView *book_view,
                                         const GList *e_uids,
                                         SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GList *i = NULL;
  GHashTable *people = NULL;

  g_return_val_if_fail (e_uids, retval);
  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment this once SoylentBrowser is a real GObject
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
  */

  people = soylent_browser_get_people (browser);
  if (people)
    {
      for (i = (GList*) e_uids; i && people; i = g_list_next (i))
        {
          const gchar *e_uid = (const gchar*) i->data;
          g_hash_table_remove (people, e_uid);
        }

      retval = TRUE;
    }
  else
    {
      g_critical ("browser's people hash is NULL");
    }

  return retval;
}

/* Handle 'contacts-sequence-complete' signal from e-d-s; after initial batch of
 * EContacts are added to SoylentBrowser's internal lookup table, hook up any of
 * those EContacts' metadata to any other lookup tables we care about.
 *
 * Return TRUE for success. */
static gboolean
soylent_browser_eds_contacts_sequence_complete_cb (EBookView *book_view,
                                                   const GList *e_uids,
                                                   SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  gboolean live_setup_retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment this once SoylentBrowser is a real GObject
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
  */

  live_setup_retval = soylent_browser_live_setup_finish (browser);
  if (!live_setup_retval)
    {
      /* non-fatal */
      g_warning ("failed to set up instant messaging integration");
    }

  retval = TRUE;

  return retval;
}

/* Set up the main EBookView's signal handlers and initiate it */
static void
soylent_browser_eds_query_start (EBook *book, EBookStatus status,
                                 EBookView *book_view, gpointer closure)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;

  g_return_if_fail (closure);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_if_fail (SOYLENT_IS_BROWSER (closure));
   */

  browser = closure;

  if ((status == E_BOOK_ERROR_OK) && book_view && E_IS_BOOK_VIEW (book_view))
    {
      gboolean set_book_view_retval = FALSE;

      g_object_ref (book_view);

      /* Connect signals on EBookView */
      g_signal_connect (G_OBJECT (book_view), "contacts_added",
                        G_CALLBACK (soylent_browser_eds_contacts_added_cb),
                        browser);
      g_signal_connect (G_OBJECT (book_view), "contacts_changed",
                        G_CALLBACK (soylent_browser_eds_contacts_changed_cb),
                        browser);
      g_signal_connect (G_OBJECT (book_view), "contacts_removed",
                        G_CALLBACK (soylent_browser_eds_contacts_removed_cb),
                        browser);
      g_signal_connect (G_OBJECT (book_view), "sequence_complete",
                        G_CALLBACK
                            (soylent_browser_eds_contacts_sequence_complete_cb),
                        browser);

      set_book_view_retval = soylent_browser_set_e_book_view (browser,
                                                              book_view);
      if (set_book_view_retval)
        {
          /* void function, for some bizarre reason */
          e_book_view_start (book_view);
          
          retval = TRUE;
        }
    }
  else
    {
      g_critical ("Got error %d when getting book view", status);
    }

  /* XXX: don't actually return, since e_book_async_get_book_view() won't accept
   * a non-void callback...
  return retval;
  */
}

/* Handler for the just-opened main EBook; initiates the main EBookView setup */
static void
soylent_browser_eds_book_opened (EBook *book, EBookStatus status,
                                 gpointer closure)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  EBookQuery *query = NULL;

  g_return_if_fail (closure);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_if_fail (SOYLENT_IS_BROWSER (closure));
   */

  browser = closure;

  if ((status == E_BOOK_ERROR_OK) && book && E_IS_BOOK (book))
    {
      gboolean e_book_set_retval = FALSE;

      e_book_set_retval = soylent_browser_set_e_book (browser, book);
      if (e_book_set_retval)
        {
          EBook *e_book = NULL;

          e_book = soylent_browser_get_e_book (browser);
          if (e_book && E_IS_BOOK (e_book))
            {
              query = e_book_query_any_field_contains ("");
              if (query)
                {
                  gboolean book_view_retval = FALSE;

                  book_view_retval = e_book_async_get_book_view
                                              (e_book, query, NULL, -1,
                                               soylent_browser_eds_query_start,
                                               browser);
                  if (book_view_retval)
                    {
                      retval = TRUE;
                    }

                  e_book_query_unref (query);
                }
            }
          else
            {
              g_critical ("browser has invalid EBook");
            }
        }
      else
        {
          g_critical ("failed to set browser's EBookView");
        }
    }
  else
    {
      g_critical ("Got error %d when opening book", status);
    }

  /* XXX: don't actually return, since e_book_async_open() won't accept
   * a non-void callback...
  return retval;
  */
}

/* Set up EBook post-opening handler.
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_eds_book_open (gpointer data)
{
  SoylentBrowser *browser = NULL;
  EBook *e_book = NULL;

  /* FIXME: once SoylentBrowser is a GObject, also check SOYLENT_IS_BROWSER */
  if (data)
    {
      browser = data;

      e_book = soylent_browser_get_e_book (browser);
      if (e_book && E_IS_BOOK (e_book))
        {
          gboolean open_retval = FALSE;

          open_retval = e_book_async_open (e_book, FALSE,
                                           soylent_browser_eds_book_opened,
                                           browser);
          if (open_retval == FALSE)
            {
              /* all is well; nothing to do */
            }
          else
            {
              g_critical ("failed to open browser's EBook");
            }
        }
      else
        {
          g_critical ("browser's EBook is invalid");
        }
    }
  else
    {
      g_critical ("browser is invalid");
    }

  /* Return FALSE to only be called once via the g_idle_add() */
  return FALSE;
}

/* Instantiate an EBook addressbook and create the SoylentBrowser's contact
 * lookup table
 *
 * Return TRUE for complete success, FALSE otherwise. */
gboolean
soylent_browser_eds_ebook_setup (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GError *error = NULL;
  EBook *e_book = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment this once SoylentBrowser is a real GObject
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
  */

  e_book = e_book_new_system_addressbook (&error);
  if (e_book && E_IS_BOOK (e_book))
    {
      gboolean book_set_retval = FALSE;

      book_set_retval = soylent_browser_set_e_book (browser, e_book);
      if (book_set_retval)
        {
          GHashTable *people_table = NULL;

          people_table = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                                (GDestroyNotify)
                                                          soylent_person_free);
          if (people_table)
            {
              gboolean table_set_retval = FALSE;

              table_set_retval = soylent_browser_set_people (browser,
                                                             people_table);
              if (table_set_retval)
                {
                  g_idle_add (soylent_browser_eds_book_open, browser);

                  retval = TRUE;
                }
              else
                {
                  g_critical ("failed to set browser's people lookup table");
                }
            }
          else
            {
              g_critical ("failed to create people lookup table");
            }
        }
      else
        {
          g_critical ("failed to set browser's EBook");
        }
    }
  else
    {
      g_critical ("Could not load system addressbook: %s", error->message);
      g_clear_error (&error);
    }

  return retval;
}
