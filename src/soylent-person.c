/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include "eds-utils.h"
#include "soylent-person.h"
#include "soylent-utils.h"

/* TODO: We'll probably eventually want a full-fledged GObject with signals and
 * a personal line of credit. In good time, baby bear. None of this is public
 * API, so it won't matter if we change it later. */
/* Note this includes representation (icon/text label) - stripping it out into a
 * parent struct seemed like an unnecessary complication; if we decide to do
 * things differently in an EmpathyPerson, then we'll deal with the differences
 * then */
struct SoylentPerson_tag
{
  EContact *e_contact;
  /* A list of EmpathyContacts (EmpathyContacts) */
  /* XXX: We'll need to be careful to keep this sync'd. Also, we'll be doing a
   * lot of linear searches through this - but it really shouldn't be that long
   */
  GList *empathy_contacts;
  SoylentPresence presence;

  GtkTreeIter *iter;
  SoylentBrowser *browser;
};

SoylentPerson*
soylent_person_new (SoylentBrowser *browser, EContact *e_contact)
{
  SoylentPerson *retval = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   * */
  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);

  retval = g_new (SoylentPerson, 1);

  retval->iter = g_new0 (GtkTreeIter, 1);
  retval->iter->stamp = SP_ITER_UNSET;
  retval->e_contact = g_object_ref (e_contact);
  retval->presence = SP_PRESENCE_UNSET;
  retval->empathy_contacts = NULL;
  retval->browser = browser;

  return retval;
}

EContact*
soylent_person_get_e_contact (SoylentPerson *person)
{
  g_return_val_if_fail (person, NULL);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */

  return person->e_contact;
}

GList*
soylent_person_get_empathy_contacts (SoylentPerson *person)
{
  g_return_val_if_fail (person, NULL);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */

  return person->empathy_contacts;
}

SoylentPresence
soylent_person_get_presence (SoylentPerson *person)
{
  g_return_val_if_fail (person, SP_PRESENCE_UNSET);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */

  return person->presence;
}

GtkTreeIter*
soylent_person_get_icon_view_iter (SoylentPerson *person)
{
  g_return_val_if_fail (person, NULL);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */

  return person->iter;
}

SoylentBrowser*
soylent_person_get_browser (SoylentPerson *person)
{
  g_return_val_if_fail (person, NULL);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */

  return person->browser;
}

/* FIXME: this should just return person->avatar; this functionality should
 * mostly be pushed into a _set_avatar() function, which is also called in
 * soylent_person_new() */
/* Return the person's Avatar (will use default Avatar if none exists).
 *
 * This memory will be freed upon Person destruction (Do not free it manually!)
 * */
GdkPixbuf*
soylent_person_get_avatar (SoylentPerson *person, guint icon_width_max,
                           guint icon_height_max)
{
  GdkPixbuf *retval = NULL;
  EContact *e_contact = NULL;
  
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   */

  e_contact = soylent_person_get_e_contact (person);
  if (e_contact && E_IS_CONTACT (e_contact))
    {
      EContactPhoto *photo = NULL;
      /*
      gboolean avatar_pixbuf_needs_unref = FALSE;
      */

      photo = e_contact_get (e_contact, E_CONTACT_PHOTO);
      if (photo && photo->type == E_CONTACT_PHOTO_TYPE_INLINED)
        {
          retval = gdk_pixbuf_from_inline_photo (photo,
                                                 icon_width_max,
                                                 icon_height_max);
        }
      else
        {
          GList *empathy_contacts = NULL;

          empathy_contacts = soylent_person_get_empathy_contacts (person);
          if (empathy_contacts)
            {
              GList *c = NULL;
              EmpathyContact *empathy_contact = NULL;

              for (c = empathy_contacts;
                   c && c->data && EMPATHY_IS_CONTACT (c->data) && !retval;
                   c = g_list_next (c))
                {
                  empathy_contact = EMPATHY_CONTACT (c->data);
                  retval = empathy_pixbuf_avatar_from_contact_scaled
                                                              (empathy_contact,
                                                               icon_width_max,
                                                               icon_height_max);
                }

            }
        }

      /* FIXME: do something with this logic elsewhere */
      /*
      if (retval)
        {
          avatar_pixbuf_needs_unref = TRUE;
        }
      */
    }
  else
    {
      g_warning ("person's EContact is invalid");
    }

  return retval;
}

/* Return a "pretty" version of a SoylentPerson's name, including a symbol to
 * indicate presence */
gchar*
soylent_person_get_display_name_with_presence (SoylentPerson *person)
{
  gchar *retval = NULL;
  EContact *e_contact = NULL;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   */

  e_contact = soylent_person_get_e_contact (person);
  if (e_contact && E_IS_CONTACT (e_contact))
    {
      const gchar *e_uid = NULL;
      const gchar *name_display_base = NULL;
      SoylentPresence presence = SP_PRESENCE_UNSET;

      e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
      name_display_base = display_name_from_e_contact (e_contact);

      presence = soylent_person_get_presence (person);
      switch (presence)
        {
          case SP_PRESENCE_AVAILABLE:
          /* TODO: indicate idleness eventually */
          case SP_PRESENCE_IDLE:
            retval = g_strdup_printf ("%s [*]", name_display_base);
            break;
          case SP_PRESENCE_BUSY:
          /* TODO: indicate idleness eventually */
          case SP_PRESENCE_IDLE_BUSY:
            retval = g_strdup_printf ("%s [...]", name_display_base);
            break;
          case SP_PRESENCE_OFFLINE:
            retval = g_strdup (name_display_base);
            break;
          default:
            retval = g_strdup (name_display_base);
            break;
        }
    }
  else
    {
      g_warning ("person's EContact is invalid");
    }

  return retval;
}

/* Returns TRUE upon success, FALSE otherwise */
gboolean
soylent_person_set_e_contact (SoylentPerson *person, EContact *e_contact)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), NULL);
   * */
  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);

  if (person->e_contact)
    {
      g_object_unref (person->e_contact);
    }
  
  person->e_contact = g_object_ref (e_contact);
  if (person->e_contact == e_contact)
    {
      retval = TRUE;
    }

  return retval;
}

/* Returns TRUE upon success, FALSE otherwise */
gboolean
soylent_person_set_empathy_contacts (SoylentPerson *person,
                                     GList *empathy_contacts)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   * */
  g_return_val_if_fail (empathy_contacts, retval);

  person->empathy_contacts = empathy_contacts;

  /* XXX: check that each entry in the GList is an EmpathyContact? */
  retval = TRUE;

  return retval;
}

/* Returns TRUE upon success, FALSE otherwise */
gboolean
soylent_person_add_empathy_contact (SoylentPerson *person,
                                    EmpathyContact *empathy_contact)
{
  gboolean retval = FALSE;
  GList *empathy_contact_existing = NULL;
  gboolean update_presence_retval = FALSE;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   * */
  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);

  /* Only add the contact if they aren't already among our live contacts */
  empathy_contact_existing = g_list_find (person->empathy_contacts,
                                          empathy_contact);
  if (!empathy_contact_existing)
    {
      person->empathy_contacts = g_list_prepend (person->empathy_contacts,
                                                 empathy_contact);
    }

  update_presence_retval = soylent_person_live_presence_updated_cb
                                                              (empathy_contact,
                                                               NULL, person);
  if (update_presence_retval)
    {
      retval = TRUE;
    }
  else
    {
      g_warning ("failed to update the online presence of a person");
    }

  return retval;
}

/* Returns TRUE upon success, FALSE otherwise */
gboolean
soylent_person_remove_empathy_contact (SoylentPerson *person,
                                       EmpathyContact *empathy_contact)
{
  gboolean retval = FALSE;
  gboolean update_presence_retval = FALSE;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   * */
  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);

  person->empathy_contacts = g_list_remove_all (person->empathy_contacts,
                                                empathy_contact);

  update_presence_retval = soylent_person_live_presence_updated_cb
                                                              (empathy_contact,
                                                               NULL, person);
  if (update_presence_retval)
    {
      retval = TRUE;
    }
  else
    {
      g_warning ("failed to update the online presence of a person");
    }

  return retval;
}

/* Returns:
 * < 0      if presence1 is less available than presence2
 * = 0      if presence1 is equally available as presence2
 * > 0      if presence1 is more available than presence2
 */
gint
soylent_person_live_presence_compare (SoylentPresence presence1,
                                      SoylentPresence presence2)
{
  if (!SP_PRESENCE_IS_VALID (presence1))
    {
      g_warning ("presence1 is invalid");
    }
  if (!SP_PRESENCE_IS_VALID (presence2))
    {
      g_warning ("presence2 is invalid");
    }

  return (presence1 - presence2);
}

/* Update a SoylentPerson's IM contacts' groups, based on their changes on the
 * network */
void
soylent_person_live_groups_updated_cb (EmpathyContact *empathy_contact,
                                       GParamSpec *param, SoylentPerson *person)
{
  soylent_debug ("function not implemented: "
                 "soylent_person_live_groups_updated_cb");
}

/* Transform an EmpathyPresence into our internal representation */
SoylentPresence
soylent_person_presence_from_mc_presence (McPresence presence)
{
  switch (presence)
    {
      case MC_PRESENCE_AVAILABLE:
        return SP_PRESENCE_AVAILABLE;
        break;
      case MC_PRESENCE_AWAY:
      case MC_PRESENCE_EXTENDED_AWAY:
      case MC_PRESENCE_DO_NOT_DISTURB:
        return SP_PRESENCE_BUSY;
        break;
      case MC_PRESENCE_OFFLINE:
      case MC_PRESENCE_HIDDEN:
      case MC_PRESENCE_UNSET:
        return SP_PRESENCE_OFFLINE;
        break;
      default:
        g_warning ("unknown Mission Control presence: %u", presence);
        break;
    }

  return SP_PRESENCE_UNSET;
}

/* Propagate IM contacts' presence changes to our internal state, updating
 * the SoylentPerson's representation and other widgets accordingly.
 *
 * Return TRUE for success, FALSE for any failure
 */
gboolean
soylent_person_live_presence_updated_cb (EmpathyContact *empathy_contact,
                                         GParamSpec *param,
                                         SoylentPerson *person)
{
  gboolean retval = FALSE;
  SoylentPresence soylent_person_presence_effective = SP_PRESENCE_OFFLINE;
  GList *l = NULL;

  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   */

  for (l = person->empathy_contacts;
       l && l->data && EMPATHY_IS_CONTACT (l->data);
       l = g_list_next (l))
    {
      EmpathyContact  *empathy_contact_cur = NULL;
      McPresence mc_presence_cur = MC_PRESENCE_UNSET;
      SoylentPresence soylent_presence_cur = SP_PRESENCE_UNSET;
      
      empathy_contact_cur = EMPATHY_CONTACT (l->data);
      mc_presence_cur = empathy_contact_get_presence (empathy_contact_cur);
      soylent_presence_cur = soylent_person_presence_from_mc_presence
                                                              (mc_presence_cur);
      
      if (SP_PRESENCE_IS_VALID (soylent_presence_cur))
        {
          if ((soylent_person_live_presence_compare
                                            (soylent_presence_cur,
                                             soylent_person_presence_effective))
              > 0)
            {
              soylent_person_presence_effective = soylent_presence_cur;
            }
        }
    }

  person->presence = soylent_person_presence_effective;

  retval = soylent_browser_update_person_icon (person->browser, person);

  return retval;
}

/* Propagate IM contacts' IM name changes to our internal state (most likely
 * ignoring the changes) */
void
soylent_person_live_name_updated_cb (EmpathyContact *empathy_contact,
                                     GParamSpec *param, SoylentPerson *person)
{
  soylent_debug ("function not implemented: "
                 "soylent_person_live_name_updated_cb");
}

/* FIXME: what's up with all this commented code? */
/* Propagate IM contacts' avatar changes to our internal state (possibly
 * updating their presented and stored representations)
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_person_live_avatar_updated_cb (EmpathyContact *empathy_contact,
                                       GParamSpec *param, SoylentPerson *person)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;

  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   * */

  browser = person->browser;
  retval = soylent_browser_update_person_icon (browser, person);

  return retval;
}

/* ??? */
void
soylent_person_live_type_updated_cb (EmpathyContact *empathy_contact,
                                     GParamSpec *param, SoylentPerson *person)
{
  soylent_debug ("function not implemented: "
                 "soylent_person_live_type_updated_cb");
}

gboolean
soylent_person_is_online (SoylentPerson *person)
{
  gboolean retval = FALSE;

  if (person)
    {
      if ((soylent_person_live_presence_compare (person->presence,
                                                 SP_PRESENCE_OFFLINE)) > 0)
        {
          retval = TRUE;
        }
    }
  else
    {
      g_warning ("soylent_person_is_online: person == NULL");
    }

  return retval;
}

/* Initiate communication with the SoylentPerson, using the
 * generally-lowest-latency method possible
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_person_action_communicate (SoylentPerson *person)
{
  gboolean retval = FALSE;

  if (soylent_person_is_online (person))
    {
      retval = soylent_browser_person_action_communicate_chat (person->browser,
                                                               person);
    }
  else
    {
      retval = soylent_browser_person_action_communicate_email (person->browser,
                                                                person);
    }

  return retval;
}

void
soylent_person_free (SoylentPerson *value)
{
  if (value)
    {
      GladeXML *wtree = NULL;
      GList *l = NULL;
      GtkListStore *model = NULL;

      wtree = soylent_browser_get_widget_tree (value->browser);
      model = GTK_LIST_STORE (gtk_icon_view_get_model (GTK_ICON_VIEW
                                                       (glade_xml_get_widget
                                                        (wtree, "iv_people"))));
      gtk_list_store_remove (model, value->iter);
      g_object_unref (value->e_contact);

      for (l = value->empathy_contacts; l && l->data; l = g_list_next (l))
        {
          g_object_unref (l->data);
        }
      g_list_free (value->empathy_contacts);

      g_free (value->iter);
      g_free (value);
    }
}
