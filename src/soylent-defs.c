/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include "soylent-defs.h"
#include "soylent-utils.h"

const gchar *MAIL_WIDGET_TYPE_STRS[] = {"tv", "entry", "entry", "entry",
                                        "entry", "entry",};
/* Evo -> en_us: locality == city */
const gchar *MAIL_FIELD_STRS[] = {"street", "po", "locality", "code", "region",
                                   "country",};
/* FIXME: use this everywhere; remove the ones above; note the ordering change -
 * does that matter at all? */
const gchar *MAIL_WIDGET_TYPE_FULL_STRS[] = {"none", "entry", "none", "tv",
                                             "entry", "entry", "entry",
                                             "entry",};
const gchar *MAIL_FIELD_FULL_STRS[] = {"address_format", "po", "ext", "street",
                                       "locality", "region", "code",
                                       "country",};
