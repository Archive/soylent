/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_UTILS_H_
#define _SOYLENT_UTILS_H_

#include <glib.h>

#define G_SIGNAL_HANDLER_MIN_VALID 1

#if BUILD_DEV
#  define soylent_debug(format, a...) g_debug(format, ##a)
#else
#  define soylent_debug(format, ...)
#endif

#define STRING_NULL_OR_EMPTY(x) ((!(x)) || (g_str_equal ((x), "")))

#define SOYLENT_EMAIL_POS_FROM_E_CONTACT_FIELD (x) ((!(x)) || (g_str_equal ((x), "")))

/* XXX: always assign this to a variable and use that variable, to avoid
 * unexpected signed-to-unsigned promotion of negative numbers (eg, a signed int
 * of -1 gets promoted to 4294967295U on 32-bit arches). */
#define ARRAY_LEN(x) ((sizeof (x)) / sizeof (x[0]))

typedef enum cb_data_tag cb_data_t;
enum cb_data_tag
{
  FIRST_CB_DATA,
  CB_DATA_SIMPLE,
  CB_DATA_COMPLEX_FROM_E_CONTACT_FIELD,
  LAST_CB_DATA,
};

typedef struct widget_signal_args_tag widget_signal_args_t;
struct widget_signal_args_tag
{
  gchar *widget_name;
  gchar *signal_name;
  gpointer callback;
  cb_data_t cb_data_type;
  gpointer user_data;
};

gboolean widget_signal_connect (GladeXML *wtree, const gchar *widget_name,
                                const gchar *signal, gconstpointer callback,
                                gpointer user_data);
gboolean widget_signal_connect_mass 
                            (SoylentBrowser *browser,
                             const widget_signal_args_t const *signal_handlers,
                             guint signals_num);

void text_buffer_set_text_block_handler (GtkTextBuffer *buf, const gchar *text,
                                         gpointer handler);
void entry_set_text_block_handler (GtkEntry *entry, const gchar *text,
                                   gpointer handler);

gchar* name_get_given_from_full (const gchar *full_name);
gchar* name_get_family_from_full (const gchar *full_name);

#endif /* _SOYLENT_UTILS_H_ */
