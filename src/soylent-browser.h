/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_BROWSER_H_
#define _SOYLENT_BROWSER_H_

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnome/libgnome.h>

#include <libebook/e-book.h>

#include <libmissioncontrol/mc-account.h>
#include <libmissioncontrol/mc-account-monitor.h>
#include <libmissioncontrol/mission-control.h>

#include <libempathy/empathy-debug.h>
#include <libempathy/empathy-utils.h>
#include <libempathy/empathy-contact.h>
#include <libempathy/empathy-tp-chat.h>
#include <libempathy/empathy-idle.h>
#include <libempathy/empathy-contact-manager.h>
#include <libempathy/empathy-contact-list.h>
#include <libempathy-gtk/empathy-ui-utils.h>

#define SOYLENT_BROWSER_PEOPLE_VIEW_MODE_IS_VALID(x) \
          (1 \
           && ((x) > FIRST_SB_PEOPLE_VIEW_MODE) \
           && ((x) < LAST_SB_PEOPLE_VIEW_MODE))

typedef struct SoylentBrowser_tag SoylentBrowser;
typedef enum soylent_browser_people_view_mode_tag
             soylent_browser_people_view_mode;

enum soylent_browser_people_view_mode_tag
{
  /* intentionally invalid */
  FIRST_SB_PEOPLE_VIEW_MODE = 0,

  SB_PEOPLE_VIEW_MODE_BROWSE,
  SB_PEOPLE_VIEW_MODE_EDIT,

  /* intentionally invalid */
  LAST_SB_PEOPLE_VIEW_MODE,
};

#include "soylent-defs.h"
#include "soylent-browser-person-view.h"
#include "soylent-person.h"

/* FIXME: move these to soylent-defs.h */
/* ========================================= */
#define IM_FIELDS_PER_CONTEXT 8

#define SB_DEFAULT_NAME "Unnamed Person"
#define E_UID_UNMATCHED "e_uid_unmatched"
/* XXX: hacky. Is there a better way around this? */
#define SP_ITER_UNSET 3901

/* FIXME: turn these into enums */
#define CONTEXT_HOME  0
#define CONTEXT_WORK  1
#define CONTEXT_OTHER 2
#define CONTEXT_FIRST CONTEXT_HOME
#define CONTEXT_LAST  CONTEXT_OTHER
#define CONTEXT_IM_FIRST CONTEXT_HOME
#define CONTEXT_IM_LAST  CONTEXT_WORK
#define CONTEXT_IM_IS_VALID(x) (   ((x) >= CONTEXT_IM_FIRST)\
                                && ((x) <= CONTEXT_IM_LAST))
/* ========================================= */


extern const gchar *CONTEXT_STRS[];

#define MAIL_FIELDS_NUM 6
extern const gchar* MAIL_WIDGET_TYPE_STRS [MAIL_FIELDS_NUM];
extern const gchar* MAIL_FIELD_STRS       [MAIL_FIELDS_NUM];
#define MAIL_FIELDS_FULL_NUM 8
extern const gchar* MAIL_WIDGET_TYPE_FULL_STRS [MAIL_FIELDS_FULL_NUM];
extern const gchar* MAIL_FIELD_FULL_STRS       [MAIL_FIELDS_FULL_NUM];

EBook* soylent_browser_get_e_book (SoylentBrowser *browser);
EBookView* soylent_browser_get_e_book_view (SoylentBrowser *browser);
guint soylent_browser_get_icon_height_max (SoylentBrowser *browser);
guint soylent_browser_get_icon_width_max (SoylentBrowser *browser);
GHashTable* soylent_browser_get_people (SoylentBrowser *browser);
GladeXML* soylent_browser_get_widget_tree (SoylentBrowser *browser);
EmpathyContactManager* soylent_browser_get_live_manager
                                                      (SoylentBrowser *browser);

GtkWidget* soylent_browser_get_widget (SoylentBrowser *browser,
                                       const gchar *type_widget,
                                       const gchar *type_data,
                                       const gchar *context,
                                       const gchar *subcontext);

SoylentPerson* soylent_browser_get_person_from_e_uid (SoylentBrowser *browser,
                                                      const gchar *e_uid);
SoylentPerson* soylent_browser_get_person_from_e_contact
                                                      (SoylentBrowser *browser,
                                                       EContact *e_contact);
GList* soylent_browser_get_selected_people (SoylentBrowser *browser);
GList* soylent_browser_get_selected_people_e_contact (SoylentBrowser *browser);
GList* soylent_browser_get_selected_people_e_uid (SoylentBrowser *browser);
SoylentPerson* soylent_browser_get_selected_person (SoylentBrowser *browser);
EContact* soylent_browser_get_selected_person_e_contact
                                                      (SoylentBrowser *browser);
gchar* soylent_browser_get_selected_person_e_uid (SoylentBrowser *browser);

gboolean soylent_browser_set_people (SoylentBrowser *browser,
                                     GHashTable *people);
gboolean soylent_browser_set_e_book (SoylentBrowser *browser, EBook *e_book);
gboolean soylent_browser_set_e_book_view (SoylentBrowser *browser,
                                          EBookView *e_book_view);

gboolean soylent_browser_live_setup_finish (SoylentBrowser *browser);

SoylentBrowser* soylent_browser_new (void);
void soylent_browser_destroy (SoylentBrowser *browser);
gboolean soylent_browser_check_add_group (SoylentBrowser *browser,
                                          const gchar *group);
gboolean soylent_browser_view_set_mode (SoylentBrowser *browser,
                                        soylent_browser_people_view_mode mode);
gboolean soylent_browser_person_select (SoylentBrowser *browser,
                                        SoylentPerson *person,
                                        gboolean scroll);
gboolean soylent_browser_update_person_icon (SoylentBrowser *browser,
                                             SoylentPerson *person);

void soylent_browser_delete_cb (GtkWidget *widget, GdkEvent *event,
                                gpointer user_data);
gboolean soylent_browser_live_members_changed_cb
                                              (EmpathyContactList *list_iface,
                                               EmpathyContact *empathy_contact,
                                               EmpathyContact *actor,
                                               guint reason, gchar *message,
                                               gboolean is_member,
                                               SoylentBrowser *browser);
gboolean soylent_browser_presence_set_cb (GtkWidget *widget,
                                          gpointer user_data);
gboolean soylent_browser_dialog_about_show_cb (GtkWidget *widget,
                                               gpointer user_data);

#endif /* _SOYLENT_BROWSER_H_ */
