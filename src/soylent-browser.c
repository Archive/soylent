/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include <config.h>

#include <libempathy/empathy-idle.h>
#include <libempathy/empathy-status-presets.h>

#include "eds-utils.h"
#include "soylent-browser.h"
#include "soylent-browser-eds.h"
#include "soylent-browser-menu-bar.h"
#include "soylent-person.h"
#include "soylent-utils.h"


#define SOYLENT_GLADE_FILE_UNINSTALLED \
                                    SOYLENT_GLADEDIR_UNINSTALLED"/soylent.glade"
#define SOYLENT_GLADE_FILE_INSTALLED SOYLENT_GLADEDIR"/soylent.glade"

/* TODO: make this a full-fledged GObject */
struct SoylentBrowser_tag
{ 
  EBook *book;
  EBookView *book_view;
  GHashTable *people;
  GladeXML *main_window;
  soylent_browser_people_view_mode view_mode;
  EmpathyContactManager *live_manager;
  EmpathyIdle *live_idle;
  /* Any EmpathyContacts unattached to e-d-s entries */
  GList *empathy_contacts_pending;

  guint icon_width_max;
  guint icon_height_max;
  GdkPixbuf *icon_stock_person;
};

static int soylent_browser_compare_people_by_icon_iter (GtkTreeModel *model,
                                                        GtkTreeIter *a,
                                                        GtkTreeIter *b,
                                                        gpointer ptr);
static gboolean soylent_browser_person_icon_activated_cb (GtkIconView *iv,
                                                          GtkTreePath *path,
                                                          gpointer user_data);

static SoylentPerson* soylent_browser_empathy_contact_find_owner_person 
                                             (SoylentBrowser *browser,
                                              EmpathyContact *empathy_contact);
static gboolean soylent_browser_person_associate_empathy_contact
                                            (SoylentBrowser *browser,
                                             SoylentPerson *person,
                                             EmpathyContact *empathy_contact);
static gboolean soylent_browser_person_dissociate_empathy_contact
                                              (SoylentBrowser *browser,
                                               SoylentPerson *person,
                                               EmpathyContact *empathy_contact);
static gboolean soylent_browser_about_dialog_setup (SoylentBrowser *browser);
static gboolean soylent_browser_widget_setup (SoylentBrowser *browser);
static gboolean soylent_browser_widget_signals_setup (SoylentBrowser *browser);

/* Instantiate the main SoylentBrowser */
SoylentBrowser*
soylent_browser_new (void)
{
  SoylentBrowser *retval = NULL;
  SoylentBrowser *browser = NULL;
  soylent_browser_people_view_mode view_mode;

  browser = g_new0 (SoylentBrowser, 1);
  view_mode = SB_PEOPLE_VIEW_MODE_BROWSE;

  /* Try to load the local glade file first for development builds */
#ifdef BUILD_DEV
  browser->main_window = glade_xml_new (SOYLENT_GLADE_FILE_UNINSTALLED,
                                        NULL, NULL);
  if (!browser->main_window)
#endif /* BUILD_DEV */
      browser->main_window = glade_xml_new (SOYLENT_GLADE_FILE_INSTALLED,
                                            NULL, NULL);
  if (browser->main_window)
    {
      GtkIconTheme *gtk_icon_theme = NULL;
      browser->live_manager = NULL;
      browser->empathy_contacts_pending = NULL;
      browser->icon_width_max  = ICON_MAX_WIDTH;
      browser->icon_height_max = ICON_MAX_HEIGHT;

      gtk_icon_theme = gtk_icon_theme_get_default ();
      if (gtk_icon_theme)
        {
          /* 
            * FIXME: we need to listen for theme updates, and refresh this
            * when they happen. See the docs for this function. The updating
            * callback also needs to g_object_unref() the current pixbuf.
            *
            * Is there a better way to do this?
            */
          browser->icon_stock_person = gtk_icon_theme_load_icon
                                                    (gtk_icon_theme,
                                                    "stock_person",
                                                    browser->icon_width_max,
                                                    0, NULL);

          /* 
          * we need to free these eventually - just not during the main
          * lifetime of the app
          */
          gboolean set_mode_retval = FALSE;

          browser->live_idle = empathy_idle_new ();
          if (browser->live_idle)
            {
              /* XXX: some of these should be user-configurable */
              empathy_idle_set_auto_away (browser->live_idle, TRUE);
              empathy_idle_set_presence (browser->live_idle,
                                        MC_PRESENCE_AVAILABLE, "");
            }
          else
            {
              /* non-fatal */
              g_warning ("unable to initialize Empathy presence "
                          "handler");
            }

          set_mode_retval = soylent_browser_view_set_mode (browser,
                                                            view_mode);
          if (set_mode_retval)
            {
              gboolean signal_setup_retval = FALSE;

              signal_setup_retval = soylent_browser_widget_signals_setup
                                                                  (browser);
              if (signal_setup_retval)
                {
                  gboolean widget_setup_retval = FALSE;

                  widget_setup_retval = soylent_browser_widget_setup
                                                                  (browser);
                  if (widget_setup_retval)
                    {
                      gboolean ebook_setup_retval = FALSE;

                      ebook_setup_retval = soylent_browser_eds_ebook_setup
                                                                  (browser);
                      if (ebook_setup_retval)
                        {
                          /* success */
                          retval = browser;
                        }
                      else
                        {
                          g_critical ("failed to set up EBook (address "
                                      "book)");
                        }
                    }
                  else
                    {
                      g_critical ("failed to set up major widgets");
                    }
                }
              else
                {
                  g_critical ("failed to set up major GObject signals");
                }
            }
          else
            {
              g_critical ("failed to set the browser view mode");
            }
        }
      else
        {
          g_critical ("unable to get default GTK theme");
        }
    }
  else
    {
      g_critical ("unable to load glade file");
    }

  if (!retval)
    {
      soylent_browser_destroy (browser);
    }

  return retval;
}

/* Free the contents of the given SoylentBrowser */
void
soylent_browser_destroy (SoylentBrowser *browser)
{
  g_object_unref (browser->book_view);
  g_object_unref (browser->book);

  /* XXX - why do these cause (I suspect) double-free segfaults?
   *  - because they were never ref'd in the first place? */
  /*
  g_object_unref (browser->people);
  */
  /*
  FIXME: actually do this
  g_hash_table_foreach (browser->empathy_contacts_pending,
                        free_empathy_contacts_pending_key_value);
  g_hash_table_destroy (browser->empathy_contacts_pending);
  */
  /* XXX: is anything else missing here? */
  g_object_unref (browser->icon_stock_person);
  g_free (browser);
}

EBook*
soylent_browser_get_e_book (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, NULL);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */

  return browser->book;
}

EBookView*
soylent_browser_get_e_book_view (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, NULL);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */

  return browser->book_view;
}

guint
soylent_browser_get_icon_height_max (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, 0);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), 0);
   */

  return browser->icon_height_max;
}

guint
soylent_browser_get_icon_width_max (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, 0);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), 0);
   */

  return browser->icon_width_max;
}

EmpathyContactManager*
soylent_browser_get_live_manager (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, NULL);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */

  return browser->live_manager;
}

GHashTable*
soylent_browser_get_people (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, NULL);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */

  return browser->people;
}

GladeXML*
soylent_browser_get_widget_tree (SoylentBrowser *browser)
{
  g_return_val_if_fail (browser, NULL);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), NULL);
   */

  return browser->main_window;
}

/* Return a GtkWidget* for a widget based on its widget type abbreviation,
 * data type, context, and subcontext (optionally NULL).
 *
 * Eg, "entry", "email", "home", "1" */
GtkWidget*
soylent_browser_get_widget (SoylentBrowser *browser, const gchar *type_widget,
                            const gchar *type_data, const gchar *context,
                            const gchar *subcontext)
{
  GtkWidget *retval = NULL;
  GladeXML *wtree = NULL;
  gchar *widget_name = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (type_widget, retval);
  g_return_val_if_fail (type_data, retval);
  g_return_val_if_fail (context, retval);

  wtree = browser->main_window;

  if (subcontext)
    {
      widget_name = g_strdup_printf ("%s_person_%s_%s_%s", type_widget,
                                     type_data, context, subcontext);
    }
  else
    {
      widget_name = g_strdup_printf ("%s_person_%s_%s", type_widget,
                                     type_data, context);
    }

  retval = glade_xml_get_widget (wtree, widget_name);

  g_free (widget_name);

  return retval;
}

/* Look up the SoylentPerson for the given EContact ID */
SoylentPerson*
soylent_browser_get_person_from_e_uid (SoylentBrowser *browser,
                                       const gchar *e_uid)
{
  SoylentPerson *retval = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (e_uid, retval);

  retval = g_hash_table_lookup (browser->people, e_uid);
  if (!retval)
    {
      g_warning ("invalid UID: '%s'", e_uid);
    }

  return retval;
}

/* Look up the SoylentPerson for the given EContact ID */
SoylentPerson*
soylent_browser_get_person_from_e_contact (SoylentBrowser *browser,
                                           EContact *e_contact)
{
  SoylentPerson *retval = NULL;
  const gchar *e_uid = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (e_contact, retval);
  g_return_val_if_fail (E_IS_CONTACT (e_contact), retval);
  
  e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
  if (e_uid)
    {
      retval = soylent_browser_get_person_from_e_uid (browser, e_uid);
    }
  else
    {
      g_warning ("failed to find UID for given EContact");
    }

  return retval;
}

/* Get a GList* of SoylentPerson* of the currently-selected people. Kinnda
 * hacky. */
GList*
soylent_browser_get_selected_people (SoylentBrowser *browser)
{
  GList *retval = NULL;
  GList *selected_people_e_uids = NULL;
  GList *l = NULL;
  guint i = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  selected_people_e_uids = soylent_browser_get_selected_people_e_uid (browser);

  /* TODO: use gtk_icon_view_selected_foreach () */

  for (i = 0, l = selected_people_e_uids; l; l = g_list_next (l))
    {
      gchar *e_uid = NULL;

      e_uid = (gchar*) l->data;
      if (e_uid)
        {
          SoylentPerson *person = NULL;

          person = g_hash_table_lookup (browser->people, e_uid);
          if (person)
            {
              retval = g_list_prepend (retval, person);
            }
          else
            {
              g_warning ("invalid UID: '%s'\n", e_uid);
            }

          g_free (e_uid);
        }
      else
        {
          g_warning ("list of selected people contains NULL UID\n");
        }
    }

  g_list_free (selected_people_e_uids);

  return retval;
}

/* Get a GList* of EContact* of the currently-selected people. Kinda hacky. */
GList*
soylent_browser_get_selected_people_e_contact (SoylentBrowser *browser)
{
  GList *retval = NULL;
  GList *selected_people_e_uids = NULL;
  GList *l = NULL;
  guint i = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  selected_people_e_uids = soylent_browser_get_selected_people_e_uid (browser);

  /* TODO: use gtk_icon_view_selected_foreach () */

  for (i = 0, l = selected_people_e_uids; l; l = g_list_next (l))
    {
      gchar *e_uid = NULL;

      e_uid = (gchar*) l->data;
      if (e_uid)
        {
          SoylentPerson *person = NULL;

          person = g_hash_table_lookup (browser->people, e_uid);
          if (person)
            {
              EContact *e_contact = NULL;
              
              e_contact = soylent_person_get_e_contact (person);
              if (e_contact && E_IS_CONTACT (e_contact))
                {
                  retval = g_list_prepend (retval, e_contact);
                }
            }
          else
            {
              g_warning ("invalid UID: '%s'\n", e_uid);
            }

          g_free (e_uid);
        }
      else
        {
          g_warning ("list of selected people contains NULL UID\n");
        }
    }

  g_list_free (selected_people_e_uids);

  return retval;
}

/* Get the EContact IDs of the currently-selected people. Kinda hacky. */
GList*
soylent_browser_get_selected_people_e_uid (SoylentBrowser *browser)
{
  GList *retval = NULL;
  GladeXML *wtree = NULL;
  GtkIconView *icon_view = NULL;
  GList *selected_people = NULL;
  GList *l = NULL;
  guint i = 0;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = browser->main_window;
  icon_view = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  selected_people = gtk_icon_view_get_selected_items (icon_view);
  /* TODO: use gtk_icon_view_selected_foreach () */
  for (i = 0, l = selected_people; l; l = g_list_next (l))
    {
      GtkTreePath *path = NULL;
      GtkTreeModel *model = NULL;
      GtkTreeIter iter;
      gchar *e_uid = NULL;

      path = (GtkTreePath *) l->data;
      model = gtk_icon_view_get_model(icon_view);

      gtk_tree_model_get_iter (model, &iter, path);
      gtk_tree_model_get (model, &iter, PEOPLE_COL_UID, &e_uid, -1);

      retval = g_list_prepend (retval, g_strdup (e_uid));
    }

  return retval;
}

/* Get the SoylentPerson* of the currently-selected user (or first-selected, if
 * a group is selected). Kinda hacky. */
SoylentPerson*
soylent_browser_get_selected_person (SoylentBrowser *browser)
{
  gchar *e_uid = NULL;
  SoylentPerson *retval = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  
  e_uid = soylent_browser_get_selected_person_e_uid (browser);
  if (e_uid)
    {
      retval = g_hash_table_lookup (browser->people, e_uid);
      if (!retval)
        {
          g_warning ("invalid UID: '%s'\n", e_uid);
        }

      g_free (e_uid);
    }
  else
    {
      /* nobody is selected */
    }

  return retval;  
}

/* Get the EContact* of the currently-selected user (or first-selected, if a
 * group is selected). Kinda hacky. */
EContact*
soylent_browser_get_selected_person_e_contact (SoylentBrowser *browser)
{
  EContact *retval = NULL;
  SoylentPerson *person = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  person = soylent_browser_get_selected_person (browser);
  if (person)
    {
      retval = soylent_person_get_e_contact (person);
    }

  return retval;
}

/* FIXME: look into gtk_icon_view_selected_foreach() */
/* Get the e-d-s UID of the currently-selected person (or first-selected, if a
 * group is selected). Kinda hacky. */
gchar*
soylent_browser_get_selected_person_e_uid (SoylentBrowser *browser)
{
  gchar *retval = NULL;
  GladeXML *wtree = NULL;
  GtkTreeIter iter;
  gchar *e_uid = NULL;
  GtkIconView *icon_view = NULL;
  GList *selected_people = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = browser->main_window;
  icon_view = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  selected_people = gtk_icon_view_get_selected_items (icon_view);
  if (selected_people)
    {
      GtkTreePath *path = NULL;

      path = (GtkTreePath*) selected_people->data;
      if (path)
        {
          GtkTreeModel *model = NULL;

          model = gtk_icon_view_get_model (icon_view);
          if (model)
            {
              gtk_tree_model_get_iter (model, &iter, path);
              gtk_tree_model_get (model, &iter, PEOPLE_COL_UID, &e_uid, -1);

              retval = g_strdup (e_uid);
            }
        }
      else
        {
          g_warning ("selected people have a NULL GtkTreePath");
        }
    }
  else
    {
      /* nobody is selected */
    }
 
  return retval;
}

/* Find the given EmpathyContact among the SoylentBrowser's IM contacts */
static SoylentPerson*
soylent_browser_empathy_contact_find_owner_person
                                              (SoylentBrowser *browser,
                                               EmpathyContact *empathy_contact)
{
  SoylentPerson *retval = NULL;
  const gchar *live_name = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);

  live_name = empathy_contact_get_id (empathy_contact);
  if (live_name)
    {
      McAccount *account = NULL;

      account = empathy_contact_get_account (empathy_contact);
      if (account)
        {
          McProfile *profile = NULL;

          profile = mc_account_get_profile (account);
          if (profile)
            {
              const gchar *proto = NULL;

              proto = mc_profile_get_protocol_name (profile);
              if (proto)
                {
                  gchar *e_contact_field_id_name = NULL;
                  EContactField proto_e_contact_field = 0;

                  /* XXX: we should maybe do this directly in the hash... though
                   * it'd probably be a lot slower. */
                  e_contact_field_id_name = g_strdup_printf ("im_%s", proto);
                  proto_e_contact_field = e_contact_field_id
                                                      (e_contact_field_id_name);
                  g_free (e_contact_field_id_name);

                  if (proto_e_contact_field > 0)
                    {
                      gboolean get_contacts_retval = FALSE;
                      EBookQuery *query = NULL;
                      GList *e_contacts = NULL;
                      GError *error = NULL;

                      query = e_book_query_field_test (proto_e_contact_field,
                                                       E_BOOK_QUERY_IS,
                                                       live_name);

                      get_contacts_retval = e_book_get_contacts (browser->book,
                                                                 query,
                                                                 &e_contacts,
                                                                 &error);
                      if (get_contacts_retval)
                        {
                          EContact *match = NULL;
                          GList *l = NULL;

                          for (l = e_contacts; l; l = g_list_next (l))
                            {
                              /* Just match the first one, for now */
                              if (!match)
                                {
                                  match = E_CONTACT (l->data);
                                }
                              else if (l->data)
                                {
                                  g_object_unref (l->data);
                                }
                            }
                              
                          e_book_query_unref (query);
                          g_list_free (e_contacts);

                          if (match)
                            {
                              retval = soylent_browser_get_person_from_e_contact
                                                                      (browser,
                                                                       match);
                            }
                        }
                      else
                        {
                          g_warning ("query to addressbook for username '%s' "
                                     "failed: %s", live_name, error->message);
                          g_clear_error (&error);
                        }
                    }
                }
              else
                {
                  g_warning ("failed to get protocol name for McProfile");
                }
            }
          else
            {
              g_warning ("failed to get McProfile for McAccount");
            }
        }
      else
        {
          g_warning ("failed to get McAccount for EmpathyContact");
        }
    }
  else
    {
      g_warning ("failed to get username for EmpathyContact");
    }

  return retval;
}

/* Set our IM presence for all of our accounts.
 *
 * Return TRUE for success, FALSE for any amount of failure. */
gboolean
soylent_browser_presence_set_cb (GtkWidget *widget, gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;

  g_return_val_if_fail (widget, retval);
  g_return_val_if_fail (GTK_IS_COMBO_BOX (widget), retval);
  g_return_val_if_fail (user_data, retval);

  browser = (SoylentBrowser*) user_data;
  /* TODO: also check that the browser is a SoylentBrowser GObject, once
   * SoylentBrowser is a GObject */
  if (browser->live_idle)
    {
      GtkComboBox *cbox = NULL;
      gchar *active_text = NULL;
      McPresence presence_new = MC_PRESENCE_UNSET; 

      cbox = GTK_COMBO_BOX (widget);
      active_text = gtk_combo_box_get_active_text (cbox);

      /* FIXME: this really needs to be independent of the actual text (hint:
       * i18n), so we need to use a struct for the cbox_presence model items,
       * not just text */
      /* FIXME: reconcile this with SoylentPresence. Which way is the right way
       * to handle this? */
      if (g_ascii_strcasecmp (active_text, "Available") == 0)
        {
          presence_new = MC_PRESENCE_AVAILABLE;
        }
      else if (g_ascii_strcasecmp (active_text, "Busy") == 0)
        {
          presence_new = MC_PRESENCE_AWAY;
        }
      else if (g_ascii_strcasecmp (active_text, "Hiding") == 0)
        {
          presence_new = MC_PRESENCE_HIDDEN;
        }
      else
        {
          g_warning ("unsupported presence setting");
          presence_new = MC_PRESENCE_UNSET;
        }

      if (presence_new != MC_PRESENCE_UNSET)
        {
          /* XXX: would check this value, but this function is void */
          empathy_idle_set_presence (browser->live_idle, presence_new,
                                     active_text);
          retval = TRUE;
        }
    }
  else
    {
      g_warning ("Empathy idle handler is NULL");
    }

  return retval;
}

/* Display and close About dialog.
 *
 * Return TRUE for success, FALSE for any amount of failure. */
gboolean
soylent_browser_dialog_about_show_cb (GtkWidget *widget, gpointer user_data)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  SoylentBrowser *browser = NULL;
  GtkDialog *dia_about = NULL;

  g_return_val_if_fail (widget, retval);
  g_return_val_if_fail (user_data, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (user_data), retval);
  */

  browser = (SoylentBrowser*) user_data;
  wtree = browser->main_window;
  dia_about = GTK_DIALOG (glade_xml_get_widget (wtree, "dia_about"));

  /* Display dialog, enter nested mainloop and block until dialog is closed */
  gtk_dialog_run (dia_about);
  /* Exit nested mainloop*/
  gtk_widget_hide (GTK_WIDGET (dia_about));

  return retval;
}

/* Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_set_people (SoylentBrowser *browser, GHashTable *people)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (people, retval);

  browser->people = people;
  retval = TRUE;

  return retval;
}

/* Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_set_e_book (SoylentBrowser *browser, EBook *e_book)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (e_book, retval);
  g_return_val_if_fail (E_IS_BOOK (e_book), retval);

  browser->book = e_book;
  retval = TRUE;

  return retval;
}

/* Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_set_e_book_view (SoylentBrowser *browser,
                                 EBookView *e_book_view)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (e_book_view, retval);
  g_return_val_if_fail (E_IS_BOOK_VIEW (e_book_view), retval);

  browser->book_view = e_book_view;
  retval = TRUE;

  return retval;
}

/* Distribute known, online EmpathyContacts to their matching SoylentPersons.
 *
 * Return TRUE for success, FALSE for any amount of failure. */
gboolean
soylent_browser_live_setup_finish (SoylentBrowser *browser)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  /* XXX: would check return value, but this function is void */
  /* Set up contact list */
  empathy_status_presets_get_all ();

  browser->live_manager = empathy_contact_manager_new ();
  if (browser->live_manager)
    {
      GList *empathy_contacts = NULL;
      GList *l = NULL;

      /* Manually call the "members-changed" callback to ensure we initialize
       * our associations for each EmpathyContact (in case we missed the only
       * "members-changed" signal instance we will get for some people for a
       * while) */
      empathy_contacts = empathy_contact_list_get_members
                                (EMPATHY_CONTACT_LIST (browser->live_manager));
      for (l = empathy_contacts;
           l && l->data && EMPATHY_CONTACT (l->data);
           l = g_list_next (l))
        {
          EmpathyContact *empathy_contact = NULL;

          empathy_contact = l->data;

          /* XXX: We may want to make a (EmpathyContact hashed ID):(GList* of
           * matching EContacts) hash in SoylentBrowser to do fast reverse
           * lookups, manually synchronizing changes via the
           * EContact::contact_changed,contact_added,contact_removed (or
           * whatever they're called) signal handlers to quickly look up which
           * SoylentPeople changes to the EmpathyContact affect, to perform
           * reactions quickly */

          retval = soylent_browser_live_members_changed_cb
                                  (EMPATHY_CONTACT_LIST (browser->live_manager),
                                   empathy_contact, NULL, 0, NULL, TRUE,
                                   browser);
          g_object_unref (empathy_contact);
        }

      /* XXX: is there any way to clean this up? Can we eliminate the manual
       * invocation above and rely on the callback to handle this stuff? */
      /* Consider this function successful if we handled an empty roster (which
       * can happen if Empathy hasn't populated it by this point) */
      if (!empathy_contacts)
        {
          retval = TRUE;
        }

      /* FIXME: add "groups-changed" */

      /* Set up the callback to handle changes in Empathy IM rosters. */
      g_signal_connect (G_OBJECT (browser->live_manager), "members-changed",
                        G_CALLBACK (soylent_browser_live_members_changed_cb),
                        browser);

      g_list_free (empathy_contacts);
    }
  else
    {
      g_warning ("unable to initialize Connection Manager");
    }

  return retval;
}

/* FIXME: this is vestigial - split it into two functions (show and hide the
 * edit window) */
/* Set the main section of the window to browseable icons or a single-person
 * detail editor
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_view_set_mode (SoylentBrowser *browser,
                               soylent_browser_people_view_mode mode)
{
  gboolean retval = FALSE;
  GladeXML  *wtree = NULL;
  GtkWidget *sw_people = NULL;
  GtkWidget *window_person_view = NULL;
  GtkWidget *img_person_view = NULL;
  GtkWidget *icon_entry_person = NULL;
  GtkWidget *hbox_detail_new = NULL;
  GtkWidget *hs_detail_new = NULL;
  GtkWidget *btn_detail_add = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (SOYLENT_BROWSER_PEOPLE_VIEW_MODE_IS_VALID (mode),
                        retval);

  wtree = browser->main_window;
  sw_people = glade_xml_get_widget (wtree, "sw_people");
  window_person_view = glade_xml_get_widget (wtree, "window_person_view");
  img_person_view = glade_xml_get_widget (wtree, "img_person_view");
  icon_entry_person = glade_xml_get_widget (wtree, "icon_entry_person");
  hbox_detail_new = glade_xml_get_widget (wtree, "hbox_detail_new");
  hs_detail_new = glade_xml_get_widget (wtree, "hs_detail_new");
  btn_detail_add = glade_xml_get_widget (wtree, "btn_detail_add");

  browser->view_mode = mode;
  switch (browser->view_mode)
    {
      SoylentPerson *person = NULL;

      case SB_PEOPLE_VIEW_MODE_BROWSE:
        gtk_widget_set_sensitive (btn_detail_add, FALSE);

        gtk_widget_show (sw_people);
        gtk_widget_hide (window_person_view);

        retval = TRUE;
        break;
      case SB_PEOPLE_VIEW_MODE_EDIT:

        /* FIXME: when SoylentPerson is a GObject, check SOYLENT_IS_PERSON */
        person = soylent_browser_get_selected_person (browser);
        retval = soylent_browser_person_view_update (browser, person);

        if (retval)
          {
            gtk_widget_show (window_person_view);
            gtk_widget_show (img_person_view);

            gtk_widget_hide (icon_entry_person);
            gtk_widget_show (hbox_detail_new);
            gtk_widget_show (hs_detail_new);
          }
        else
          {
            g_warning ("failed to update person's details; not displaying edit "
                       "window");
          }
        break;
      default:
        /* the g_return_val_if_fail() mode check above should prevent us from
         * reaching this block */
        g_warning ("invalid SoylentBrowser mode (you should never get this "
                   "warning)");
        break;
    }

  return retval;
}

/* Visually select the given person in the browser's main view.
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_person_select (SoylentBrowser *browser, SoylentPerson *person,
                               gboolean scroll)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkIconView *iv_people = NULL;
  GtkTreeModel *model = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   */

  wtree = browser->main_window;
  iv_people = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  model = gtk_icon_view_get_model (iv_people);
  if (model)
    {
      GtkTreeIter *icon_view_iter = NULL;

      icon_view_iter = soylent_person_get_icon_view_iter (person);
      if (icon_view_iter)
        {
          GtkTreePath *path = NULL;

          path = gtk_tree_model_get_path (model, icon_view_iter);
          gtk_icon_view_select_path (iv_people, path);

          if (scroll)
            {
              /* minimal-distance scroll; don't bother with alignment */
              gtk_icon_view_scroll_to_path (iv_people, path, FALSE, 0, 0);
            }

          gtk_tree_path_free (path);

          retval = TRUE;
        }
      else
        {
          g_critical ("icon view iterator is NULL");
        }
    }
  else
    {
      g_critical ("icon view's model is NULL");
    }

  return retval;
}

/* Update the visual representation of the given SoylentPerson in the given
 * SoylentBrowser
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_update_person_icon (SoylentBrowser *browser,
                                    SoylentPerson *person)
{
  gboolean retval = FALSE;
  GtkIconView *icon_view = NULL;
  GtkListStore *model = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (person, retval);
  /* FIXME: uncomment once SoylentPerson is a GObject:
  g_return_val_if_fail (SOYLENT_IS_PERSON (person), retval);
   */

  icon_view = GTK_ICON_VIEW (glade_xml_get_widget (browser->main_window,
                                                   "iv_people"));
  model = GTK_LIST_STORE (gtk_icon_view_get_model (icon_view));
  if (model)
    {
      gchar *name_display = NULL;

      name_display = soylent_person_get_display_name_with_presence (person);
      if (name_display)
        {
          EContact *e_contact = NULL;

          e_contact = soylent_person_get_e_contact (person);
          if (e_contact && E_IS_CONTACT (e_contact))
            {
              const gchar *e_uid = NULL;

              e_uid = e_contact_get_const (e_contact, E_CONTACT_UID);
              if (e_uid)
                {
                  GdkPixbuf *avatar_pixbuf = NULL;
                  GtkTreeIter *icon_view_iter = NULL;

                  avatar_pixbuf = soylent_person_get_avatar
                                                    (person,
                                                     browser->icon_width_max,
                                                     browser->icon_height_max);

                  if (!avatar_pixbuf)
                    {
                      avatar_pixbuf = browser->icon_stock_person;
                    }

                  icon_view_iter = soylent_person_get_icon_view_iter (person);
                  if (icon_view_iter)
                    {
                      if (icon_view_iter->stamp == SP_ITER_UNSET)
                        {
                          gtk_list_store_insert_with_values
                                              (model, icon_view_iter, 0,
                                               PEOPLE_COL_NAME, name_display,
                                               PEOPLE_COL_UID, e_uid,
                                               PEOPLE_COL_PHOTO, avatar_pixbuf,
                                               -1);
                        }
                      else
                        {
                          gtk_list_store_set (model, icon_view_iter,
                                             /* XXX - for some reason, if we
                                              * don't set the uid here again,
                                              * the UID stored can get corrupted
                                              * */
                                              PEOPLE_COL_UID, e_uid,
                                              PEOPLE_COL_NAME, name_display,
                                              PEOPLE_COL_PHOTO, avatar_pixbuf,
                                              -1);
                        }

                      g_free (name_display);

                      /* FIXME: handle this memory management */
                      /*
                      if (avatar_pixbuf_needs_unref)
                        g_object_unref (avatar_pixbuf);
                        */
                      retval =  TRUE;
                    }
                  else
                    {
                      g_critical ("failed to get main icon view's iterator");
                    }
                }
              else
                {
                  g_warning ("failed to get EContact's UID");
                }
            }
          else
            {
              g_warning ("person's EContact is invalid");
            }
        }
      else
        {
          g_warning ("failed to get display name for person");
        }
    }
  else
    {
      g_critical ("main icon view's model is invalid");
    }

  return retval;
}

/* Union the given group with the SoylentBrowser's current set of groups 
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
soylent_browser_check_add_group (SoylentBrowser *browser, const gchar *group)
{
  gboolean retval = FALSE;

 /*TODO*/
  retval = TRUE;

  return retval;
}

void
soylent_browser_delete_cb (GtkWidget *widget, GdkEvent *event,
                           gpointer user_data)
{
  SoylentBrowser *browser = NULL;

  browser = (SoylentBrowser*) user_data;
  /* FIXME: once SoylentBrowser is a GObject, also check SOYLENT_IS_BROWSER */
  if (browser)
    {
      gboolean save_retval = FALSE;
      GList *selected_people = NULL;

      /* use the main window's "delete-event" to make sure that we save any
       * unsaved changes upon main window deletion */
      selected_people = soylent_browser_get_selected_people_e_uid (browser);
      if (selected_people)
        {
          save_retval = soylent_browser_person_view_save_finalize_cb (NULL,
                                                                      NULL,
                                                                      browser);
          if (!save_retval)
            {
              g_warning ("failed to save changes to person");
            }
        }
    }

  gtk_main_quit();
}

/* Handle additions and removals of contacts to the given IM account's roster
 *
 * Return TRUE for success, FALSE for any amount of failure */
gboolean
soylent_browser_live_members_changed_cb (EmpathyContactList *list_iface,
                                         EmpathyContact *empathy_contact,
                                         EmpathyContact *actor,
                                         guint reason, gchar *message,
                                         gboolean is_member,
                                         SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  SoylentPerson *person = NULL;

  person = soylent_browser_empathy_contact_find_owner_person (browser,
                                                              empathy_contact);

  /* If we are being notified this contact is a new member to the roster, hook
   * up signal callbacks and attach the EmpathyContact to the SoylentPerson */
  if (is_member)
    {
      /* If we know a SoylentPerson who matches this contact, hook up
       * notification signals (otherwise, wait until another signal would cause
       * the association to hook up these Empathy notification signals (this
       * saves us some effort in the meantime)) */
      if (person)
        {
          g_signal_connect (empathy_contact, "notify::groups",
                            G_CALLBACK (soylent_person_live_groups_updated_cb),
                            (gpointer) person);
          g_signal_connect
                          (empathy_contact, "notify::presence",
                           G_CALLBACK (soylent_person_live_presence_updated_cb),
                           (gpointer) person);
          g_signal_connect (empathy_contact, "notify::name",
                            G_CALLBACK (soylent_person_live_name_updated_cb),
                            (gpointer) person);
          g_signal_connect (empathy_contact, "notify::avatar",
                            G_CALLBACK (soylent_person_live_avatar_updated_cb),
                            (gpointer) person);
          g_signal_connect (empathy_contact, "notify::type",
                            G_CALLBACK (soylent_person_live_type_updated_cb),
                            (gpointer) person);
        }

      /* this handles NULL person pointers (adds the contact to a browser-wide
       * pool) */
      retval = soylent_browser_person_associate_empathy_contact
                                                              (browser, person,
                                                               empathy_contact);
    }
  /* Otherwise, this EmpathyContact was removed from our roster; drop them from
   * our own structures */
  else
    {
      g_signal_handlers_disconnect_by_func
                          (empathy_contact,
                           G_CALLBACK (soylent_person_live_groups_updated_cb),
                           (gpointer) person);
      g_signal_handlers_disconnect_by_func
                        (empathy_contact,
                         G_CALLBACK (soylent_person_live_presence_updated_cb),
                         (gpointer) person);
      g_signal_handlers_disconnect_by_func
                            (empathy_contact,
                             G_CALLBACK (soylent_person_live_name_updated_cb),
                             (gpointer) person);
      g_signal_handlers_disconnect_by_func
                          (empathy_contact,
                           G_CALLBACK (soylent_person_live_avatar_updated_cb),
                           (gpointer) person);
      g_signal_handlers_disconnect_by_func
                            (empathy_contact,
                             G_CALLBACK (soylent_person_live_type_updated_cb),
                             (gpointer) person);

      /* this handles NULL person pointers (adds the contact to a browser-wide
       * pool) */
      retval = soylent_browser_person_dissociate_empathy_contact
                                                              (browser, person,
                                                               empathy_contact);
    }

  return retval;
}

/* Compare the display names of two people in the main icon view; used to sort
 * the main icon view
 *
 * Return
 *    0   if the two people have the same name
 *  < 0   if person a's name < person b's name
 *  > 0   if person b's name > person a's name */
static int
soylent_browser_compare_people_by_icon_iter (GtkTreeModel *model,
                                             GtkTreeIter *a, GtkTreeIter *b,
                                             gpointer ptr)
{
  int retval = 0;
  const gchar *name_a = NULL;
  const gchar *name_b = NULL;

  gtk_tree_model_get (model, a, PEOPLE_COL_NAME, &name_a, -1);
  gtk_tree_model_get (model, b, PEOPLE_COL_NAME, &name_b, -1);

  retval = g_ascii_strcasecmp (name_a, name_b);

  return retval;
}

/* Handle the person icon activation
 *
 * Return TRUE for success, FALSE for any failure */
static gboolean
soylent_browser_person_icon_activated_cb (GtkIconView *iv, GtkTreePath *path,
                                          gpointer user_data)
{
  gboolean retval = FALSE;
  SoylentBrowser *browser = NULL;
  SoylentPerson *person = NULL;

  /* FIXME: once SoylentBrowser is a GObject, also check SOYLENT_IS_BROWSER */
  browser = (SoylentBrowser*) user_data;
  if (browser)
    {
      /* FIXME: once SoylentPerson is a GObject, also check SOYLENT_IS_PERSON */
      person = soylent_browser_get_selected_person (browser);
      if (person)
        {
          retval = soylent_person_action_communicate (person); 
        }
      else
        {
          g_warning ("no person is selected to communicate with");
        }
    }
  else
    {
      g_critical ("failed to activate a person: browser is NULL");
    }
  
  return retval;
}

/* Add the given EmpathyContact to the given SoylentPerson in the given
 * SoylentBrowser
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_associate_empathy_contact
                                              (SoylentBrowser *browser,
                                               SoylentPerson *person,
                                               EmpathyContact *empathy_contact)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);

  /* FIXME: once SoylentPerson is a GObject, check SOYLENT_IS_PERSON */
  if (person)
    {
      retval = soylent_person_add_empathy_contact (person, empathy_contact);
    }
  else
    {
      browser->empathy_contacts_pending = g_list_prepend
                                            (browser->empathy_contacts_pending,
                                             empathy_contact);
      retval = TRUE;
    }

  return retval;
}

/* Remove the given EmpathyContact from the given SoylentPerson in the given
 * SoylentBrowser
 *
 * Return TRUE for success, FALSE for any failure. */
static gboolean
soylent_browser_person_dissociate_empathy_contact
                                              (SoylentBrowser *browser,
                                               SoylentPerson *person,
                                               EmpathyContact *empathy_contact)
{
  gboolean retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */
  g_return_val_if_fail (empathy_contact, retval);
  g_return_val_if_fail (EMPATHY_IS_CONTACT (empathy_contact), retval);

  /* FIXME: once SoylentPerson is a GObject, check SOYLENT_IS_PERSON */
  if (person)
    {
      retval = soylent_person_remove_empathy_contact (person, empathy_contact);
    }
  else
    {
      browser->empathy_contacts_pending = g_list_remove_all
                                            (browser->empathy_contacts_pending,
                                             empathy_contact);

      retval = TRUE;
    }

  return retval;
}

/* Set build-specific information in the About dialog
 *
 * Return TRUE for complete success, FALSE otherwise. */
static gboolean
soylent_browser_about_dialog_setup (SoylentBrowser *browser)
{
  const gchar *authors[] =
    {"Travis Reitter <treitter-dev@netdrain.com> (maintainer)",
     "Chris Lord <chris@openedhand.com>",
     "Rob Taylor <rob.taylor@codethink.co.uk>",
     NULL};
  const gchar *artists[] = {"Hylke Bons <h.bons@student.rug.nl> (logo)", NULL};
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkAboutDialog *dia_about = NULL;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = browser->main_window;
  dia_about = GTK_ABOUT_DIALOG (glade_xml_get_widget (wtree, "dia_about"));

  gtk_about_dialog_set_version (dia_about, VERSION);

  /* XXX: work around Glade bugs (preventing us from setting these in the glade
   * file) */
  gtk_about_dialog_set_authors (dia_about, authors);
  gtk_about_dialog_set_artists (dia_about, artists);

  retval = TRUE;

  return retval;
}

/* Any widget setup that Glade won't do/doesn't do properly.
 *
 * Return TRUE for complete success, FALSE otherwise. */
static gboolean
soylent_browser_widget_setup (SoylentBrowser *browser)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  GtkListStore *list_store = NULL;
  GtkComboBox *cbox_presence = NULL;
  GtkIconView *icon_view = NULL;
  gboolean detail_add_retval = FALSE;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = browser->main_window;

  /* Configure the main icon view */
  list_store = gtk_list_store_new (NUM_PEOPLE_COLS, G_TYPE_STRING,
                                   G_TYPE_STRING, GDK_TYPE_PIXBUF);
  icon_view = GTK_ICON_VIEW (glade_xml_get_widget (wtree, "iv_people"));
  gtk_icon_view_set_model (icon_view, GTK_TREE_MODEL (list_store));
  gtk_icon_view_set_text_column (icon_view, PEOPLE_COL_NAME);
  gtk_icon_view_set_pixbuf_column (icon_view, PEOPLE_COL_PHOTO);
  gtk_tree_sortable_set_sort_column_id ((GtkTreeSortable*) list_store,
                                        PEOPLE_COL_NAME, GTK_SORT_ASCENDING);
  gtk_tree_sortable_set_sort_func ((GtkTreeSortable*) list_store,
                                   PEOPLE_COL_NAME,
                                   soylent_browser_compare_people_by_icon_iter,
                                   NULL, NULL);

  /* XXX - Glade lets us set these values, but they aren't honored */
  cbox_presence = GTK_COMBO_BOX (glade_xml_get_widget (wtree, "cbox_presence"));
  gtk_combo_box_set_active (cbox_presence, 0);

  detail_add_retval = soylent_browser_person_detail_add_cbox_setup (browser,
                                                                    FALSE);
  if (detail_add_retval)
    {
      gboolean menu_bar_retval = FALSE;

      menu_bar_retval = soylent_browser_menu_bar_setup (browser);
      if (menu_bar_retval)
        {
          retval = soylent_browser_about_dialog_setup (browser);
        }
      else
        {
          g_critical ("failed to set up menu bar");
        }
    }
  else
    {
      g_critical ("failed to set up Detail Add combo boxes");
    }

  return retval;
}

/* Hook up the dialog box button handlers
 *
 * Return TRUE for success, for FALSE for any failure */
static gboolean
soylent_browser_widget_signals_setup (SoylentBrowser *browser)
{
  /* XXX: this is dangerously close to being "clever", but it's better than a
   * bunch of similar-but-different functions */
  const widget_signal_args_t const signal_handlers[] =
    {
      /* Main app window */
      {"window_main", "delete-event", soylent_browser_delete_cb, CB_DATA_SIMPLE,
       browser},

      /* People icons in main icon view */
      {"iv_people", "selection-changed",
       soylent_browser_person_selection_changed_cb, CB_DATA_SIMPLE, browser},
      {"iv_people", "item_activated", soylent_browser_person_icon_activated_cb,
       CB_DATA_SIMPLE, browser},

      /* Set Online Presence combo box */
      {"cbox_presence", "changed", soylent_browser_presence_set_cb,
       CB_DATA_SIMPLE, browser},

      /* Person action buttons */
      {"btntb_email", "clicked",
       soylent_browser_person_action_email_menu_tool_button_cb, CB_DATA_SIMPLE,
       browser},
      {"btntb_chat", "clicked",
       soylent_browser_person_action_chat_menu_tool_button_cb, CB_DATA_SIMPLE,
       browser},

      /* "Delete Person?" dialog */
      {"btn_delete_person_cancel", "clicked", 
       soylent_browser_person_action_delete_selected_hide_dialog_cb,
       CB_DATA_SIMPLE, browser,},
      {"btn_delete_person_confirm", "clicked", 
       soylent_browser_person_action_delete_selected_finalize_cb,
       CB_DATA_SIMPLE, browser},
      {"dia_delete_person_confirm", "delete-event", gtk_widget_hide_on_delete,
       CB_DATA_SIMPLE, browser},

      /* Error dialogs */
      {"error_chat_new", "delete-event", gtk_widget_hide_on_delete,
       CB_DATA_SIMPLE, browser},
      {"error_chat_new", "close", gtk_widget_hide, CB_DATA_SIMPLE, browser},
      {"error_chat_new", "response", gtk_widget_hide, CB_DATA_SIMPLE, browser},

      /* Automatically save changes when closing the Person Details window */
      {"window_person_view", "delete-event",
       soylent_browser_person_view_save_finalize_cb, CB_DATA_SIMPLE, browser},

      /* Adding new details to a person */
      /*   general */
      {"cbox_detail_domain", "changed",
       soylent_browser_person_detail_add_cbox_update, CB_DATA_SIMPLE, browser},
      {"entry_detail_add", "changed",
       soylent_browser_person_btn_detail_add_update_simple,
       CB_DATA_SIMPLE, browser},
      {"btn_detail_add", "clicked", soylent_browser_person_detail_add_commit,
       CB_DATA_SIMPLE, browser},
      /*   postal mail */
      {"tv_person_mail_add_street", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
      {"entry_person_mail_add_po", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
      {"entry_person_mail_add_locality", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
      {"entry_person_mail_add_code", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
      {"entry_person_mail_add_region", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
      {"entry_person_mail_add_country", "changed",
       soylent_browser_person_btn_detail_add_update_mail, CB_DATA_SIMPLE,
       browser},
    };
  gboolean retval = FALSE;
  gint signals_num = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  signals_num = ARRAY_LEN (signal_handlers);

  retval = TRUE;
  retval &= widget_signal_connect_mass (browser, signal_handlers, signals_num);

  return retval;
}
