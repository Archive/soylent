/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *  Author: Rob Taylor <rob.taylor@codethink.co.uk>
 *  Author: Chris Lord <chris@o-hand.com>
 *   (Some code copied from Contacts, by Chris Lord)
 *
 *  Copyright (c) 2005 OpenedHand Ltd - http://o-hand.com
 *  Copyright (c) 2007 Codethink Ltd - http://codethink.co.uk
 *  Copyright (c) 2007 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#ifndef _SOYLENT_DEFS_H_
#define _SOYLENT_DEFS_H_

#include <glade/glade.h>
#include <libebook/e-book.h>

#include <libempathy/empathy-debug.h>
#include <libempathy/empathy-utils.h>
#include <libempathy/empathy-contact.h>
#include <libempathy/empathy-tp-chat.h>
#include <libempathy/empathy-idle.h>
#include <libempathy/empathy-contact-manager.h>
#include <libempathy/empathy-contact-list.h>
#include <libempathy-gtk/empathy-ui-utils.h>

typedef enum sb_people_view_icon_model_column_tag
                                              sb_people_view_icon_model_column;
typedef enum SoylentPresence_tag SoylentPresence;
typedef struct cb_entry_changed_e_contact_tag cb_entry_changed_e_contact;

#include "soylent-browser.h"

#define MAX_GUI_EMAIL 4

/* Set to the first e-d-s IM field (hopefully won't change) */
#define CB_DATA_FIELD_IM  E_CONTACT_IM_AIM
/* TODO: put these in configure.ac */
#define ICON_MAX_WIDTH  72
#define ICON_MAX_HEIGHT 72

#define SP_PRESENCE_IS_VALID(x) (((x) > SP_PRESENCE_UNSET) \
                                 && ((x) <= SP_PRESENCE_AVAILABLE))

extern const gchar *MAIL_WIDGET_TYPE_STRS[];
/* Evo -> en_us: locality == city */
extern const gchar *MAIL_FIELD_STRS[];
/* FIXME: use this everywhere; remove the ones above; note the ordering change -
 * does that matter at all? */
extern const gchar *MAIL_WIDGET_TYPE_FULL_STRS[];
extern const gchar *MAIL_FIELD_FULL_STRS[];

enum sb_people_view_icon_model_column_tag
{
  PEOPLE_COL_UID,
  PEOPLE_COL_NAME,
  PEOPLE_COL_PHOTO,
  NUM_PEOPLE_COLS
};

/* These are ordered from least to most available */
enum SoylentPresence_tag
{
  SP_PRESENCE_UNSET = 0,
  SP_PRESENCE_OFFLINE,
  SP_PRESENCE_IDLE_BUSY,
  SP_PRESENCE_IDLE,
  SP_PRESENCE_BUSY,
  SP_PRESENCE_AVAILABLE,
  LAST_SP_PRESENCE
};

struct cb_entry_changed_e_contact_tag
{
  SoylentBrowser *browser;
  gchar *widget_name;
  EContactField *field;
  guint field_pos;
};

#endif /* _SOYLENT_DEFS_H_ */
