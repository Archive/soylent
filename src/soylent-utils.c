/* 
 *  Soylent people browser
 *
 *  Author: Travis Reitter <treitter-dev@netdrain.com>
 *
 *  Copyright (c) 2007-2008 Travis Reitter
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

#include "soylent-defs.h"
#include "soylent-utils.h"

/* Retrieve the named widget from the wtree, then attach the callback and
 * user_data to the widget's given signal. If the widget is a GtkTextView, the
 * callback is instead attached to the given signal for its GtkTextBuffer
 *
 * Return TRUE for success, FALSE for any failure */
gboolean
widget_signal_connect (GladeXML *wtree, const gchar *widget_name,
                       const gchar *signal, gconstpointer callback,
                       gpointer user_data)
{
  gboolean retval = FALSE;
  GtkWidget *widget = NULL;

  g_return_val_if_fail (wtree, retval);
  g_return_val_if_fail (GLADE_IS_XML (wtree), retval);
  g_return_val_if_fail (widget_name, retval);
  g_return_val_if_fail (signal, retval);
  g_return_val_if_fail (callback, retval);

  widget = glade_xml_get_widget (wtree, widget_name);
  if (widget && GTK_IS_WIDGET (widget))
    {
      gint handler_id = -1;

      /* If the given widget is a GtkTextView, attach the callback to the signal
       * of its GtkTextBuffer instead */
      if (GTK_IS_TEXT_VIEW (widget))
        {
          GtkTextBuffer *text_buffer = NULL;

          text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
          handler_id = g_signal_connect (G_OBJECT (text_buffer), signal,
                                         G_CALLBACK (callback), user_data);
          if (handler_id >= G_SIGNAL_HANDLER_MIN_VALID)
            {
              retval = TRUE;
            }
        }
      else
        {
          handler_id = g_signal_connect (G_OBJECT (widget), signal,
                                         G_CALLBACK (callback), user_data);
          if (handler_id >= G_SIGNAL_HANDLER_MIN_VALID)
            {
              retval = TRUE;
            }
        }
    }

  return retval;
}

/* Connect a collection callbacks to the given widgets' signals
 *
 * Return TRUE for success, FALSE for any failure. */
gboolean
widget_signal_connect_mass (SoylentBrowser *browser,
                            const widget_signal_args_t const *signal_handlers,
                            guint signals_num)
{
  gboolean retval = FALSE;
  GladeXML *wtree = NULL;
  gint i = -1;

  g_return_val_if_fail (browser, retval);
  /* FIXME: uncomment once SoylentBrowser is a GObject:
  g_return_val_if_fail (SOYLENT_IS_BROWSER (browser), retval);
   */

  wtree = soylent_browser_get_widget_tree (browser);

  /* invert usual logic to simplify the loop below */
  retval = TRUE;

  for (i = 0; i < signals_num; i++)
    {
      gboolean connect_retval = FALSE;
      widget_signal_args_t cur_args = signal_handlers[i];
      cb_entry_changed_e_contact *cb_data = NULL;

      switch (cur_args.cb_data_type)
        {
          case CB_DATA_SIMPLE:
            connect_retval = widget_signal_connect (wtree, cur_args.widget_name,
                                                    cur_args.signal_name,
                                                    cur_args.callback,
                                                    cur_args.user_data);
            break;
          case CB_DATA_COMPLEX_FROM_E_CONTACT_FIELD:
            cb_data = g_new0 (cb_entry_changed_e_contact, 1);
            cb_data->browser = browser;
            cb_data->widget_name = cur_args.widget_name;
            cb_data->field = g_new0 (EContactField, 1);
            if (cb_data->field)
              {
                *(cb_data->field) = (EContactField) cur_args.user_data;

                /* FIXME - use a version which uses g_signal_connect_data so
                  * we can eventually free each instance of cb_data */
                connect_retval = widget_signal_connect (wtree,
                                                        cur_args.widget_name,
                                                        cur_args.signal_name,
                                                        cur_args.callback,
                                                        cb_data);
              }

            if (!connect_retval)
              {
                if (cb_data)
                  {
                    g_free (cb_data->field);
                    g_free (cb_data);
                  }
              }
          break;
          default:
            g_critical ("unhandled signal setup cb_data_t: %d\n",
                        cur_args.cb_data_type);
            break;

          if (!connect_retval)
            {
              g_critical ("failed to set up signal handler for widget %s::%s\n",
                          cur_args.widget_name, cur_args.signal_name);
              retval = FALSE;
            }
        }
    }

  return retval;
}

/* Set the contents of a GtkTextBuffer without triggering its handler
 * (eg, when pre-filling them with e-d-s fields for the given person) */
void
text_buffer_set_text_block_handler (GtkTextBuffer *buf, const gchar *text,
                                    gpointer handler)
{
  g_signal_handlers_block_matched   (buf, G_SIGNAL_MATCH_FUNC, 0, (GQuark) 0,
                                     NULL, handler, NULL);

  gtk_text_buffer_set_text (buf, text, -1);

  g_signal_handlers_unblock_matched (buf, G_SIGNAL_MATCH_FUNC, 0, (GQuark) 0,
                                     NULL, handler, NULL);
}

/* Set the contents of a GtkEntry without triggering its handler
 * (eg, when pre-filling them with e-d-s fields for the given person) */
void
entry_set_text_block_handler (GtkEntry *entry, const gchar *text,
                              gpointer handler)
{
  g_signal_handlers_block_matched   (entry, G_SIGNAL_MATCH_FUNC, 0, (GQuark) 0,
                                     NULL, handler, NULL);

  gtk_entry_set_text (entry, text);

  g_signal_handlers_unblock_matched (entry, G_SIGNAL_MATCH_FUNC, 0, (GQuark) 0,
                                     NULL, handler, NULL);
}

/* Return a copy of the given name portion of a person's full name if
 * successful; otherwise, NULL */
gchar*
name_get_given_from_full (const gchar *full_name)
{
  gchar *retval = NULL;
  gchar **name_split = NULL;

  g_return_val_if_fail (full_name, retval);
  g_return_val_if_fail (!g_str_equal (full_name, ""), retval);

  name_split = g_strsplit (full_name, " ", -1);
  retval = g_strdup (name_split[0]);

  g_strfreev (name_split);

  return retval;
}

/* Return a copy of the family name portion of a person's full name if
 * successful; otherwise, NULL */
gchar*
name_get_family_from_full (const gchar *full_name)
{
  gchar *retval = NULL;
  gchar **name_split = NULL;
  guint names_num = 0;

  g_return_val_if_fail (full_name, retval);
  g_return_val_if_fail (!g_str_equal (full_name, ""), retval);

  name_split = g_strsplit (full_name, " ", -1);

  for (names_num = 0; name_split && name_split[names_num]; names_num++)
    {
    }

  if (names_num > 1)
    {
      retval = g_strdup (name_split[names_num - 1]);
    }

  g_strfreev (name_split);

  return retval;
}
